﻿using FootApp.DAO;
using FootApp.DTO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.PDF
{
    class PDFUtil
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly Font centuryGothic;
        static PDFUtil()
        {
            centuryGothic = new Font(BaseFont.CreateFont(
                @"C:\Windows\Fonts\Calibri.ttf",
                BaseFont.IDENTITY_H,
                BaseFont.EMBEDDED));

        }

        public byte[] CreateMembershipFeePDFAsByteArray()
        {
            Document doc = new Document(PageSize.A4);

            try
            {
                MemoryStream ms = new MemoryStream();
                PdfWriter.GetInstance(doc, ms);
                doc.Open();

                PdfPTable table = new PdfPTable(3);
                table.TotalWidth = 400f;
                table.LockedWidth = true;
                float[] widths = new float[] { 50f, 250f, 100f };
                table.SetWidths(widths);
                //header
                PdfPCell cell1 = new PdfPCell(new Phrase("Redni Broj", centuryGothic));
                cell1.HorizontalAlignment = 1;
                table.AddCell(cell1);
                PdfPCell cell2 = new PdfPCell(new Phrase("Ime i Prezime", centuryGothic));
                cell2.HorizontalAlignment = 1;
                table.AddCell(cell2);
                PdfPCell cell3 = new PdfPCell(new Phrase("Mjesec", centuryGothic));
                cell3.HorizontalAlignment = 1;
                table.AddCell(cell3);
                //rows
                int i = 1;
                List<Debt> debts = new MembershipFeeDAO().GetAllDebts();
                foreach (var item in debts)
                {
                    table.AddCell(new Phrase((i++).ToString(), centuryGothic));
                    table.AddCell(new Phrase(item.Name + " " + item.Surname, centuryGothic));
                    table.AddCell(new Phrase(item.Date, centuryGothic));
                }
                doc.Add(table);
                doc.Close();
                return ms.ToArray();
            }
            catch (Exception e)
            {
                log.Warn(e);
                return null;
            }
        }
        public bool ExportDataAsPdf(String fileLocation, byte[] documentData)
        {
            try
            {
                FileStream file = new FileStream(fileLocation, FileMode.Create);
                file.Write(documentData, 0, documentData.Length);
                file.Close();
                System.Diagnostics.Process.Start(fileLocation);
                return true;
            }
            catch (Exception e)
            {
                log.Warn(e);
                return false;
            }
        }


        public byte[] CreateFinancePDFAsByteArray(DateTime startDate, DateTime endDate)
        {
            Document doc = new Document(PageSize.A4);

            try
            {
                MemoryStream ms = new MemoryStream();
                PdfWriter.GetInstance(doc, ms);
                doc.Open();
                Paragraph title = new Paragraph();
                title.Alignment = Element.ALIGN_CENTER;
                title.Font = FontFactory.GetFont("Arial", 54);
                title.Add("\nFinance \n\n");
                doc.Add(title);
                Paragraph period = new Paragraph();
                period.Alignment = Element.ALIGN_CENTER;
                //period.Font = FontFactory.GetFont("Arial", 32);
                period.Add("Period: " + startDate.ToString("dd.MM.yyyy") + " - " + endDate.ToString("dd.MM.yyyy"));
                doc.Add(period);
                doc.Add(new Paragraph("\n"));
                List<FinancialEntry> entries = new FinancialEntryDAO().GetFinancialEntriesForPeriod(startDate,endDate);
                foreach (var item in entries)
                {
                    PdfPTable table = new PdfPTable(2);
                    table.TotalWidth = 500f;
                    table.LockedWidth = true;
                    float[] widths = new float[] { 200f, 300f };
                    table.SetWidths(widths);
                    //header
                    PdfPCell cell1 = new PdfPCell(new Phrase("Field", centuryGothic));
                    cell1.HorizontalAlignment = 1;
                    table.AddCell(cell1);
                    PdfPCell cell2 = new PdfPCell(new Phrase("Value", centuryGothic));
                    cell2.HorizontalAlignment = 1;
                    table.AddCell(cell2);
                    table.AddCell(new Phrase("Description", centuryGothic));
                    table.AddCell(new Phrase(item.Description, centuryGothic));
                    table.AddCell(new Phrase("Time", centuryGothic));
                    table.AddCell(new Phrase(item.Time.ToString("dd.MM.yyyy HH:mm"), centuryGothic));
                    table.AddCell(new Phrase("Count", centuryGothic));
                    table.AddCell(new Phrase(item.Count.ToString(), centuryGothic));
                    table.AddCell(new Phrase("Amount", centuryGothic));
                    table.AddCell(new Phrase(item.Amount.ToString(), centuryGothic));
                    table.AddCell(new Phrase("Type", centuryGothic));
                    table.AddCell(new Phrase(item.Type.ToString(), centuryGothic)) ;
                    doc.Add(table);
                    doc.Add(new Paragraph("\n"));
                }
                
                doc.Close();
                return ms.ToArray();
            }
            catch (Exception e)
            {
                log.Warn(e);
                return null;
            }
        }
        public byte[] CreatePlayerStatsPDFAsByteArray(Player player,List<Season> seasons)
        {
            Document doc = new Document(PageSize.A4);

            try
            {
                MemoryStream ms = new MemoryStream();
                PdfWriter.GetInstance(doc, ms);
                doc.Open();
                Paragraph title = new Paragraph();
                title.Alignment = Element.ALIGN_CENTER;
                title.Font = FontFactory.GetFont("Arial", 54);
                title.Add("\nPlayer Statistics \n\n");
                doc.Add(title);


                foreach (var season in seasons)
                {
                    PlayerStatistics playerStatistics = new PlayerStatisticsDAO().GetPlayerStatisticsForSeason(player, season);
                    PdfPTable table = new PdfPTable(2);
                    table.TotalWidth = 500f;
                    table.LockedWidth = true;
                    float[] widths = new float[] { 200f, 300f };
                    table.SetWidths(widths);
                    //header
                    PdfPCell cell1 = new PdfPCell(new Phrase("Field", centuryGothic));
                    cell1.HorizontalAlignment = 1;
                    table.AddCell(cell1);
                    PdfPCell cell2 = new PdfPCell(new Phrase("Value", centuryGothic));
                    cell1.HorizontalAlignment = 1;
                    table.AddCell(cell2);
                    table.AddCell(new Phrase("Games Played", centuryGothic));
                    table.AddCell(new Phrase(playerStatistics.GamesPlayed.ToString(), centuryGothic));
                    table.AddCell(new Phrase("Passes per Game", centuryGothic));
                    table.AddCell(new Phrase(playerStatistics.PassesPerGame.ToString(), centuryGothic));
                    table.AddCell(new Phrase("Successful Passes Percent", centuryGothic));
                    table.AddCell(new Phrase(playerStatistics.SuccessfulPassesPercent.ToString(), centuryGothic));
                    table.AddCell(new Phrase("Goals per Game", centuryGothic));
                    table.AddCell(new Phrase(playerStatistics.GoalsPerGame.ToString(), centuryGothic));
                    table.AddCell(new Phrase("Assists per Game", centuryGothic));
                    table.AddCell(new Phrase(playerStatistics.AssistsPerGame.ToString(), centuryGothic));
                    table.AddCell(new Phrase("Shots per Game", centuryGothic));
                    table.AddCell(new Phrase(playerStatistics.ShotsPerGame.ToString(), centuryGothic));
                    table.AddCell(new Phrase("Shot Accuracy", centuryGothic));
                    table.AddCell(new Phrase(playerStatistics.ShotAccuracy.ToString(), centuryGothic));
                    table.AddCell(new Phrase("Total Goals", centuryGothic));
                    table.AddCell(new Phrase(playerStatistics.TotalGoals.ToString(), centuryGothic));
                    table.AddCell(new Phrase("Total Assists", centuryGothic));
                    table.AddCell(new Phrase(playerStatistics.TotalAssists.ToString(), centuryGothic));
                    table.AddCell(new Phrase("Total Red Cards", centuryGothic));
                    table.AddCell(new Phrase(playerStatistics.TotalRedCards.ToString(), centuryGothic));
                    table.AddCell(new Phrase("Total Yellow Cards", centuryGothic));
                    table.AddCell(new Phrase(playerStatistics.TotalYellowCards.ToString(), centuryGothic));
                    doc.Add(new Paragraph("Season: " + season.StartDate.ToString("yyyy") + "/" + season.EndDate.ToString("yyyy")+"\n \n"));
                    doc.Add(table);
                    doc.Add(new Paragraph("\n"));
                }
                doc.Close();
                return ms.ToArray();
            }
            catch (Exception e)
            {
                log.Warn(e);
                return null;
            }
        }
    }
}
