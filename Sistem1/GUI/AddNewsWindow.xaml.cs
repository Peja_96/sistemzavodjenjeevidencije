﻿using FootApp.DAO;
using FootApp.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for AddNewsWindow.xaml
    /// </summary>
    public partial class AddNewsWindow : Window
    {
        public AddNewsWindow()
        {
            InitializeComponent();
            this.ShowInTaskbar = false;
            InitForComboBox();
        }

        private void InitForComboBox()
        {
            cmbxFor.Items.Add("All");
            cmbxFor.Items.Add("Player");
            cmbxFor.Items.Add("Employee");
            cmbxFor.SelectedIndex = 0;
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
            
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(tbxTitle.Text) || string.IsNullOrEmpty(cmbxPosition.SelectedItem as string) || string.IsNullOrEmpty(cmbxFor.SelectedItem as string) || string.IsNullOrEmpty(tbxContent.Text))
            {
                new CustomMessageBox("Error", "Provide all information fro new announcement.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
            else
            {
                new AnnouncementDAO().AddAnnouncement(new Announcement
                {
                    AnnouncementTableID = 1,
                    PersonType = cmbxFor.SelectedItem as string,
                    Title = tbxTitle.Text,
                    Text = tbxContent.Text,
                    Release = DateTime.Now,
                    Position = cmbxPosition.SelectedItem as string
                });

                if ((MainWindow.workGrid.Children[0] as NewsControl).DataContext is Announcement)
                {
                    MainWindow.workGrid.Children.Clear();
                    MainWindow.workGrid.Children.Add(new NewsControl(new Announcement()));
                }
                Close();
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cmbxPosition.Items.Clear();
            if ("Player".Equals(cmbxFor.SelectedItem as string))
            {
                cmbxPosition.Items.Add("All");
                cmbxPosition.Items.Add("First team");
                cmbxPosition.Items.Add("Seniors");
                cmbxPosition.Items.Add("Juniors");
                cmbxPosition.Items.Add("Membership");
                cmbxPosition.SelectedIndex = 0;
                cmbxPosition.IsEnabled = true;
            }
            else
            {
                cmbxPosition.Items.Add("All");
                cmbxPosition.SelectedIndex = 0;
                cmbxPosition.IsEnabled = false;
            }
        }
    }
}
