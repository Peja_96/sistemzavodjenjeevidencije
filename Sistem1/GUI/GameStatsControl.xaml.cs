﻿using FootApp.DAO;
using FootApp.DTO;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for GameStatsControl.xaml
    /// </summary>
    public partial class GameStatsControl : UserControl
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Game oldGame;
        public GameStatsControl(Game game)
        {
            InitializeComponent();
            tbxName.Text = DTO.Club.FootballClub.Name;
            oldGame = game;
            new CompetitionDAO().GetCompetitions().ForEach(competition => cmbxCompetition.Items.Add(competition));
            new TeamDAO().GetTeams().ForEach(team => cmbxCategory.Items.Add(team));
            //DataContext = DTO.Club.FootballClub;
        }


        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            SaveStats("Save");
        }
        public void SaveStats(string content)
        {
            if (cmbxCompetition.SelectedItem == null || cmbxCategory.SelectedItem == null || string.IsNullOrWhiteSpace(tbxName.Text) || string.IsNullOrWhiteSpace(tbxOpponentName.Text) ||
                string.IsNullOrWhiteSpace(tbxGoals.Text) || string.IsNullOrWhiteSpace(tbxGoalsOp.Text) || string.IsNullOrWhiteSpace(tbxPossesion.Text) ||
                string.IsNullOrWhiteSpace(tbxPossesionOp.Text) || string.IsNullOrWhiteSpace(tbxShots.Text) || string.IsNullOrWhiteSpace(tbxShotsOp.Text) || string.IsNullOrWhiteSpace(tbxFreeKicks.Text) ||
                string.IsNullOrWhiteSpace(tbxFreeKicksOp.Text) || string.IsNullOrWhiteSpace(tbxCorners.Text) || string.IsNullOrWhiteSpace(tbxCornersOp.Text) ||
                string.IsNullOrWhiteSpace(tbxShotsOnGoal.Text) || string.IsNullOrWhiteSpace(tbxShotsOnGoalOp.Text) || string.IsNullOrWhiteSpace(tbxYellowCards.Text) ||
                string.IsNullOrWhiteSpace(tbxYellowCardsOp.Text) || string.IsNullOrWhiteSpace(tbxRedCards.Text) || string.IsNullOrWhiteSpace(tbxRedCardsOp.Text))
            {
                new CustomMessageBox("Error", "Please enter required information.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }

            if (!(Int16.TryParse(tbxGoals.Text, out short goals) && Int16.TryParse(tbxGoalsOp.Text, out short goalsOp)
                 && Int16.TryParse(tbxPossesion.Text, out short possession) && Int16.TryParse(tbxPossesionOp.Text, out short possessionOp)
                 && Int16.TryParse(tbxShots.Text, out short shots) && Int16.TryParse(tbxShotsOp.Text, out short shotsOp)
                 && Int16.TryParse(tbxFreeKicks.Text, out short freeKicks) && Int16.TryParse(tbxFreeKicksOp.Text, out short freeKicksOp)
                 && Int16.TryParse(tbxCorners.Text, out short corners) && Int16.TryParse(tbxCornersOp.Text, out short cornersOp)
                 && Int16.TryParse(tbxShotsOnGoal.Text, out short shotsOnGoal) && Int16.TryParse(tbxShotsOnGoal.Text, out short shotsOnGoalOp)
                && Int16.TryParse(tbxYellowCards.Text, out short yellowCards) && Int16.TryParse(tbxYellowCardsOp.Text, out short yellowCardsOp)
                && Int16.TryParse(tbxRedCards.Text, out short redCards) && Int16.TryParse(tbxRedCardsOp.Text, out short redCardsOp)
                && cmbxCategory.SelectedIndex != -1 && cmbxCompetition.SelectedIndex != -1
                && Int32.TryParse(tbxTicketsSold.Text, out int ticketSold)))
            {
                new CustomMessageBox("Wrong Data", "Please enter correct information.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }

            try
            {
                GameDAO gameDAO = new GameDAO();
                Game newGame = new Game
                {
                    ClubCorners = corners,
                    ClubBallPossession = possession,
                    ClubFreeKicks = freeKicks,
                    ClubRedCards = redCards,
                    ClubResult = goals,
                    ClubShots = shots,
                    ClubShotsOnGoal = shotsOnGoal,
                    ClubYellowCards = yellowCards,
                    IsHost = cbxIsAway.IsChecked.Value,
                    Opponent = tbxOpponentName.Text,
                    OpponentBallPossession = possessionOp,
                    OpponentCorners = cornersOp,
                    OpponentFreeKicks = freeKicksOp,
                    OpponentRedCards = redCardsOp,
                    OpponentResult = goalsOp,
                    OpponentShots = shotsOp,
                    OpponentShotsOnGoal = shotsOnGoalOp,
                    OpponentYellowCards = yellowCardsOp,
                    TotalPenalities = 0,
                    TicketsSold = ticketSold,
                    Round = "Nemamo runde",
                    StartTime = DateTime.Now,
                    CompetitionID = (cmbxCompetition.SelectedItem as Competition).Id
                };
                if (content.Equals("Save"))
                {
                    gameDAO.AddGame(newGame);
                    new CustomMessageBox("Information", "Stats for match succesfully added.", CustomMessageBoxButton.Ok).ShowDialog();
                    new FinancialEntryDAO().AddFinancialEntry(new FinancialEntry
                    {
                        Amount = 0,
                        ClubID = Club.FootballClub.ID,
                        Count = ticketSold,
                        Time = DateTime.Now,
                        Type = FinancialEntryType.Income,
                        Description = "Ticket Earnings - " + Club.FootballClub.Name + " : " + newGame.Opponent
                    });
                }
                else
                {
                    gameDAO.ModifyGame(oldGame, newGame);
                    new CustomMessageBox("Information", "Stats for match succesfully modified.", CustomMessageBoxButton.Ok).ShowDialog();
                    if ((MainWindow.workGrid.Children[0] as NewsControl).DataContext is Game)
                    {
                        MainWindow.workGrid.Children.Clear();
                        MainWindow.workGrid.Children.Add(new NewsControl(new Game()));

                    }
                }

            }
            catch (Exception ex)
            {
                log.Warn(ex);
                new CustomMessageBox("Error", "Error during adding game stats", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }

        }

        private static string oldPossesionStr = "";
        private void TbxPossesion_TextChanged(object sender, TextChangedEventArgs e)
        {

            if (string.IsNullOrWhiteSpace(tbxPossesion.Text))
            {
                if (!string.IsNullOrWhiteSpace(tbxPossesionOp.Text))
                {
                    tbxPossesionOp.Text = "";
                }
                oldPossesionStr = "";
                return;
            }

            Int32.TryParse(tbxPossesionOp.Text, out int clubPossessionOp);


            bool isParsed = Int32.TryParse(tbxPossesion.Text, out int clubPossession);
            if (!isParsed || clubPossession > 100 || clubPossession < 0)
            {
                tbxPossesion.Text = oldPossesionStr;
            }
            else
            {
                if ((clubPossession + clubPossessionOp) != 100)
                    tbxPossesionOp.Text = (100 - clubPossession).ToString();
            }

            oldPossesionStr = tbxPossesion.Text;
        }

        private static string oldPossessionOpStr = "";
        private void TbxPossesionOp_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbxPossesionOp.Text))
            {
                if (!string.IsNullOrWhiteSpace(tbxPossesion.Text))
                {
                    tbxPossesion.Text = "";
                }
                return;
            }


            Int32.TryParse(tbxPossesion.Text, out int clubPossession);


            bool isParsed = Int32.TryParse(tbxPossesionOp.Text, out int clubPossessionOp);
            if (!isParsed || clubPossessionOp > 100 || clubPossessionOp < 0)
            {
                tbxPossesionOp.Text = oldPossessionOpStr;
            }
            else
            {
                if ((clubPossession + clubPossessionOp) != 100)
                    tbxPossesion.Text = (100 - clubPossessionOp).ToString();
            }
            oldPossessionOpStr = tbxPossesionOp.Text;
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
