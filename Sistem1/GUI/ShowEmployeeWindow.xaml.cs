﻿using FootApp.DAO;
using FootApp.DTO;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for ShowEmployeeWindow.xaml
    /// </summary>
    public partial class ShowEmployeeWindow : Window
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        readonly Employee employee;
        public ShowEmployeeWindow(Employee employee)
        {
            InitializeComponent();
            this.employee = employee;
            setLabel();
        }
        private void setLabel()
        {
            tbxName.Text = employee.Name;
            tbxRecordBook.Text = employee.EmploymentRecordBook;
            tbxSurname.Text = employee.Surname;
            tbxDate.Text = employee.DateOfBirth.ToString("dd-MM-yyyy");
            List<String> proffesion = new List<String>(new String[] { "Director", "Sport Director", "Coach", "Assistant Coach"
            ,"Goalkeeper Coach","Nutritionist","Econom","Physiotherapeut","Conditional Trainer","Doctor"});
            proffesion.ForEach(x=>cmbxProffesion.Items.Add(x));
            cmbxProffesion.SelectedItem = employee.Position;
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            if (!(string.IsNullOrWhiteSpace(tbxName.Text) || string.IsNullOrWhiteSpace(tbxSurname.Text) 
                || string.IsNullOrWhiteSpace(tbxRecordBook.Text) || string.IsNullOrWhiteSpace(tbxDate.Text)
                || cmbxProffesion.SelectedIndex == -1) && DateTime.TryParse(tbxDate.Text,out DateTime dateEmp))
            {
                try
                {
                    Employee newEmployee = new Employee
                    {
                        ID = employee.ID,
                        DateOfBirth = dateEmp,
                        EmploymentRecordBook = tbxRecordBook.Text,
                        Name = tbxName.Text,
                        Surname = tbxSurname.Text,
                        Position = cmbxProffesion.SelectedItem.ToString()
                    };
                    new EmployeeDAO().UpdateEmployeeInfo(newEmployee);
                    new CustomMessageBox("Success", "Employee successfuly updated.", CustomMessageBoxButton.Ok).ShowDialog();
                    this.DialogResult = true;
                    this.Close();
                }
                catch (Exception ex)
                {
                    log.Warn(ex);
                    new CustomMessageBox("Error", "Error during updating player", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                }
            }
        }
        private void TbxDate_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            DateMatch(textBox, e);
        }
        private void DateMatch(TextBox textBox, TextCompositionEventArgs e)
        {
            if (e.Text.Equals("-") && textBox.Text.Split('-').Length == 3)
            {
                e.Handled = true;
            }
            else if (textBox.CaretIndex > 9)
                e.Handled = true;
            else if (textBox.CaretIndex == 0)
            {
                Regex regex = new Regex("[^0-3]");
                e.Handled = regex.IsMatch(e.Text);
            }
            else if (textBox.CaretIndex == 1)
            {
                if (textBox.Text[0].ToString().Equals("3"))
                {
                    Regex regex = new Regex("[^0-1]");
                    e.Handled = regex.IsMatch(e.Text);
                }
                else
                {
                    Regex regex = new Regex("[^0-9]");
                    e.Handled = regex.IsMatch(e.Text);
                }

            }
            else if (textBox.CaretIndex == 2 || textBox.CaretIndex == 5)
            {
                Regex regex = new Regex("[^-]");
                e.Handled = regex.IsMatch(e.Text);
            }
            else if (textBox.CaretIndex == 3)
            {
                Regex regex = new Regex("[^0-1]");
                e.Handled = regex.IsMatch(e.Text);
            }
            else if (textBox.CaretIndex == 4)
            {
                if (textBox.Text[3].ToString().Equals("0"))
                {
                    Regex regex = new Regex("[^0-9]");
                    e.Handled = regex.IsMatch(e.Text);
                }
                else
                {
                    Regex regex = new Regex("[^0-2]");
                    e.Handled = regex.IsMatch(e.Text);
                }
            }
            else if (textBox.CaretIndex >= 6 && textBox.CaretIndex <= 9)
            {
                Regex regex = new Regex("[^0-9]");
                e.Handled = regex.IsMatch(e.Text);
            }
        }


        private void TbxDate_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            UndoDelete(textBox, e);
        }
        private void UndoDelete(TextBox textBox, KeyEventArgs e)
        {
            if (e.Key == Key.Back)
            {
                if (textBox.CaretIndex > 0 && textBox.Text[textBox.CaretIndex - 1].ToString().Equals("-"))
                {
                    textBox.CaretIndex -= 1;
                    e.Handled = true;
                }
            }
            else if (e.Key == Key.Delete)
            {
                if (textBox.Text.Length != (textBox.CaretIndex) && textBox.Text[textBox.CaretIndex].ToString().Equals("-"))
                {
                    textBox.CaretIndex += 1;
                    e.Handled = true;
                }
            }
            //else if(e.Key == Key.Z && Keyboard.Modifiers == ModifierKeys.Control)
            //{
            //    textBox.Undo();
            //    Console.WriteLine("ctrl");
            //}
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsValid(((TextBox)sender).Text + e.Text);
        }

        public static bool IsValid(string str)
        {
            int i;
            return int.TryParse(str, out i) && i >= 0;
        }
    }
}
