﻿using FootApp.DAO;
using FootApp.DTO;
using FootApp.FinancialReports;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for CashFlowControl.xaml
    /// </summary>
    public partial class CashFlowControl : UserControl
    {
        public CashFlowControl()
        {
            InitializeComponent();
            MyViewModel expensses = new MyViewModel();
            IncomeExpenses incomeExpensse = new IncomeExpenses().GetIncomeExpensseForCurrentMonth();
            expensses.AddExpensses(incomeExpensse);
            mcExpenses.DataContext = expensses;
            MyViewModel income = new MyViewModel();
            income.AddIncome(incomeExpensse);
            mcIncome.DataContext = income;

        }
        public class MyViewModel
        {
            public ObservableCollection<MyData> Data { get; set; }

            public MyViewModel()
            {

            }
            public void AddExpensses(IncomeExpenses incomeExpensse)
            {
                Data = new ObservableCollection<MyData>
                {
                    new MyData {Name="Players salaries", Count = incomeExpensse.PlayersSalaries},
                    new MyData {Name="Employees salaries", Count = incomeExpensse.EmployeeSalaries},
                    new MyData {Name="Equipment costs", Count = incomeExpensse.EquipmentCosts},
                    new MyData {Name="Total", Count = incomeExpensse.PlayersSalaries+incomeExpensse.EmployeeSalaries+incomeExpensse.EquipmentCosts},
                };
            }
            public void AddIncome(IncomeExpenses incomeExpensse)
            {
                Data = new ObservableCollection<MyData>
                {
                    new MyData {Name="Sponsors Income", Count = incomeExpensse.SponsorsIncome},
                    new MyData {Name="Other Income", Count = incomeExpensse.TicketsIncome},
                    new MyData {Name="Membership Fees", Count =incomeExpensse.MembershipFees },
                    new MyData {Name="Total", Count = incomeExpensse.SponsorsIncome+incomeExpensse.TicketsIncome+incomeExpensse.MembershipFees}
                };
            }
        }
        public class MyData
        {

            public string Name { get; internal set; }
            public decimal Count { get; internal set; }
        }

        private void BntCalculate_Click(object sender, RoutedEventArgs e)
        {
            if (tbxStartDate.Text.Split('-').Length == 3 && tbxEndDate.Text.Split('-').Length == 3)
            {
                if (DateTime.TryParse(tbxStartDate.Text, out DateTime startDate) &&
                    startDate.CompareTo(new DateTime(1901, 1, 1)) > 0 && DateTime.TryParse(tbxEndDate.Text, out DateTime endDate)
                    && endDate.CompareTo(new DateTime(1901, 1, 1)) > 0)
                {
                    if (endDate.CompareTo(startDate) > 0)
                    {
                        IncomeExpenses incomeExpensse = new IncomeExpenses().GetIncomeExpensseForPeriod(startDate, endDate);
                        MyViewModel expensses = new MyViewModel();
                        expensses.AddExpensses(incomeExpensse);
                        mcExpenses.DataContext = expensses;
                        MyViewModel income = new MyViewModel();
                        income.AddIncome(incomeExpensse);
                        mcIncome.DataContext = income;
                    }
                    else
                    {
                        new CustomMessageBox("Error Date","End date is before start date",CustomMessageBoxButton.Ok,CustomMessageBoxIcon.Error).ShowDialog();
                    }
                }
                else
                {
                    new CustomMessageBox("Error Date", "Date is not valid", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                }
            }
            else
            {
                new CustomMessageBox("Error Date", "Date is not valid", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
        }

        private void tbxStartDate_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            DateMatch(textBox,e);
        }

        private void tbxEndDate_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            DateMatch(textBox, e);
        }
        private void DateMatch(TextBox textBox, TextCompositionEventArgs e)
        {
            if (e.Text.Equals("-") && textBox.Text.Split('-').Length == 3)
            {
                e.Handled = true;
            }
            else if (textBox.CaretIndex > 9)
                e.Handled = true;
            else if (textBox.CaretIndex == 0)
            {
                Regex regex = new Regex("[^0-3]");
                e.Handled = regex.IsMatch(e.Text);
            }
            else if (textBox.CaretIndex == 1)
            {
                if (textBox.Text[0].ToString().Equals("3"))
                {
                    Regex regex = new Regex("[^0-1]");
                    e.Handled = regex.IsMatch(e.Text);
                }
                else
                {
                    Regex regex = new Regex("[^0-9]");
                    e.Handled = regex.IsMatch(e.Text);
                }

            }
            else if (textBox.CaretIndex == 2 || textBox.CaretIndex == 5)
            {
                Regex regex = new Regex("[^-]");
                e.Handled = regex.IsMatch(e.Text);
            }
            else if (textBox.CaretIndex == 3)
            {
                Regex regex = new Regex("[^0-1]");
                e.Handled = regex.IsMatch(e.Text);
            }
            else if (textBox.CaretIndex == 4)
            {
                if (textBox.Text[3].ToString().Equals("0"))
                {
                    Regex regex = new Regex("[^0-9]");
                    e.Handled = regex.IsMatch(e.Text);
                }
                else
                {
                    Regex regex = new Regex("[^0-2]");
                    e.Handled = regex.IsMatch(e.Text);
                }
            }
            else if (textBox.CaretIndex >= 6 && textBox.CaretIndex <= 9)
            {
                Regex regex = new Regex("[^0-9]");
                e.Handled = regex.IsMatch(e.Text);
            }
        }


        private void tbxEndDate_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            UndoDelete(textBox, e);
        }
        private void UndoDelete(TextBox textBox, KeyEventArgs e)
        {
            if (e.Key == Key.Back)
            {
                if (textBox.CaretIndex > 0 && textBox.Text[textBox.CaretIndex - 1].ToString().Equals("-"))
                {
                    textBox.CaretIndex -= 1;
                    e.Handled = true;
                }
            }else if (e.Key == Key.Delete )
            {
                if (textBox.Text.Length != (textBox.CaretIndex) && textBox.Text[textBox.CaretIndex].ToString().Equals("-"))
                {
                    textBox.CaretIndex += 1;
                    e.Handled = true;
                }
            }
            //else if(e.Key == Key.Z && Keyboard.Modifiers == ModifierKeys.Control)
            //{
            //    textBox.Undo();
            //    Console.WriteLine("ctrl");
            //}
        }

        private void tbxStartDate_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            UndoDelete(textBox, e);
        }
        private bool FinanceToPdf(String path,DateTime startDate,DateTime endDate)
        {
            PDF.PDFUtil util = new PDF.PDFUtil();
            return util.ExportDataAsPdf(path, util.CreateFinancePDFAsByteArray(startDate, endDate));
        }

        private void bntExport_Click(object sender, RoutedEventArgs e)
        {
            if (tbxStartDate.Text.Split('-').Length == 3 && tbxEndDate.Text.Split('-').Length == 3)
            {
                if (DateTime.TryParse(tbxStartDate.Text, out DateTime startDate) &&
                    startDate.CompareTo(new DateTime(1901, 1, 1)) > 0 && DateTime.TryParse(tbxEndDate.Text, out DateTime endDate)
                    && endDate.CompareTo(new DateTime(1901, 1, 1)) > 0)
                {
                    if (endDate.CompareTo(startDate) > 0)
                    {
                        SaveFileDialog sfd = new SaveFileDialog();
                        sfd.Filter = "Pdf files (*.pdf)|*.pdf";
                        if (sfd.ShowDialog() == true)
                        {
                            FinanceToPdf(sfd.FileName,startDate,endDate);
                        }
                    }
                    else
                    {
                        new CustomMessageBox("Error Date", "End date is before start date", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                    }
                }
                else
                {
                    new CustomMessageBox("Error Date", "Date is not valid", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                }
            }
            else
            {
                new CustomMessageBox("Error Date", "Date is not valid", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
           
        }
    }
}
