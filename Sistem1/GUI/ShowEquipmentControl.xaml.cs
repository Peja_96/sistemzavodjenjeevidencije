﻿using FootApp.DAO;
using FootApp.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for ShowEquipmentControl.xaml
    /// </summary>
    public partial class ShowEquipmentControl : UserControl
    {
        public ShowEquipmentControl()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            LoadEquipment();
        }
        private void LoadEquipment()
        {
            EquipmentTable.Items.Clear();
            List<Equipment> list = new EquipmentDAO().GetEquipment();
            list.ForEach(p => EquipmentTable.Items.Add(p));
        }

        private void tbxSearch_TextChanged(object sender, TextChangedEventArgs e)
        {

            EquipmentTable.Items.Clear();
            List<Equipment> list = new EquipmentDAO().GetEquipmentByType(tbxSearch.Text);
            list.ForEach(p => EquipmentTable.Items.Add(p));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (EquipmentTable.SelectedItem != null)
            {
                bool modifyResult = new RemoveEquipmentWiondow((Equipment)EquipmentTable.SelectedItem).ShowDialog().Value;
                if (modifyResult)
                {
                    LoadEquipment();
                }
            }
        }
    }
}
