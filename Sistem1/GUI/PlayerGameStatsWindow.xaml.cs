﻿using FootApp.DAO;
using FootApp.DTO;
using log4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for PlayerGameStatsWindow.xaml
    /// </summary>
    public partial class PlayerGameStatsWindow : Window
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        Player player;
        static int offset = 0;
        List<Game> games;
        public PlayerGameStatsWindow(Player player)
        {
            this.player = player;
            InitializeComponent();
            LoadPlayer();            
        }
        private void LoadPlayer()
        {
            tbxName.Text = player.Name + " " + player.Surname;
            tbxBirthDate.Text = player.Date;
            tbxCategory.Text = player.Category;
            if (!string.IsNullOrWhiteSpace(player.ImageFilePath))
            {
                try
                {
                    playerImg.Source = new BitmapImage(new Uri(player.ImageFilePath.Replace("%20", " ")));
                }
                catch (Exception ex)
                {
                    log.Warn(ex);
                }
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

    

        private void cmbxGame_DropDownOpened(object sender, EventArgs e)
        {
            ObservableCollection<string> list = new ObservableCollection<string>();
            games = new GameDAO().getTenGames(offset);
            cmbxGame.ItemsSource = games;
        }

       

        private void CmbxGame_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (cmbxGame.SelectedIndex == -1)
            {
                ResetFieldsToDefault();
                ComponentEnabling(false);
                if(!BtnModify.Content.Equals("Modify"))
                    BtnModify.Content = "Modify";
                return;
            }
            Game selectedGame = (Game)cmbxGame.SelectedItem;
            PlayerPerformance playerPerformance = new DAO.PlayerPerformanceDAO().GetPlayerPerformance(selectedGame, player);
            if(playerPerformance == null)
            {
                new CustomMessageBox("Empty", "Please enter statistics for selected game.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                BtnModify.Content = "Add";
                ResetFieldsToDefault();
                ComponentEnabling(true);
                return;
            }
            BtnModify.Content = "Modify";
            TbxGoalsScored.Text = playerPerformance.Goals.ToString();
            TbxAssists.Text = playerPerformance.Assists.ToString();
            TbxFouls.Text = playerPerformance.Fouls.ToString();
            TbxYellowCards.Text = playerPerformance.YellowCards.ToString();
            CbxRedCard.IsChecked = playerPerformance.RedCard;
            TbxTotalPasses.Text = playerPerformance.TotalPasses.ToString();
            TbxSuccessfulPasses.Text = playerPerformance.SuccessfulPasses.ToString();
            TbxTotalShots.Text = playerPerformance.TotalShots.ToString();
            TbxShotsOntarget.Text = playerPerformance.ShotsOnGoal.ToString();
            CbxCleanSheet.IsChecked = true; 
            // TODO dodati iz baze
        }

        private void ResetFieldsToDefault()
        {
            TbxGoalsScored.Text = "";
            TbxAssists.Text = "";
            TbxFouls.Text = "";
            TbxYellowCards.Text = "";
            TbxTotalPasses.Text = "";
            TbxSuccessfulPasses.Text = "";
            TbxTotalShots.Text = "";
            TbxShotsOntarget.Text = "";
            CbxCleanSheet.IsChecked = false;
            CbxRedCard.IsChecked = false;
        }
        private void ComponentEnabling(bool isEnable)
        {
            TbxGoalsScored.IsEnabled = isEnable;
            TbxAssists.IsEnabled = isEnable;
            TbxFouls.IsEnabled = isEnable;
            TbxYellowCards.IsEnabled = isEnable;
            CbxRedCard.IsEnabled = isEnable;
            TbxTotalPasses.IsEnabled = isEnable;
            TbxSuccessfulPasses.IsEnabled = isEnable;
            TbxTotalShots.IsEnabled = isEnable;
            TbxShotsOntarget.IsEnabled = isEnable;
            CbxCleanSheet.IsEnabled = isEnable;
        }

        private void BtnModify_Click(object sender, RoutedEventArgs e)
        {
            if (BtnModify.Content.Equals("Add"))
            {
                if (short.TryParse(TbxGoalsScored.Text, out short goalsScored) && short.TryParse(TbxAssists.Text, out short assists)
                    && short.TryParse(TbxFouls.Text, out short fouls) && short.TryParse(TbxYellowCards.Text, out short yellowCards)
                    && short.TryParse(TbxTotalPasses.Text, out short totalPasses)
                    && short.TryParse(TbxSuccessfulPasses.Text, out short successfulPasses) && short.TryParse(TbxTotalShots.Text, out short totalShots)
                    && short.TryParse(TbxShotsOntarget.Text, out short shotsOnTarget) && goalsScored > -1 && assists > -1 && fouls > -1 &&
                    yellowCards > -1 && totalPasses > -1 && successfulPasses > -1 && totalShots > -1 && shotsOnTarget > -1)
                {
                    Game selectedGame = (Game)cmbxGame.SelectedItem;
                    Competition competition = new DAO.CompetitionDAO().GetCompetitionByID(selectedGame.CompetitionID);
                    PlayerPerformance pp = new PlayerPerformance {
                        Assists = assists,
                        CompetitionID = selectedGame.CompetitionID,
                        Fouls = fouls,
                        Goals = goalsScored,
                        PlayerCategory = player.Category,
                        PlayerID = player.ID,
                        RedCard = CbxRedCard.IsChecked.Value,
                        ShotsOnGoal = shotsOnTarget,
                        StartTime = selectedGame.StartTime,
                        SuccessfulPasses = successfulPasses,
                        TotalPasses = totalPasses,
                        TotalShots = totalShots,
                        Category = competition.Category,
                        YellowCards = yellowCards
                    };
                    new DAO.PlayerPerformanceDAO().AddPerformanceForPlayer(pp);
                    new CustomMessageBox("Information", "Player stats for selected game successfuly added.", CustomMessageBoxButton.Ok).ShowDialog();
                }
                else
                {
                    new CustomMessageBox("Wrong Data", "Please enter required data correctly.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                }
            }
            else if (BtnModify.Content.Equals("Modify")) 
            {
                if(cmbxGame.SelectedIndex != -1)
                { 
                    ComponentEnabling(true);
                    BtnModify.Content = "Save";
                }
            }
            else
            {
                if(short.TryParse(TbxGoalsScored.Text, out short goalsScored) && short.TryParse(TbxAssists.Text, out short assists)
                    && short.TryParse(TbxFouls.Text, out short fouls) && short.TryParse(TbxYellowCards.Text, out short yellowCards)
                    && short.TryParse(TbxTotalPasses.Text, out short totalPasses)
                    && short.TryParse(TbxSuccessfulPasses.Text, out short successfulPasses) && short.TryParse(TbxTotalShots.Text, out short totalShots)
                    && short.TryParse(TbxShotsOntarget.Text, out short shotsOnTarget))
                {
                    Game selectedGame = (Game)cmbxGame.SelectedItem;
                    PlayerPerformance pp = new PlayerPerformance {
                        Assists = assists,
                        Category = player.Category,
                        Fouls = fouls,
                        Goals = goalsScored,
                        PlayerCategory = player.Category,
                        PlayerID = player.ID,
                        RedCard = CbxRedCard.IsChecked.Value,
                        ShotsOnGoal = shotsOnTarget,
                        StartTime = selectedGame.StartTime,
                        SuccessfulPasses = successfulPasses,
                        TotalPasses = totalPasses,
                        TotalShots = totalShots,
                        CompetitionID = selectedGame.CompetitionID,
                        YellowCards = yellowCards
                    };
                    new DAO.PlayerPerformanceDAO().ModifyPlayerPerformance(pp);
                    new CustomMessageBox("Information", "Player stats for selected game successfuly modified.", CustomMessageBoxButton.Ok).ShowDialog();
                }
                else
                {
                    new CustomMessageBox("Wrong data", "Please enter required data correctly.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                }
                ComponentEnabling(false);
                BtnModify.Content = "Modify";
            }
        }

        private void CmbxGame_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            ScrollViewer scrollViewer = e.OriginalSource as ScrollViewer;
            if (e.VerticalChange > 0)
            {
                if (scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
                {
                    offset++;
                    List<Game> loadGames = new GameDAO().getThreeGames(3 * offset);
                    if (loadGames.Count == 0) offset--;
                    else
                    {
                        loadGames.ForEach(x => games.Add(x));
                        cmbxGame.Items.Refresh();
                    }
                }
            }
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsValid(((TextBox)sender).Text + e.Text);
        }

        public static bool IsValid(string str)
        {
            int i;
            return int.TryParse(str, out i) && i >= 0;
        }
    }
}
