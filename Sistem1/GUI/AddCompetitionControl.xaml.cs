﻿using FootApp.DAO;
using FootApp.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for AddCompetitionControl.xaml
    /// </summary>
    public partial class AddCompetitionControl : UserControl
    {
        public AddCompetitionControl()
        {
            InitializeComponent();
        }

        private void Add_Button_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbxTitle.Text) || string.IsNullOrWhiteSpace(tbxDescription.Text) || cmbxType.SelectedIndex == -1 || 
                string.IsNullOrWhiteSpace(tbxStartDate.Text) || string.IsNullOrWhiteSpace(tbxEndDate.Text) || 
                (!cbxFirstTeam.IsChecked.Value && !cbxSeniors.IsChecked.Value && !cbxJuniors.IsChecked.Value && !cbxMembership.IsChecked.Value) ||
                !DateTime.TryParse(tbxStartDate.Text, out DateTime startDate) || !DateTime.TryParse(tbxEndDate.Text, out DateTime endDate))
            {
                new CustomMessageBox("Error", "Please enter correctly required information", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }

            if(startDate >= endDate)
            {
                new CustomMessageBox("Error", "Please enter correctly required information", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }

            List<string> categories = new List<string>();
            if (cbxFirstTeam.IsChecked.Value)
                categories.Add("First");
            if (cbxSeniors.IsChecked.Value)
                categories.Add("Seniors");
            if (cbxJuniors.IsChecked.Value)
                categories.Add("Juniors");
            if (cbxMembership.IsChecked.Value)
                categories.Add("Membership");

            if(cmbxType.SelectedIndex == 0)
            {
                foreach(string category in categories)
                {
                    new LeagueDAO().AddLeague(new League
                    {
                        Category = category,
                        ClubID = Club.FootballClub.ID,
                        Description = tbxDescription.Text,
                        Title = tbxTitle.Text,
                        StartDate = startDate,
                        EndDate = endDate,
                        Type = "League",
                        SeasonID = new SeasonDAO().GetCurrentSeason().Id
                    });
                }
            }
            else
            {
                foreach (string category in categories)
                {
                    new CupDAO().AddCup(new Cup
                    {
                        Category = category,
                        ClubID = Club.FootballClub.ID,
                        Description = tbxDescription.Text,
                        Title = tbxTitle.Text,
                        StartDate = startDate,
                        EndDate = endDate,
                        Type = "League",
                        SeasonID = new SeasonDAO().GetCurrentSeason().Id
                    });
                }
            }
            new CustomMessageBox("Information", "Competition added for all selected categories", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Information).ShowDialog();
            tbxEndDate.Clear();
            tbxStartDate.Clear();
            tbxTitle.Clear();
            tbxDescription.Clear();
            cmbxType.SelectedIndex = -1;
            cbxFirstTeam.IsChecked = false;
            cbxSeniors.IsChecked = false;
            cbxJuniors.IsChecked = false;
            cbxMembership.IsChecked = false;
        }
    }
}
