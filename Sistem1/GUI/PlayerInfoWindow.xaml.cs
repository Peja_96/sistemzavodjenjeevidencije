﻿using FootApp.DTO;
using log4net;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for PlayerInfoWindow.xaml
    /// </summary>
    public partial class PlayerInfoWindow : Window
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        readonly Player player;
        private string newImgPath = "";
        public PlayerInfoWindow(Player p)
        {
            InitializeComponent();
            PopulateFields(p);
            player = p;
        }

        private void PopulateFields(Player p)
        {
            tbxName.Text = p.Name;
            tbxSurname.Text = p.Surname;
            tbxDate.Text = p.DateOfBirth.ToString("d-M-yyyy");
            tbxRegistrationNumber.Text = p.RegistrationNumber;
            tbxHeight.Text = p.Height.ToString();
            cmbxPosition.Items.Add(p.Position);
            cmbxPosition.SelectedItem = p.Position;
            cmbxCategory.Items.Add(p.Category);
            cmbxCategory.SelectedItem = p.Category;
            cmbxCategory.IsEnabled = false;
            cmbxFoot.Items.Add(p.PrimaryFoot);
            cmbxFoot.SelectedItem = p.PrimaryFoot;
            cbxIsBought.IsChecked = p.IsBought;
            cbxIsInjured.IsChecked = p.IsInjured;
            if (!string.IsNullOrWhiteSpace(p.ImageFilePath))
            {
                try
                {
                    this.playerImg.Source = new BitmapImage(new Uri(p.ImageFilePath.Replace("%20", " ")));
                }
                catch (Exception ex)
                {
                    log.Warn(ex);
                }
            }
        }
        private void ButtonAddImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Image files (*.png;*.jpg)|*.png;*.jpg|All files (*.*)|*.*";
            ofd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
            if (ofd.ShowDialog() == true)
            {
                playerImg.Source = new BitmapImage(new Uri(ofd.FileName));
                newImgPath = ofd.FileName;
            }
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbxName.Text) || string.IsNullOrWhiteSpace(tbxSurname.Text) || string.IsNullOrWhiteSpace(tbxDate.Text) ||
                string.IsNullOrWhiteSpace(tbxRegistrationNumber.Text) || string.IsNullOrWhiteSpace(tbxHeight.Text) ||
                cmbxCategory.SelectedIndex == -1 || cmbxFoot.SelectedIndex == -1 || cmbxFoot.SelectedIndex == -1)
            {
                new CustomMessageBox("Enter info", "Please enter required information.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
            else if (tbxDate.Text.Split('-').Length != 3 || !DateTime.TryParse(tbxDate.Text, out DateTime birthDay) || birthDay.CompareTo(new DateTime(1901, 1, 1)) < 0)
            {
                new CustomMessageBox("Wrong Date", "Please enter correct date (day-month-year).", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error);
            }
            else if (!int.TryParse(tbxHeight.Text, out int height) || height < 75 || height > 250)
            {
                new CustomMessageBox("Wrong height", "Please enter correct height in cm.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
            else if (player.Name.Equals(tbxName.Text) && player.Surname.Equals(tbxSurname.Text) && player.RegistrationNumber.Equals(tbxRegistrationNumber.Text) &&
                player.DateOfBirth.CompareTo(DateTime.Parse(tbxDate.Text)) == 0 && player.Height == int.Parse(tbxHeight.Text) && player.Category.Equals(cmbxCategory.SelectedItem.ToString())
                && player.Position.Equals(cmbxPosition.SelectedItem.ToString()) && player.PrimaryFoot.Equals(cmbxFoot.SelectedItem.ToString()) && player.IsBought == cbxIsBought.IsChecked.Value
                && player.IsInjured == cbxIsInjured.IsChecked.Value && (newImgPath.Replace(@"\", "/").CompareTo(player.ImageFilePath.Replace("%20", " ")) == 0))
            {
                new CustomMessageBox("Same Data", "Player data not changed.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
            else
            {
                try
                {
                   // DAO.PlayerDAO pd = new DAO.PlayerDAO();
                    player.Name = tbxName.Text;
                    player.Surname = tbxSurname.Text;
                    player.DateOfBirth = birthDay;
                    player.Position = cmbxPosition.SelectedItem as string;
                    player.RegistrationNumber = tbxRegistrationNumber.Text;
                    player.Height = height;
                    player.PrimaryFoot = cmbxFoot.SelectedItem.ToString();
                    player.IsInjured = cbxIsInjured.IsChecked.Value;
                    player.IsBought = cbxIsBought.IsChecked.Value;
                    player.ImageFilePath = (playerImg.Source as BitmapImage) == null ? AppDomain.CurrentDomain.BaseDirectory + "default-avatar.jpg" : (playerImg.Source as BitmapImage).UriSource.AbsolutePath;
                    new DAO.PlayerDAO().UpdatePlayerInfo(player);
                    new CustomMessageBox("Success", "Player successfuly updated.",CustomMessageBoxButton.Ok).ShowDialog();
                    this.DialogResult = true;
                    this.Close();
                }
                catch (Exception ex)
                {
                    log.Warn(ex);
                    new CustomMessageBox("Error", "Error during updating player", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                }
            }
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsValid(((TextBox)sender).Text + e.Text);
        }

        public static bool IsValid(string str)
        {
            int i;
            return int.TryParse(str, out i) && i > 0;
        }
    }
}
