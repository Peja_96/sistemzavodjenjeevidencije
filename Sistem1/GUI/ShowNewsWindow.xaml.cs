﻿using FootApp.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for ShowNewsWindow.xaml
    /// </summary>
    public partial class ShowNewsWindow : Window
    {
        Announcement announcement;
        public ShowNewsWindow(Announcement announcement, UserControl uc)
        {
            InitializeComponent();
            this.ShowInTaskbar = false;
            this.announcement = announcement;
            DataContext = this.announcement;
            this.Owner = Window.GetWindow(uc);
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
