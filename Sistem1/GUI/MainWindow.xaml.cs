﻿using FootApp.DAO;
using FootApp.DTO;
using FootApp.GUI;
using FootApp.PDF;
using log4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace FootApp
{
    public enum RadioOptions { Option1, Option2 }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        UserControl ControlSaver = null;
        public static Grid workGrid = null;
        public static Grid WorkingGrid2 = null;
        Grid temp;

        public MainWindow()
        {
            log4net.Config.XmlConfigurator.Configure();
            InitializeComponent();
            workGrid = WorkingGrid;
            AddNews();
            Club.FootballClub = new ClubDAO().ReadClubInfo();
            new BudgetDAO().UpdateMonthlyCashFlow();
            DataContext = Club.FootballClub;
            CreateMenuBar();
        }

        private void AddClubInfo()
        {
            WorkingGrid.Children.Add(new ClubInfoControl());
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DatabaseConnection.GetConnection().CloseConnection();
            Application.Current.Shutdown();
        }

        private void OpenMenuButton_Click(object sender, RoutedEventArgs e)
        {
            UserControl temp = null;
            if (WorkingGrid.Children.Count > 0)
            {
                temp = (UserControl)WorkingGrid.Children[0];
                WorkingGrid.Children.Clear();
                
            }
            if (ControlSaver != null)
                WorkingGrid.Children.Add(ControlSaver);
            ControlSaver = temp;
            workGrid.Height = 400;
            workGrid.VerticalAlignment = VerticalAlignment.Top;
            OpenMenuButton.Visibility = Visibility.Collapsed;
            CloseMenuButton.Visibility = Visibility.Visible;
        }

        private void CloseMenuButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UserControl temp = null;
                if (WorkingGrid.Children.Count > 0)
                {
                    temp = (UserControl)WorkingGrid.Children[0];
                    WorkingGrid.Children.Clear();
                }
                if (ControlSaver != null)
                    WorkingGrid.Children.Add(ControlSaver);
                ControlSaver = temp;
            }
            catch (InvalidCastException) { }
            workGrid.VerticalAlignment = VerticalAlignment.Stretch;
            workGrid.Height = Double.NaN; 
            //WorkingGrid.Children.Add(new EventsControl());
            OpenMenuButton.Visibility = Visibility.Visible;
            CloseMenuButton.Visibility = Visibility.Collapsed;

        }
        private void CreateMenuBar()
        {
            var manuPlayers = new List<SubItem>();
            manuPlayers.Add(new SubItem("  Add", new AddPlayerControl()));
            manuPlayers.Add(new SubItem("  Show all", new ShowPlayersControl()));
            var item1 = new ItemMenu("Players", manuPlayers);

            var menuTeams = new List<SubItem>();
            menuTeams.Add(new SubItem("  First", new ShowPlayersControl("First")));
            menuTeams.Add(new SubItem("  Seniors", new ShowPlayersControl("Seniors")));
            menuTeams.Add(new SubItem("  Juniors", new ShowPlayersControl("Juniors")));
            menuTeams.Add(new SubItem("  Membership", new ShowPlayersControl("Membership")));
            var item2 = new ItemMenu("Teams", menuTeams);

            var menuSponsors = new List<SubItem>();
            menuSponsors.Add(new SubItem("  Add", new AddSponsorControl()));
            menuSponsors.Add(new SubItem("  Show all", new ShowSponsorsControl()));
            var item3 = new ItemMenu("Sponsors", menuSponsors);

            var menuEmployees = new List<SubItem>();
            menuEmployees.Add(new SubItem("  Add", new AddEmployeeControl()));
            menuEmployees.Add(new SubItem("  Show all", new ShowEmployeeControl()));
            var item4 = new ItemMenu("Employees", menuEmployees);

            var menuEquipment = new List<SubItem>();
            menuEquipment.Add(new SubItem("  Add", new AddEquipmentControl()));
            menuEquipment.Add(new SubItem("  Show all", new ShowEquipmentControl()));
            var item5 = new ItemMenu("Equipment", menuEquipment);

            var menuContracts = new List<SubItem>();
            menuContracts.Add(new SubItem("  Players contracts", new ContractsControl(new PlayerContract())));
            menuContracts.Add(new SubItem("  Employees contracts", new ContractsControl(new EmployeeContract())));
            menuContracts.Add(new SubItem("  Sponsors contracts", new ContractsControl(new SponsorContract())));
            menuContracts.Add(new SubItem("  Membership fee", new ContractsControl(new MembershipFee())));
            var item6 = new ItemMenu("Contracts", menuContracts);

            var menuCompetitions = new List<SubItem>();
            menuCompetitions.Add(new SubItem("  Add", new AddCompetitionControl()));
            menuCompetitions.Add(new SubItem("  Show all", new CompetitionsControl()));
            var item7 = new ItemMenu("Competitions", menuCompetitions);

            var menuTransfers = new List<SubItem>();
            menuTransfers.Add(new SubItem("Add", new AddTransferControl()));
            menuTransfers.Add(new SubItem("Show all", new ShowTransfersControl()));
            var item8 = new ItemMenu("Transfers", menuTransfers);

            var menuStats = new List<SubItem>();
            menuStats.Add(new SubItem("  Season stats", new SeasonStatsControl()));
            var item9 = new ItemMenu("Statistics", menuStats);

            var menuFinance = new List<SubItem>();
            menuFinance.Add(new SubItem("Cash flow", new CashFlowControl()));
            menuFinance.Add(new SubItem("Club info", new ClubInfoControl()));
            var item10 = new ItemMenu("Finance", menuFinance);

            




            Menu.Children.Add(new MenuBar(item1, this));
            Menu.Children.Add(new MenuBar(item2, this));
            Menu.Children.Add(new MenuBar(item3, this));
            Menu.Children.Add(new MenuBar(item4, this));
            Menu.Children.Add(new MenuBar(item5, this));
            Menu.Children.Add(new MenuBar(item6, this));
            Menu.Children.Add(new MenuBar(item7, this));
            Menu.Children.Add(new MenuBar(item8, this));
            Menu.Children.Add(new MenuBar(item9, this));
            Menu.Children.Add(new MenuBar(item10, this));




        }
        internal void SwitchScreen(object sender)
        {
            var screen = ((UserControl)sender);
            try
            {
                if (screen != null)
                {
                    if (WorkingGrid2 == null)
                    {
                        WorkingGrid.Children.Clear();
                        WorkingGrid.Children.Add(screen);
                    }
                    else
                    {
                        if (checkButton.IsChecked == true)
                        {
                            WorkingGrid.Children.Clear();
                            WorkingGrid.Children.Add(screen);
                        }
                        else
                        {
                            WorkingGrid2.Children.Clear();
                            WorkingGrid2.Children.Add(screen);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Warn(e);
            }
        }

        private void GridMenu_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.Source is Expander exp)
            {
                if (!exp.IsExpanded)
                {
                    foreach (MenuBar mb in Menu.Children)
                    {
                        if (mb.ExpanderMenu != exp)
                            mb.ExpanderMenu.IsExpanded = false;
                        
                    }
                }
            }
        }

        private void Menu_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {

            foreach (MenuBar mb in Menu.Children)
            {
                if (mb != e.Source as MenuBar)
                {
                    mb.ExpanderMenu.IsExpanded = false;
                }
                else
                {
                    mb.ListViewMenu.BringIntoView();
                }
            }


        }

        private void Calendar_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            if (WorkingGrid.Children[0] is NewsControl)
                ((NewsControl)workGrid.Children[0]).getByDate(CustomCalendar.SelectedDate.Value);
            Mouse.Capture(null);
        }
        private void AddNews()
        {
            WorkingGrid.Children.Clear();
            WorkingGrid.Children.Add(new NewsControl(new Announcement()));
        }

        private void bntNews_Click(object sender, RoutedEventArgs e)
        {
            AddNews();
        }

        private void btnGames_Click(object sender, RoutedEventArgs e)
        {
            WorkingGrid.Children.Clear();
            WorkingGrid.Children.Add(new NewsControl(new Game()));
        }

        private void bntAddNews_Click(object sender, RoutedEventArgs e)
        {
            new AddNewsWindow().ShowDialog();
        }

        private void btnMaximise_Click(object sender, RoutedEventArgs e)
        {
            if (WindowState == WindowState.Normal)
            {
                if (CloseMenuButton.IsVisible)
                {
                    Storyboard sb = this.FindResource("CloseMenu") as Storyboard;
                    
                    sb.Begin();
                    WorkingGrid.Height = Double.NaN;
                    WorkingGrid.VerticalAlignment = VerticalAlignment.Stretch;
                }
                
                InitializeWorkingGrid2();
                GridMain.Children.Add(WorkingGrid2);
                GridMain.Children.Remove(checkButton2);
                GridMain.Children.Add(checkButton2);
                WindowState = WindowState.Maximized;
                WorkingGrid.SetValue(Grid.ColumnProperty, 1);
                WorkingGrid.SetValue(Grid.ColumnSpanProperty, 1);
                WorkingGrid.SetValue(Grid.RowProperty, 1);
                WorkingGrid.SetValue(Grid.RowSpanProperty, 1);
                OpenMenuButton.Visibility = Visibility.Collapsed;
                checkButton.Visibility = Visibility.Visible;
                
                checkButton2.Visibility = Visibility.Visible;
                headerGrid.SetValue(Grid.ColumnProperty, 1);
                headerGrid.SetValue(Grid.RowProperty, 0);
                headerGrid.Margin = new Thickness(0);
                GridMaster.Children.Remove(GridMenu);
                temp = new Grid();
                temp.Width = 281;
                temp.Background = (Brush)new BrushConverter().ConvertFrom("#FF42903D");
                temp.SetValue(Grid.ColumnProperty, 0);
                temp.SetValue(Grid.RowSpanProperty, 3);
                GridMenu.Margin = new Thickness(0,0,0,0);
                scrollMenu.Height = Double.NaN;
                temp.Children.Add(GridMenu);
                GridMain.Children.Add(temp);
            }

            else
            {
                WindowState = WindowState.Normal;
                GridMain.Children.Remove(WorkingGrid2);
                WorkingGrid2.Children.Clear();
                WorkingGrid2 = null;
                WorkingGrid.SetValue(Grid.ColumnSpanProperty, 2);
                WorkingGrid.SetValue(Grid.ColumnProperty, 0);
                WorkingGrid.SetValue(Grid.RowSpanProperty, 3);
                WorkingGrid.SetValue(Grid.RowProperty, 0);
                WorkingGrid.Height = Double.NaN;
                WorkingGrid.VerticalAlignment = VerticalAlignment.Stretch;
                AddNews();
                checkButton.Visibility = Visibility.Collapsed;
                checkButton2.Visibility = Visibility.Collapsed;
                OpenMenuButton.Visibility = Visibility.Visible;
                CloseMenuButton.Visibility = Visibility.Collapsed;
                headerGrid.Margin = new Thickness(0, -50, 0, 50);
                headerGrid.SetValue(Grid.RowSpanProperty, 1);
                headerGrid.SetValue(Grid.ColumnSpanProperty, 2);
                headerGrid.SetValue(Grid.ColumnProperty, 0);
                scrollMenu.Height = 400;
                GridMain.Children.Remove(temp);
                temp.Children.Clear();
                temp = null;
                GridMenu.Margin = new Thickness(-281, 0, 0, 0);
                GridMaster.Children.Add(GridMenu);
                
            }
                

        }

        private void InitializeWorkingGrid2()
        {
            WorkingGrid2 = new Grid();
            WorkingGrid2.Background = WorkingGrid.Background;
            WorkingGrid2.SetValue(Grid.ColumnProperty, 1);
            WorkingGrid2.SetValue(Grid.ColumnSpanProperty, 1);
            WorkingGrid2.SetValue(Grid.RowProperty, 2);
            WorkingGrid2.SetValue(Grid.RowSpanProperty, 1);

            
            
        }

       

        private void checkButton_Click(object sender, RoutedEventArgs e)
        {
            if (checkButton.IsChecked == true)
                checkButton.IsChecked = false;
            else
                checkButton.IsChecked = true;
            checkButton2.IsChecked = false;
        }

        private void checkButton2_Click(object sender, RoutedEventArgs e)
        {
            if (checkButton2.IsChecked == true)
                checkButton2.IsChecked = false;
            else
                checkButton2.IsChecked = true;
            checkButton.IsChecked = false;
        }
       
    }
    public class NotBoolenConverter : IValueConverter
    {
        public NotBoolenConverter()
        {
        }

        public object Convert(
            object value,
            Type targetType,
            object parameter,
            CultureInfo culture)
        {
            bool output = (bool)value;
            return !output;
        }

        public object ConvertBack(
            object value,
            Type targetType,
            object parameter,
            CultureInfo culture)
        {
            bool output = (bool)value;
            return !output;
        }
    }
}
