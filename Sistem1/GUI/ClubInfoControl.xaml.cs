﻿using FootApp.DAO;
using FootApp.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for ClubInfoControl.xaml
    /// </summary>
    public partial class ClubInfoControl : UserControl
    {
        public ClubInfoControl()
        {
            InitializeComponent();
            if(Club.FootballClub != null)
                tbxClubName.Text = Club.FootballClub.Name;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (Club.FootballClub != null)
            {
                MembershipFeeValueDAO membershipFeeValueDAO = new MembershipFeeValueDAO();
                tbxClubName.Text = Club.FootballClub.Name;
                tbxNumberOfSeats.Text = Club.FootballClub.StadionSeats + "";
                tbxStadionName.Text = Club.FootballClub.StadionName;
                tbxAddress.Text = Club.FootballClub.Address;
                MembershipFeeValue membershipFeeValue = membershipFeeValueDAO.GetCurrentMembershipFeeValue();
                if (membershipFeeValue == null)
                    tbxMembershipFee.Text = "N/A";
                else
                    tbxMembershipFee.Text = Math.Round(membershipFeeValue.Value, 2) + "";
                if (membershipFeeValue !=  null && !membershipFeeValue.EndDate.ToShortDateString().Equals(new DateTime().ToShortDateString()))
                {
                    lblMembershipFeeValueChange.Content = "Next month membership fee value: " + Math.Round(membershipFeeValueDAO.GetValueForMonth(DateTime.Now.AddMonths(1)), 2);
                }
                else
                    lblMembershipFeeValueChange.Content = string.Empty;
            }
        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            MembershipFeeValueDAO membershipFeeValueDAO = new MembershipFeeValueDAO();
            decimal currMembershipFeeValue = membershipFeeValueDAO.GetValueForMonth(DateTime.Now);
            if (string.IsNullOrWhiteSpace(tbxClubName.Text) || string.IsNullOrWhiteSpace(tbxNumberOfSeats.Text) || string.IsNullOrWhiteSpace(tbxStadionName.Text)
                || string.IsNullOrWhiteSpace(tbxAddress.Text) || !Int32.TryParse(tbxNumberOfSeats.Text, out int numberOfSeats) ||
                string.IsNullOrWhiteSpace(tbxMembershipFee.Text) || !Decimal.TryParse(tbxMembershipFee.Text, out decimal newMembershipFeeValue) || newMembershipFeeValue < 0 )
            {
                new CustomMessageBox("Error", "You entered invalid data ", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }
            if (Club.FootballClub != null && tbxClubName.Text.Equals(Club.FootballClub.Name) && tbxStadionName.Text.Equals(Club.FootballClub.StadionName) &&
                tbxAddress.Text.Equals(Club.FootballClub.Address) && numberOfSeats == Club.FootballClub.StadionSeats && currMembershipFeeValue == newMembershipFeeValue)
            {
                new CustomMessageBox("Error", "You entered same data as before", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }
            if (Club.FootballClub == null)
            {
                Club.FootballClub = new Club();

                Club.FootballClub.Name = tbxClubName.Text;
                Club.FootballClub.StadionName = tbxStadionName.Text;
                Club.FootballClub.StadionSeats = numberOfSeats;
                Club.FootballClub.Address = tbxAddress.Text;
                new ClubDAO().InsertClubInfo();
            }
            else
            {
                Club.FootballClub.Name = tbxClubName.Text;
                Club.FootballClub.StadionName = tbxStadionName.Text;
                Club.FootballClub.StadionSeats = numberOfSeats;
                Club.FootballClub.Address = tbxAddress.Text;
                new ClubDAO().ModifyClubInfo();
            }
            if (membershipFeeValueDAO.GetCurrentMembershipFeeValue() == null)
            {
                
                MembershipFeeValue newValue = new MembershipFeeValue
                {
                    ClubID = Club.FootballClub.ID,
                    StartDate = DateTime.Now,
                    EndDate = new DateTime(),
                    Value = newMembershipFeeValue
                };
                membershipFeeValueDAO.AddNewMembershipFeeValue(newValue);
                new CustomMessageBox("Information", "Club data successfully changed (including membership fee).", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Information).ShowDialog();
            }
            else if (currMembershipFeeValue != newMembershipFeeValue)
            {
                DateTime startDate = DateTime.Now.AddMonths(1);
                startDate = new DateTime(startDate.Year, startDate.Month, 1);
                MembershipFeeValue newValue = new MembershipFeeValue
                {
                    ClubID = Club.FootballClub.ID,
                    StartDate = startDate,
                    EndDate = new DateTime(),
                    Value = newMembershipFeeValue
                };

                membershipFeeValueDAO.AddNewMembershipFeeValue(newValue);
                new CustomMessageBox("Information", "Club data successfully changed (including membership fee).", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Information).ShowDialog();
            }
            else
                new CustomMessageBox("Information", "Club data successfully changed.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Information).ShowDialog();

            UserControl_Loaded(sender, e);
        }

        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {
            if (new MembershipFeeValueDAO().ResetMembershipFeeValueChange().Value)
            {
                new CustomMessageBox("Information", "Membership Fee change removed.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Information).ShowDialog();
                UserControl_Loaded(sender, e);
            }
            
        }
    }
}
