﻿using FootApp.DTO;
using FootApp.GUI;
using log4net;
using Microsoft.Win32;
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Linq;

namespace FootApp
{
   
    public partial class AddPlayerControl : UserControl
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public AddPlayerControl()
        {
            InitializeComponent();
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(tbxName.Text) || string.IsNullOrEmpty(tbxSurname.Text) ||
                    string.IsNullOrEmpty(tbxDate.Text) || string.IsNullOrEmpty(tbxRegistrationNumber.Text) 
                    || cmbxPosition.SelectedIndex.Equals(-1) || string.IsNullOrEmpty(tbxHeight.Text) ||
                    cmbxCategory.SelectedIndex.Equals(-1) || cmbxFoot.SelectedIndex.Equals(-1))
            {
                new CustomMessageBox("Wrong Data", "Please enter required information.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
            else
            {
                if (tbxDate.Text.Split('-').Length == 3 && DateTime.TryParse(tbxDate.Text, out DateTime birthDay) &&
                    birthDay.CompareTo(new DateTime(1901, 1, 1)) > 0 && int.TryParse(tbxHeight.Text, out int height) && height > 75)
                {
                    if (tbxRegistrationNumber.Text.All(char.IsDigit))
                    {

                        try
                        {
                            Player newPlayer = new Player
                            {
                                ID = 0,
                                Name = tbxName.Text,
                                Surname = tbxSurname.Text,
                                DateOfBirth = birthDay,
                                Position = cmbxPosition.Text,
                                RegistrationNumber = tbxRegistrationNumber.Text,
                                IsInjured = cbxIsInjured.IsChecked.Value,
                                IsBought = cbxIsBought.IsChecked.Value,
                                DateJoined = DateTime.Now,
                                Height = height,
                                Category = cmbxCategory.Text,
                                PrimaryFoot = cmbxFoot.Text,
                                ImageFilePath = (playerImg.Source as BitmapImage) == null ? AppDomain.CurrentDomain.BaseDirectory + "default-avatar.jpg" : (playerImg.Source as BitmapImage).UriSource.AbsolutePath
                            };

                            if ("Membership".Equals(cmbxCategory.Text))
                            {
                                new DAO.PlayerDAO().AddPlayer(newPlayer);
                                new CustomMessageBox("Information", "Player successfuly added.", CustomMessageBoxButton.Ok).ShowDialog();
                                EmptyFields();
                            }
                            else { 
                            ContractWindow cw = new ContractWindow(newPlayer);
                                if (cw.ShowDialog().Value)
                                {
                                    EmptyFields();
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            log.Warn(ex);
                            new CustomMessageBox("Error", "Error during adding player", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                        }
                    }
                    else
                    {
                        new CustomMessageBox("Wrong data", "Please enter registration number properly.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();

                    }
                }
                else
                {
                    new CustomMessageBox("Wrong data", "Please enter height and date properly (dd-mm-yyyy).", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                }
            }
        }

        private void EmptyFields() 
        {
            tbxName.Text = "";
            tbxSurname.Text = "";
            tbxDate.Text = "";
            tbxRegistrationNumber.Text = "";
            tbxHeight.Text = "";
            cmbxPosition.SelectedIndex = -1;
            cmbxFoot.SelectedIndex = -1;
            cmbxCategory.SelectedIndex = -1;
        }
        private void ButtonAddImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Image files (*.png;*.jpg)|*.png;*.jpg|All files (*.*)|*.*";
            ofd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
            if (ofd.ShowDialog() == true)
            {
                playerImg.Source = new BitmapImage(new Uri(ofd.FileName));
            }
        }

        private void TbxDate_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            DateMatch(textBox, e);
        }
        private void DateMatch(TextBox textBox, TextCompositionEventArgs e)
        {
            if (e.Text.Equals("-") && textBox.Text.Split('-').Length == 3)
            {
                e.Handled = true;
            }
            else if (textBox.CaretIndex > 9)
                e.Handled = true;
            else if (textBox.CaretIndex == 0)
            {
                Regex regex = new Regex("[^0-3]");
                e.Handled = regex.IsMatch(e.Text);
            }
            else if (textBox.CaretIndex == 1)
            {
                if (textBox.Text[0].ToString().Equals("3"))
                {
                    Regex regex = new Regex("[^0-1]");
                    e.Handled = regex.IsMatch(e.Text);
                }
                else
                {
                    Regex regex = new Regex("[^0-9]");
                    e.Handled = regex.IsMatch(e.Text);
                }

            }
            else if (textBox.CaretIndex == 2 || textBox.CaretIndex == 5)
            {
                Regex regex = new Regex("[^-]");
                e.Handled = regex.IsMatch(e.Text);
            }
            else if (textBox.CaretIndex == 3)
            {
                Regex regex = new Regex("[^0-1]");
                e.Handled = regex.IsMatch(e.Text);
            }
            else if (textBox.CaretIndex == 4)
            {
                if (textBox.Text[3].ToString().Equals("0"))
                {
                    Regex regex = new Regex("[^0-9]");
                    e.Handled = regex.IsMatch(e.Text);
                }
                else
                {
                    Regex regex = new Regex("[^0-2]");
                    e.Handled = regex.IsMatch(e.Text);
                }
            }
            else if (textBox.CaretIndex >= 6 && textBox.CaretIndex <= 9)
            {
                Regex regex = new Regex("[^0-9]");
                e.Handled = regex.IsMatch(e.Text);
            }
        }


        private void TbxDate_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            UndoDelete(textBox, e);
        }
        private void UndoDelete(TextBox textBox, KeyEventArgs e)
        {
            if (e.Key == Key.Back)
            {
                if (textBox.CaretIndex > 0 && textBox.Text[textBox.CaretIndex - 1].ToString().Equals("-"))
                {
                    textBox.CaretIndex -= 1;
                    e.Handled = true;
                }
            }
            else if (e.Key == Key.Delete)
            {
                if (textBox.Text.Length != (textBox.CaretIndex) && textBox.Text[textBox.CaretIndex].ToString().Equals("-"))
                {
                    textBox.CaretIndex += 1;
                    e.Handled = true;
                }
            }
            //else if(e.Key == Key.Z && Keyboard.Modifiers == ModifierKeys.Control)
            //{
            //    textBox.Undo();
            //    Console.WriteLine("ctrl");
            //}
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsValid(((TextBox)sender).Text + e.Text);
        }

        public static bool IsValid(string str)
        {
            int i;
            return int.TryParse(str, out i) && i > 0;
        }
    }
}
