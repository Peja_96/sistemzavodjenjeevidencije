﻿using FootApp.DAO;
using FootApp.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for AddCompetitionWindow.xaml
    /// </summary>
    public partial class CompetitionInfoWindow : Window
    {
        private Competition competition;
        public CompetitionInfoWindow(Competition competition)
        {
            this.competition = competition;
            InitializeComponent();
            FillTextBoxes();
        }

        private void FillTextBoxes()
        {
            tbxTitle.Text = competition.Title;
            tbxDescription.Text = competition.Description;
            tbxStartDate.Text = competition.StartDate.Day + "-" + competition.StartDate.Month + "-" + competition.StartDate.Year;
            tbxEndDate.Text = competition.EndDate.Day + "-" + competition.EndDate.Month + "-" + competition.EndDate.Year;
            tbxCategory.Text = competition.Category;
            tbxCategory.Focusable = false;
            if (competition is League)
                tbxType.Text = "League";
            else
                tbxType.Text = "Cup";
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void Modify_Button_Click(object sender, RoutedEventArgs e)
        {
            if(string.IsNullOrWhiteSpace(tbxTitle.Text) || string.IsNullOrWhiteSpace(tbxDescription.Text) || string.IsNullOrWhiteSpace(tbxStartDate.Text)
                || string.IsNullOrWhiteSpace(tbxEndDate.Text) || !DateTime.TryParse(tbxStartDate.Text, out DateTime startDate) || !DateTime.TryParse(tbxEndDate.Text, out DateTime endDate))
            {
                new CustomMessageBox("Error", "Please provide required information", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }

            if (startDate >= endDate)
            {
                new CustomMessageBox("Error", "Please enter correctly required information", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }

            if (competition.Title.Equals(tbxTitle.Text) && competition.Description.Equals(tbxDescription.Text) && 
                competition.StartDate.ToLongDateString().Equals(startDate.ToLongDateString()) &&
                competition.EndDate.ToLongDateString().Equals(endDate.ToLongDateString()))
            {
                new CustomMessageBox("Error", "No changes in selected competition", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }

            competition.Title = tbxTitle.Text;
            competition.Description = tbxDescription.Text;
            competition.StartDate = startDate;
            competition.EndDate = endDate;
            new CompetitionDAO().ModifyCompetitionInfo(competition);
            new CustomMessageBox("Information", "Competition information successfully changed", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Information).ShowDialog();
            DialogResult = true;
            Close();
        }
    }
}
