﻿using FootApp.DTO;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for ContractWindow.xaml
    /// </summary>
    public partial class ContractWindow : Window
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        Person person = null;
        Sponsor sponsor = null;
        public ContractWindow(Person person)
        {
            InitializeComponent();
            this.person = person;
            LoadInfo();
        }

        public ContractWindow(Sponsor sponsor)
        {
            InitializeComponent();
            this.sponsor = sponsor;
            LoadInfo();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        private void LoadInfo()
        {
            if (person is Player)
                title_Label.Content = "Player contract";
            else if (person is Employee)
                title_Label.Content = "Employee contract";
            else
                title_Label.Content = "Sponsor contract";
        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbxStart.Text) && !string.IsNullOrWhiteSpace(tbxEnd.Text) && !string.IsNullOrWhiteSpace(tbxValue.Text))
            {
                if (tbxStart.Text.Split('-').Length == 3 && DateTime.TryParse(tbxStart.Text, out DateTime contractStart) &&
                    contractStart.CompareTo(new DateTime(1901, 1, 1)) > 0 &&
                    tbxEnd.Text.Split('-').Length == 3 && DateTime.TryParse(tbxEnd.Text, out DateTime contractEnd) &&
                    contractEnd.CompareTo(contractStart) > 0 && int.TryParse(tbxValue.Text, out int value) && value > -1)
                {
                    if (person is Player)
                    {
                        Player p = (Player)person;
                        new DAO.PlayerDAO().AddPlayer(p);
                        new DAO.PlayerContractDAO().AddPlayerContract(new PlayerContract
                        {
                            Name = p.Name,
                            Surname = p.Surname,
                            PlayerID = p.ID,
                            Salary = value,
                            StartDate = contractStart,
                            EndDate = contractEnd
                        });
                        new CustomMessageBox("Success", "Player " + p.Name + " " + p.Surname + " succesfully added", CustomMessageBoxButton.Ok).ShowDialog();
                        DialogResult = true;
                        Close();
                    }
                    else if (person is Employee)
                    {
                        Employee emp = (Employee)person;
                        new DAO.EmployeeDAO().AddEmployee(emp);
                        try
                        {
                            new DAO.EmployeeContractDAO().AddEmployeeContract(new EmployeeContract
                            {
                                Name = emp.Name,
                                Surname = emp.Surname,
                                EmployeeID = emp.ID,
                                Salary = value,
                                StartDate = contractStart,
                                EndDate = contractEnd
                            });
                            new CustomMessageBox("Success", "Employee " + emp.Name + " " + emp.Surname + " succesfully added", CustomMessageBoxButton.Ok).ShowDialog();
                        }
                        catch (Exception ex)
                        {
                            log.Warn(ex);
                            new CustomMessageBox("Error", "Error addingemployee contract.", CustomMessageBoxButton.Ok).ShowDialog();
                        }
                        DialogResult = true;
                        Close();
                    }
                    else //sponsor
                    {
                        try
                        {
                            new DAO.SponsorDAO().AddSponsor(sponsor);
                            new DAO.SponsorContractDAO().AddSponsorContract(new SponsorContract
                            {
                                Name = sponsor.Name,
                                SponsorID = sponsor.Id,
                                Investment = value,
                                StartDate = contractStart,
                                EndDate = contractEnd
                            });
                            new CustomMessageBox("Success", "Sponsor " + sponsor.Name + " succesfully added", CustomMessageBoxButton.Ok).ShowDialog();
                            DialogResult = true;
                            Close();
                        } 
                        catch(Exception ex)
                        {
                            log.Warn(ex);
                            new CustomMessageBox("Error", "Error adding sponsor.");
                        }
                    }
                }
                else
                {
                    new CustomMessageBox("Wrong data", "Please enter correct data.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                }
            }
            else
            {
                new CustomMessageBox("Enter data", "Please enter required data.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
        }

        private void DateMatch(TextBox textBox, TextCompositionEventArgs e)
        {
            if (e.Text.Equals("-") && textBox.Text.Split('-').Length == 3)
            {
                e.Handled = true;
            }
            else if (textBox.CaretIndex > 9)
                e.Handled = true;
            else if (textBox.CaretIndex == 0)
            {
                Regex regex = new Regex("[^0-3]");
                e.Handled = regex.IsMatch(e.Text);
            }
            else if (textBox.CaretIndex == 1)
            {
                if (textBox.Text[0].ToString().Equals("3"))
                {
                    Regex regex = new Regex("[^0-1]");
                    e.Handled = regex.IsMatch(e.Text);
                }
                else
                {
                    Regex regex = new Regex("[^0-9]");
                    e.Handled = regex.IsMatch(e.Text);
                }

            }
            else if (textBox.CaretIndex == 2 || textBox.CaretIndex == 5)
            {
                Regex regex = new Regex("[^-]");
                e.Handled = regex.IsMatch(e.Text);
            }
            else if (textBox.CaretIndex == 3)
            {
                Regex regex = new Regex("[^0-1]");
                e.Handled = regex.IsMatch(e.Text);
            }
            else if (textBox.CaretIndex == 4)
            {
                if (textBox.Text[3].ToString().Equals("0"))
                {
                    Regex regex = new Regex("[^0-9]");
                    e.Handled = regex.IsMatch(e.Text);
                }
                else
                {
                    Regex regex = new Regex("[^0-2]");
                    e.Handled = regex.IsMatch(e.Text);
                }
            }
            else if (textBox.CaretIndex >= 6 && textBox.CaretIndex <= 9)
            {
                Regex regex = new Regex("[^0-9]");
                e.Handled = regex.IsMatch(e.Text);
            }
        }

        private void TbxDate_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            UndoDelete(textBox, e);
        }
        private void UndoDelete(TextBox textBox, KeyEventArgs e)
        {
            if (e.Key == Key.Back)
            {
                if (textBox.CaretIndex > 0 && textBox.Text[textBox.CaretIndex - 1].ToString().Equals("-"))
                {
                    textBox.CaretIndex -= 1;
                    e.Handled = true;
                }
            }
            else if (e.Key == Key.Delete)
            {
                if (textBox.Text.Length != (textBox.CaretIndex) && textBox.Text[textBox.CaretIndex].ToString().Equals("-"))
                {
                    textBox.CaretIndex += 1;
                    e.Handled = true;
                }
            }
            //else if(e.Key == Key.Z && Keyboard.Modifiers == ModifierKeys.Control)
            //{
            //    textBox.Undo();
            //    Console.WriteLine("ctrl");
            //}
        }

        private void TbxDate_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            DateMatch(textBox, e);
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsValid(((TextBox)sender).Text + e.Text);
        }

        public static bool IsValid(string str)
        {
            int i;
            return int.TryParse(str, out i) && i > 0;
        }

    }
}
