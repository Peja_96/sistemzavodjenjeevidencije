﻿using FootApp.DAO;
using FootApp.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for CompetitionsControl.xaml
    /// </summary>
    public partial class CompetitionsControl : UserControl
    {
        private List<Season> seasons;
        private int selectedIndex;
        bool formLoaded = false;
        public CompetitionsControl()
        {
            InitializeComponent();
        }

        private void ButtonInfo_Click(object sender, RoutedEventArgs e)
        {
            if (CompetitionTable.SelectedItem == null)
            {
                new CustomMessageBox("Error", "You need to select competition", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }
            bool result = new CompetitionInfoWindow(CompetitionTable.SelectedItem as Competition).ShowDialog().Value;
            if (result)
                TbxSearch_TextChanged(sender, null);
        }

        private void ButtonRemove_Click(object sender, RoutedEventArgs e)
        {
            if (CompetitionTable.SelectedItem == null)
            {
                new CustomMessageBox("Error", "Please select competition to remove", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }

            Competition competition = CompetitionTable.SelectedItem as Competition;
            if (!new GameDAO().CompetitionHasGame(competition))
            {
                if (competition is Cup)
                    new CupDAO().DeleteCup(competition as Cup);
                else
                    new LeagueDAO().DeleteLeague(competition as League);
                new CustomMessageBox("Information", "Competition successfully removed.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Information).ShowDialog();
                TbxSearch_TextChanged(sender, null);
                return;
            }
            new CustomMessageBox("Error", "Selected competition has recorded games", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();

        }

        private void ButtonAddGame_Click(object sender, RoutedEventArgs e)
        {
            if (CompetitionTable.SelectedItem == null)
            {
                new CustomMessageBox("Error", "Please select competition to add game for", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }

            new ShowGameWindow(CompetitionTable.SelectedItem as Competition, 
                                new TeamDAO().GetTeamByCategory(((CompetitionTable.SelectedItem) as Competition).Category)).ShowDialog();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            seasons = new SeasonDAO().GetSeasons();
            SetCheckBoxes();
            SetSeasonsPart();
            FillCompetitionTable();
            formLoaded = true;
        }
        private void SetCheckBoxes()
        {
            cbxFirstTeam.IsChecked = true;
            cbxJuniors.IsChecked = true;
            cbxMembership.IsChecked = true;
            cbxSeniors.IsChecked = true;
        }
        private void FillCompetitionTable()
        {
            CompetitionTable.Items.Clear();
            List<Competition> competitions = new List<Competition>();
            if (selectedIndex >= 0)
            {
                if (cbxFirstTeam.IsChecked.Value)
                    competitions.AddRange(new CompetitionDAO().GetCompetitionsBySeasonAndTeam(seasons[selectedIndex], new TeamDAO().GetTeamByCategory("First")));
                if (cbxSeniors.IsChecked.Value)
                    competitions.AddRange(new CompetitionDAO().GetCompetitionsBySeasonAndTeam(seasons[selectedIndex], new TeamDAO().GetTeamByCategory("Seniors")));
                if (cbxJuniors.IsChecked.Value)
                    competitions.AddRange(new CompetitionDAO().GetCompetitionsBySeasonAndTeam(seasons[selectedIndex], new TeamDAO().GetTeamByCategory("Juniors")));
                if (cbxMembership.IsChecked.Value)
                    competitions.AddRange(new CompetitionDAO().GetCompetitionsBySeasonAndTeam(seasons[selectedIndex], new TeamDAO().GetTeamByCategory("Membership")));
                competitions.ForEach(competition => CompetitionTable.Items.Add(competition));
            }
        }

        private void NextSeason(object sender, RoutedEventArgs e)
        {
            if (!leftButton.IsEnabled) leftButton.IsEnabled = true;
            selectedIndex++;
            seasonTxtBox.Text = "Season " + seasons[selectedIndex].StartDate.ToString("yy") + "/" + seasons[selectedIndex].EndDate.ToString("yy");
            if (selectedIndex.Equals(seasons.Count - 1))
                rightButton.IsEnabled = false;
            TbxSearch_TextChanged(sender, null);
        }
        private void SetSeasonsPart()
        {
            rightButton.IsEnabled = false;
            if (seasons.Count == 1)
            {
                leftButton.IsEnabled = false;
            }
            else if (seasons.Count == 0)
            {
                seasonTxtBox.Text = "No Seasons";
                selectedIndex = -1;
                leftButton.IsEnabled = false;
                return;
            }
            selectedIndex = seasons.IndexOf(new SeasonDAO().GetCurrentSeason());
            if (selectedIndex < seasons.Count - 1 && selectedIndex != -1)
                rightButton.IsEnabled = true;
            seasonTxtBox.Text = "Season " + seasons[selectedIndex].StartDate.ToString("yy") + "/" + seasons[selectedIndex].EndDate.ToString("yy");
        }

        private void PreviousSeason(object sender, RoutedEventArgs e)
        {
            if (!rightButton.IsEnabled)
                rightButton.IsEnabled = true;
            selectedIndex--;
            seasonTxtBox.Text = "Season " + seasons[selectedIndex].StartDate.ToString("yy") + "/" + seasons[selectedIndex].EndDate.ToString("yy");
            if (selectedIndex.Equals(0))
                leftButton.IsEnabled = false;
            TbxSearch_TextChanged(sender, null);
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (formLoaded)
            {
                if (!cbxMembership.IsChecked.Value && !cbxFirstTeam.IsChecked.Value && !cbxJuniors.IsChecked.Value && !cbxSeniors.IsChecked.Value)
                    SetCheckBoxes();
                TbxSearch_TextChanged(sender, null);
            }
        }

        private void TbxSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbxSearch.Text))
                FillCompetitionTable();
            else
            {
                CompetitionTable.Items.Clear();
                List<Competition> competitions = new CompetitionDAO().GetCompetitionsByTitle(tbxSearch.Text);
                competitions = competitions.FindAll(c => c.SeasonID == seasons[selectedIndex].Id);
                if (!cbxFirstTeam.IsChecked.Value)
                    competitions.RemoveAll(c => c.Category.Equals("First"));
                if (!cbxSeniors.IsChecked.Value)
                    competitions.RemoveAll(c => c.Category.Equals("Seniors"));
                if (!cbxJuniors.IsChecked.Value)
                    competitions.RemoveAll(c => c.Category.Equals("Juniors"));
                if (!cbxMembership.IsChecked.Value)
                    competitions.RemoveAll(c => c.Category.Equals("Membership"));
                competitions.ForEach(c => CompetitionTable.Items.Add(c));
            }
        }

        private void ButtonAddSeason_Click(object sender, RoutedEventArgs e)
        {
            SeasonDAO seasonDAO = new SeasonDAO();
            Season currentSeason = seasonDAO.GetCurrentSeason();
            Season season = seasonDAO.GetSeasonByDate(currentSeason.StartDate.Year + 1, currentSeason.EndDate.Year + 1);
            if (season == null)
            {
                seasonDAO.AddSeason(new Season
                {
                    EndDate = currentSeason.EndDate.AddYears(1),
                    StartDate = currentSeason.StartDate.AddYears(1),
                });
                new CustomMessageBox("Information", "Season successfully added.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Information).ShowDialog();
            }
            else
                new CustomMessageBox("Error", "Next season already added.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
        }
    }
}
