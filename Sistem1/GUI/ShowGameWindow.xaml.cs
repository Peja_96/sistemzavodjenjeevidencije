﻿using FootApp.DAO;
using FootApp.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for ShowGameWindow.xaml
    /// </summary>
    public partial class ShowGameWindow : Window 
    {
        private Game game;
        private GameStatsControl gameControl;
        public Competition selectedCompetition;
        public Team selectedTeam;
        string action = "Save";
        public ShowGameWindow(Competition competition, Team team)
        {
            
            InitializeComponent();
            game = null;
            gameControl = new GameStatsControl(game);
            border.Child = gameControl;
            gameControl.ButtonSave.Visibility = Visibility.Collapsed;
            gameControl.cmbxCompetition.SelectedIndex = gameControl.cmbxCompetition.Items.Cast<Competition>().ToList().IndexOf(competition);
            gameControl.cmbxCategory.SelectedIndex = gameControl.cmbxCategory.Items.Cast<Team>().ToList().IndexOf(team);
            this.Topmost = true;
        }
        public ShowGameWindow(Game game)
        {
            InitializeComponent();
            this.game = game;
            gameControl = new GameStatsControl(game);
            gameControl.ButtonSave.Visibility = Visibility.Collapsed;
            SetGrid();
            action = "Modify";
        }
        private void SetGrid()
        {
            border.Child = gameControl;
            FillControl();
           // DisableFocus();
        }
        private void FillControl()
        {
            gameControl.tbxOpponentName.Text = game.Opponent;
            gameControl.tbxGoals.Text = game.ClubResult + "";
            gameControl.tbxGoalsOp.Text = game.OpponentResult + "";
            gameControl.tbxShots.Text = game.ClubShots + "";
            gameControl.tbxShotsOp.Text = game.OpponentShots + "";
            gameControl.tbxShotsOnGoal.Text = game.ClubShotsOnGoal + "";
            gameControl.tbxShotsOnGoalOp.Text = game.OpponentShotsOnGoal + "";
            gameControl.tbxYellowCards.Text = game.ClubYellowCards + "";
            gameControl.tbxYellowCardsOp.Text = game.OpponentYellowCards + "";
            gameControl.tbxRedCards.Text = game.ClubRedCards + "";
            gameControl.tbxRedCardsOp.Text = game.OpponentRedCards + "";
            gameControl.tbxPossesion.Text = game.ClubBallPossession + "";
            gameControl.tbxCorners.Text = game.ClubCorners + "";
            gameControl.tbxCornersOp.Text = game.OpponentCorners + "";
            gameControl.tbxFreeKicks.Text = game.ClubFreeKicks + "";
            gameControl.tbxFreeKicksOp.Text = game.OpponentFreeKicks + "";
            gameControl.tbxPossesionOp.Text = game.OpponentBallPossession + "";
            gameControl.cbxIsAway.IsChecked = game.IsHost;
            Competition competition = new CompetitionDAO().GetCompetitionByID(game.CompetitionID);
            gameControl.cmbxCompetition.SelectedIndex = gameControl.cmbxCompetition.Items.Cast<Competition>().ToList().IndexOf(competition);
            gameControl.tbxTicketsSold.Text = game.TicketsSold + "";
            gameControl.cmbxCategory.SelectedIndex = gameControl.cmbxCategory.Items.Cast<Team>().ToList().IndexOf(new TeamDAO().GetTeamByCategory(competition.Category));
            gameControl.ButtonSave.Visibility = Visibility.Hidden;
        }
        
        private void DisableFocus() { 
            gameControl.tbxOpponentName.Focusable = false;
            gameControl.tbxGoals.Focusable = false;
            gameControl.tbxGoalsOp.Focusable = false;
            gameControl.tbxShots.Focusable = false;
            gameControl.tbxShotsOp.Focusable = false;
            gameControl.tbxShotsOnGoal.Focusable = false;
            gameControl.tbxShotsOnGoalOp.Focusable = false;
            gameControl.tbxYellowCards.Focusable = false;
            gameControl.tbxYellowCardsOp.Focusable = false;
            gameControl.tbxRedCards.Focusable = false;
            gameControl.tbxRedCardsOp.Focusable = false;
            gameControl.tbxPossesion.Focusable = false;
            gameControl.tbxPossesionOp.Focusable = false;
            gameControl.cbxIsAway.Focusable = false;
            gameControl.cbxIsAway.IsEnabled = false;
            gameControl.tbxTicketsSold.Focusable = false;
            gameControl.cmbxCompetition.Focusable = false;
            gameControl.tbxName.Focusable = false;
            gameControl.tbxCorners.Focusable = false;
            gameControl.tbxCornersOp.Focusable = false;
            gameControl.tbxFreeKicks.Focusable = false;
            gameControl.tbxFreeKicksOp.Focusable = false;
           
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            gameControl.SaveStats(action);
            Close();
        }
    }
}
