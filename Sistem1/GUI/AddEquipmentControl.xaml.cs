﻿using FootApp.DAO;
using FootApp.DTO;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for AddEquipmentControl.xaml
    /// </summary>
    public partial class AddEquipmentControl : UserControl
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public AddEquipmentControl()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(tbxPrice.Text) || string.IsNullOrEmpty(tbxType.Text) ||
                string.IsNullOrEmpty(tbxQuantity.Text) || !Int32.TryParse(tbxQuantity.Text, out int quantity)
                || !Int32.TryParse(tbxPrice.Text, out int price) || !tbxPrice.Text.All(char.IsDigit) || !tbxQuantity.Text.All(char.IsDigit))
            {
                new CustomMessageBox("Wrong Data", "Please enter required information correctly.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
            else
            {
                try {
                    EquipmentDAO equipmentDAO = new EquipmentDAO();
                    Equipment equipment = equipmentDAO.GetEquipmentByTypeEqual(tbxType.Text);
                    if ( equipment== null)
                    {
                        equipmentDAO.AddEquipment(new Equipment
                        {
                            EquipmentType = tbxType.Text,
                            Quantity = quantity,
                            ClubID = Club.FootballClub.ID

                        });
                        new FinancialEntryDAO().AddFinancialEntry(new FinancialEntry
                        {
                            Amount = price,
                            ClubID = Club.FootballClub.ID,
                            Count = quantity,
                            Description = "Equipment purchase - " + tbxType.Text,
                            Time = DateTime.Now,
                            Type = FinancialEntryType.Expense
                        });
                        new CustomMessageBox("Success", tbxType.Text + " succesfully added", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Information).ShowDialog();
                        EmptyFields();
                    }
                    else
                    {
                        equipment.Quantity += quantity;
                        equipmentDAO.UpdateEquipment(equipment);
                      
                        new CustomMessageBox("Success", tbxType.Text + " succesfully updated", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Information).ShowDialog();
                        EmptyFields();
                    }
                }
                catch (Exception ex)
                {
                    log.Warn(ex);
                    new CustomMessageBox("Error", "Error during adding equipment", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                }
                }
        }

        private void EmptyFields()
        {
            tbxType.Text = "";
            tbxQuantity.Text = "";
            tbxPrice.Text = "";
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsValid(((TextBox)sender).Text + e.Text);
        }

        public static bool IsValid(string str)
        {
            int i;
            return int.TryParse(str, out i) && i > 0;
        }
    }
}
