﻿using FootApp.DAO;
using FootApp.DTO;
using FootApp.PDF;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for ContractsControl.xaml
    /// </summary>
    public partial class ContractsControl : UserControl
    {
        Object data;
        private int offset = 0;
        String dataString;
        public ContractsControl(Object obj)
        {
            InitializeComponent();
            data = obj;
            SetData();
        }
        private void LoadContracts()
        {
            offset = 0;
            if (data is PlayerContract)
            {
                contractsTable.Items.Clear();
                List<PlayerContract> contracts = new PlayerContractDAO().GetTenContracts(offset);
                contracts.ForEach(x => contractsTable.Items.Add(x));
            }
            else if(data is EmployeeContract)
            {
                contractsTable.Items.Clear();
                List<EmployeeContract> contracts = new EmployeeContractDAO().GetTenContracts(offset);
                contracts.ForEach(x => contractsTable.Items.Add(x));
            }
            else if(data is SponsorContract)
            {
                contractsTable.Items.Clear();
                List<SponsorContract> contracts = new SponsorContractDAO().GetTenSponsorContracts(offset);
                contracts.ForEach(x => contractsTable.Items.Add(x));
            }
            else
            {
                contractsTable.Items.Clear();
                List<Player> tempList = new PlayerDAO().GetTenPlayersByCategory("Membership",offset);
                List<MembershipData> contracts = new List<MembershipData>();
                tempList.ForEach(x => contracts.Add(new MembershipData() { Name = x.Name, Surname = x.Surname, Id = x.ID,
                Due = new MembershipFeeDAO().GetDue(x)
                }));
                contracts.ForEach(x => contractsTable.Items.Add(x));
            }
           
        }
        private void SetData()
        {
            if (data is PlayerContract) dataString = "player";
            else if (data is EmployeeContract) dataString = "employee";
            else dataString = "sponsor";
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
            if (contractsTable.SelectedItem != null)
            {
                if (data is PlayerContract)
                {
                    bool isSet = new ContractInfoWindow((PlayerContract)contractsTable.SelectedItem).ShowDialog().Value;
                    if (isSet)
                    {
                        contractsTable.Items.Clear();
                        LoadContracts();
                    }
                }
                else if (data is EmployeeContract)
                {
                    bool isSet = new ContractInfoWindow((EmployeeContract)contractsTable.SelectedItem).ShowDialog().Value;
                    if (isSet)
                    {
                        contractsTable.Items.Clear();
                        LoadContracts();
                    }
                }
                else
                {
                    bool isSet = new ContractInfoWindow((SponsorContract)contractsTable.SelectedItem).ShowDialog().Value;
                    if (isSet)
                    {
                        contractsTable.Items.Clear();
                        LoadContracts();
                    }
                }
                
            }
            else
            {
                new CustomMessageBox("Error", "Please select "+dataString, CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            offset = 0;
            if (data is SponsorContract)
            {
                columnSurname.Visibility = Visibility.Collapsed;
                columnSalary.Header = "Investment";
                columnSalary.Binding = new Binding("Investment");
            }
            else if (data is MembershipFee)
            {
                columnExpires.Visibility = Visibility.Collapsed;
                columnSigned.Visibility = Visibility.Collapsed;
                btnAddContract.Visibility = Visibility.Collapsed;
                btnRemoveContract.Visibility = Visibility.Collapsed;
                btnExport.Visibility = Visibility.Visible;
                btnPay.Visibility = Visibility.Visible;
                btnDebt.Visibility = Visibility.Visible;
                columnSalary.Header = "Due";
                columnSalary.Binding = new Binding("Due");
            }
                LoadContracts();
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Title = "Save a PDF file";
            saveFileDialog.Filter = "pdf | *.pdf";
            saveFileDialog.ShowDialog();
            if (saveFileDialog.FileName != null)
            {
                new PDFUtil().ExportDataAsPdf(saveFileDialog.FileName, new PDFUtil().CreateMembershipFeePDFAsByteArray());
            }

        }

        private void btnPay_Click(object sender, RoutedEventArgs e)
        {
            MembershipData tmp= contractsTable.SelectedItem as MembershipData;
            if (contractsTable.SelectedItem != null)
            {
                if (tmp.Due > 0)
                {
                    new MembershipFeeDAO().AddNextPaymentForPlayer(new PlayerDAO().GetPlayerByID(((MembershipData)contractsTable.SelectedItem).Id));
                    tmp.Due = new MembershipFeeDAO().GetDue(new PlayerDAO().GetPlayerByID(tmp.Id));
                    contractsTable.Items.Refresh();
                    new CustomMessageBox("Info", "Due successfull paid", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Information).ShowDialog();
                }
                else
                {
                    new CustomMessageBox("Info", "Selected player has no debts", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Information).ShowDialog();
                }
            }
            else
            {
                new CustomMessageBox("Error", "Please select player", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
        }
        public class MembershipData
        {
            public string Name { get; internal set; }
            public string Surname { get; internal set; }
            public int Id { get; internal set; }
            public decimal Due { get; internal set; }
        }
        private void ContractsTable_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            ScrollViewer scrollViewer = e.OriginalSource as ScrollViewer;
            if (e.VerticalChange > 0)
            {
                if (scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
                {
                    offset++;
                    if (data is PlayerContract)
                    {
                        List<PlayerContract> contracts = new PlayerContractDAO().GetTenContracts(offset * 10);
                        if (contracts.Count == 0)
                        {
                            offset--;
                            return;
                        }
                        contracts.ForEach(x => contractsTable.Items.Add(x));
                    }
                    else if (data is EmployeeContract)
                    {
                        List<EmployeeContract> contracts = new EmployeeContractDAO().GetTenContracts(offset * 10);
                        if (contracts.Count == 0)
                        {
                            offset--;
                            return;
                        }
                        contracts.ForEach(x => contractsTable.Items.Add(x));
                    }
                    else if (data is SponsorContract)
                    {
                        List<SponsorContract> contracts = new SponsorContractDAO().GetTenSponsorContracts(offset * 10);
                        if (contracts.Count == 0)
                        {
                            offset--;
                            return;
                        }
                        contracts.ForEach(x => contractsTable.Items.Add(x));
                    }
                    else
                    {
                        List<Player> tempList = new PlayerDAO().GetTenPlayersByCategory("Membership",offset*10);
                        if (tempList.Count == 0)
                        {
                            offset--;
                            return;
                        }
                        List<MembershipData> contracts = new List<MembershipData>();
                        tempList.ForEach(x => contracts.Add(new MembershipData()
                        {
                            Name = x.Name,
                            Surname = x.Surname,
                            Id = x.ID,
                            Due = new MembershipFeeDAO().GetDue(x)
                        }));
                        contracts.ForEach(x => contractsTable.Items.Add(x));
                    }
                }
            }
        }

        private void btnDebt_Click(object sender, RoutedEventArgs e)
        {
            MembershipData tmp = contractsTable.SelectedItem as MembershipData;
            if (contractsTable.SelectedItem != null)
            {
                Player player = new PlayerDAO().GetPlayerByID(tmp.Id);
                bool isRemove = new MembershipFeeDAO().RemoveLastPaymentForPlayer(player);
                if (isRemove) { 
                tmp.Due = new MembershipFeeDAO().GetDue(player);
                contractsTable.Items.Refresh();
                new CustomMessageBox("Info", "Remove last payment successfull", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Information).ShowDialog();
                }
                else
                new CustomMessageBox("Info", "You can not add dept for this player.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Information).ShowDialog();

            }
            else
            {
                new CustomMessageBox("Error", "Please select player", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
        }

        private void TbxSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbxSearch.Text))
            {
                LoadContracts();
            }
            else
            {
                if (data is PlayerContract)
                {
                    contractsTable.Items.Clear();
                    List<PlayerContract> contracts = new PlayerContractDAO().GetPlayerContractsByPlayerName(tbxSearch.Text);
                    contracts.ForEach(x => contractsTable.Items.Add(x));
                }
                else if (data is EmployeeContract)
                {
                    contractsTable.Items.Clear();
                    List<EmployeeContract> contracts = new EmployeeContractDAO().GetEmployeeContractsByPlayerName(tbxSearch.Text);
                    contracts.ForEach(x => contractsTable.Items.Add(x));
                }
                else if (data is SponsorContract)
                {
                    contractsTable.Items.Clear();
                    List<SponsorContract> contracts = new SponsorContractDAO().GetSponsorContractsBySponsorName(tbxSearch.Text);
                    contracts.ForEach(x => contractsTable.Items.Add(x));
                }
                else
                {
                    contractsTable.Items.Clear();
                    List<Player> tempList = new PlayerDAO().GetPlayerByNameAndCategory(tbxSearch.Text, "Membership");
                    List<MembershipData> contracts = new List<MembershipData>();
                    tempList.ForEach(x => contracts.Add(new MembershipData()
                    {
                        Name = x.Name,
                        Surname = x.Surname,
                        Id = x.ID,
                        Due = new MembershipFeeDAO().GetDue(x)
                    }));
                    contracts.ForEach(x => contractsTable.Items.Add(x));
                }
            }
        }

        private void btnRemoveContract_Click(object sender, RoutedEventArgs e)
        {
            if (contractsTable.SelectedItem != null)
            {
                if(data is PlayerContract)
                {
                    PlayerContract playerContract = contractsTable.SelectedItem as PlayerContract;
                    new PlayerContractDAO().RemovePlayerContract(playerContract);
                    contractsTable.Items.Remove(playerContract);
                    contractsTable.Items.Refresh();
                }
                else if(data is EmployeeContract)
                {
                    EmployeeContract employeeContract = contractsTable.SelectedItem as EmployeeContract;
                    new EmployeeContractDAO().RemoveEmployeeContract(employeeContract);
                    contractsTable.Items.Remove(employeeContract);
                    contractsTable.Items.Refresh();
                }
                else
                {
                    SponsorContract sponsorContract = contractsTable.SelectedItem as SponsorContract;
                    new SponsorContractDAO().RemoveSponsorContract(sponsorContract);
                    contractsTable.Items.Remove(sponsorContract);
                    contractsTable.Items.Refresh();
                }

            }
            else
            {
                new CustomMessageBox("Error", "Please select "+dataString, CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
        }
    }
}
