﻿using FootApp.DAO;
using FootApp.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for ShowTransfersControl.xaml
    /// </summary>
    public partial class ShowTransfersControl : UserControl
    {
        public ShowTransfersControl()
        {
            InitializeComponent();
        }

        private void ButtonModifyPlayer_Click(object sender, RoutedEventArgs e)
        {
            if (PlayersTable.SelectedItem == null)
            {
                new CustomMessageBox("Error", "You didn't select any transfer.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }
            if (new TransferWindow(PlayersTable.SelectedItem as PlayerTransfer).ShowDialog().Value)
                UserControl_Loaded(sender, e);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            PlayersTable.Items.Clear();
            new PlayerTransferDAO().GetPlayerTransfers().ForEach(transfer => PlayersTable.Items.Add(transfer));
        }

        private void ButtonRemove_Click(object sender, RoutedEventArgs e)
        {
            if (PlayersTable.SelectedItem == null)
            {
                new CustomMessageBox("Error", "You didn't select any transfer.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }
            new PlayerTransferDAO().RemovePlayerTransfer(PlayersTable.SelectedItem as PlayerTransfer);
            new CustomMessageBox("Information", "Player transfer removed successfuly", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Information).ShowDialog();
            UserControl_Loaded(sender, e);
        }

        private void TbxSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbxSearch.Text))
                UserControl_Loaded(sender, e);
            else
            {
                PlayersTable.Items.Clear();
                new PlayerTransferDAO().GetPlayerTransfersByPlayer(tbxSearch.Text).ForEach(transfer => PlayersTable.Items.Add(transfer));
            }
        }
    }
}
