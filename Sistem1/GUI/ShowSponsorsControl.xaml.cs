﻿using FootApp.DAO;
using FootApp.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for ShowSponsorsControl.xaml
    /// </summary>
    public partial class ShowSponsorsControl : UserControl
    {
        public ShowSponsorsControl()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            LoadSponsors();
        }
        private void LoadSponsors()
        {
            SponsorsTable.Items.Clear();
            List<Sponsor> list = new SponsorDAO().GetCurrentSponsors();
            list.ForEach(p => SponsorsTable.Items.Add(p));
        }

        private void tbxSearch_TextChanged(object sender, TextChangedEventArgs e)
        {

            SponsorsTable.Items.Clear();
            List<Sponsor> list = new SponsorDAO().GetSponsorsByName(tbxSearch.Text);
            list.ForEach(p => SponsorsTable.Items.Add(p));
        }

        private void ButtonModifyPlayer_Click(object sender, RoutedEventArgs e)
        {
            if (SponsorsTable.SelectedItem != null)
            {
                bool modifyResult = new ShowSponsorWindow((Sponsor)SponsorsTable.SelectedItem).ShowDialog().Value;
                if (modifyResult)
                {
                    LoadSponsors();
                }
            }
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            new SponsorDAO().RemoveSponsor(SponsorsTable.SelectedItem as Sponsor);
            LoadSponsors();
            new CustomMessageBox("Information", "Sponsor removed successfully.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Information).ShowDialog();
        }
    }
}
