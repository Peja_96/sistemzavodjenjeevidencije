﻿using FootApp.DAO;
using FootApp.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for AddTransferControl.xaml
    /// </summary>
    public partial class AddTransferControl : UserControl
    {
        public AddTransferControl()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbxName.Text) || string.IsNullOrWhiteSpace(tbxSurname.Text) || string.IsNullOrWhiteSpace(tbxPrice.Text) ||
                string.IsNullOrWhiteSpace(tbxOtherClub.Text) || !Decimal.TryParse(tbxPrice.Text, out decimal price) || price < 0)
            {
                new CustomMessageBox("Error", "Please provide correct data for transfer", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }

            PlayerTransfer playerTransfer = new PlayerTransfer
            {
                Amount = price,
                ClubID = Club.FootballClub.ID,
                Count = 1,
                PlayerName = tbxName.Text,
                PlayerSurname = tbxSurname.Text,
                Description = "Player transfer - " + tbxName.Text + " " + tbxSurname.Text,
                OtherClub = tbxOtherClub.Text,
                Time = DateTime.Now,
                TransferType = cbxIsOutgoingTransfer.IsChecked.Value ? TransferType.Outgoing : TransferType.Incomming,
                Type = cbxIsOutgoingTransfer.IsChecked.Value ? FinancialEntryType.Income : FinancialEntryType.Expense
            };

            new PlayerTransferDAO().AddPlayerTransfer(playerTransfer);
            new CustomMessageBox("Information", "New transfer added succesfully.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Information).ShowDialog();
            tbxName.Clear();
            tbxSurname.Clear();
            tbxPrice.Clear();
            tbxOtherClub.Clear();
            cbxIsOutgoingTransfer.IsChecked = false;
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsValid(((TextBox)sender).Text + e.Text);
        }

        public static bool IsValid(string str)
        {
            int i;
            return int.TryParse(str, out i) && i > 0;
        }
    }   
}
