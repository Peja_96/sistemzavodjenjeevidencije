﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FootApp
{
    public partial class MenuBar : UserControl
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        MainWindow window;
        public MenuBar(ItemMenu itemMenu, MainWindow window)
        {
            InitializeComponent();

            this.window = window;
            ExpanderMenu.Visibility = itemMenu.SubItems == null ? Visibility.Collapsed : Visibility.Visible;
            ListViewItemMenu.Visibility = itemMenu.SubItems == null ? Visibility.Visible : Visibility.Collapsed;

            this.DataContext = itemMenu;
        }
        private void ListViewMenu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                window.SwitchScreen(((SubItem)((ListView)sender).SelectedItem).Screen);
            }
            catch (NullReferenceException){}
        }

        private void ListViewMenu_LostFocus(object sender, RoutedEventArgs e)
        {
            ListViewMenu.SelectedIndex = -1;
        }

        private void ExpanderMenu_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MessageBox.Show("A");
        }

       
    }
}
