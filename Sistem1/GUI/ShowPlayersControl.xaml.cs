﻿using FootApp.DTO;
using FootApp.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for ShowPlayersControl.xaml
    /// </summary>
    public partial class ShowPlayersControl : UserControl
    {
        string category;
        private int offset = 0;
        public ShowPlayersControl()
        {
            InitializeComponent();

        }
        public ShowPlayersControl(string category)
        {
            InitializeComponent();
            this.category = category;
        }

        private void LoadPlayers(string Category)
        {
            PlayersTable.Items.Clear();
            List<Player> list = new PlayerDAO().GetTenPlayersByCategory(Category, offset * 10);
            list.ForEach(p => PlayersTable.Items.Add(p));
        }
        private void LoadPlayers()
        {

            PlayersTable.Items.Clear();
            List<Player> list = new PlayerDAO().GetTenPlayers(offset * 10);
            list.ForEach(p => PlayersTable.Items.Add(p));

        }

        private void ButtonInfo_Click(object sender, RoutedEventArgs e)
        {
            if (PlayersTable.SelectedItem != null)
            {
                Player player = (Player)PlayersTable.SelectedItem;
                PlayerStats ps = new PlayerStats(player);
                ps.ShowDialog();
            }
            else
            {
                new CustomMessageBox("Error", "Please select player", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            offset = 0;
            if (string.IsNullOrEmpty(category))
                LoadPlayers();
            else
                LoadPlayers(category);
        }

        private void PlayersTable_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1).ToString();
        }

        private void ButtonGameStats_Click(object sender, RoutedEventArgs e)
        {
            if (PlayersTable.SelectedItem != null)
            {
                Player player = (Player)PlayersTable.SelectedItem;
                PlayerGameStatsWindow ps = new PlayerGameStatsWindow(player);
                ps.ShowDialog();
            }
            else
            {
                new CustomMessageBox("Error", "Please select player", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
        }

        private void ButtonRemove_Click(object sender, RoutedEventArgs e)
        {
            if (PlayersTable.SelectedItem != null)
            {
                bool? messageBoxResult = new CustomMessageBox("Delete Confirmation", "Are you sure?", CustomMessageBoxButton.YesNo, CustomMessageBoxIcon.Warning).ShowDialog();
                if (messageBoxResult == true)
                {
                    Player player = (Player)PlayersTable.SelectedItem;

                    try
                    {
                        new DAO.PlayerDAO().DeletePlayer(player);
                        PlayersTable.Items.Remove(player);
                        new CustomMessageBox("Deleted", "Igrac " + player.Name + " " + player.Surname + " successfully deleted.", CustomMessageBoxButton.Ok).ShowDialog();

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.StackTrace);
                        new CustomMessageBox("Error", "Error removing player.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                    }
                }
            }
            else
            {
                new CustomMessageBox("Error", "Please select player", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
        }

        private void ButtonModifyPlayer_Click(object sender, RoutedEventArgs e)
        {
            if (PlayersTable.SelectedItem != null)
            {
                bool modifyResult = new PlayerInfoWindow((Player)PlayersTable.SelectedItem).ShowDialog().Value;
                if (modifyResult)
                {
                    offset = 0;
                    LoadPlayers();
                    PlayersTable.ScrollIntoView(PlayersTable.Items[0]);
                }
            }
            else
            {
                new CustomMessageBox("Error", "Please select player", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
        }



        private void tbxSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            PlayersTable.Items.Clear();
            if (string.IsNullOrWhiteSpace(tbxSearch.Text))
            {
                UserControl_Loaded(sender, e);
            }
            else
            {
                if (!string.IsNullOrEmpty(category))
                {
                    List<Player> list = new PlayerDAO().GetPlayerByNameAndCategory(tbxSearch.Text, category);
                    list.ForEach(p => PlayersTable.Items.Add(p));
                }
                else
                {
                    List<Player> list = new PlayerDAO().GetPlayersByName(tbxSearch.Text);
                    list.ForEach(p => PlayersTable.Items.Add(p));
                }
            }

        }

        private void PlayersTable_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            ScrollViewer scrollViewer = e.OriginalSource as ScrollViewer;
            if (e.VerticalChange > 0)
            {
                if (scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
                {
                    offset++;
                    List<Player> list;
                    if (string.IsNullOrEmpty(category))
                    {
                        list = new PlayerDAO().GetTenPlayers(offset * 10);
                    }
                    else
                    {
                        list = new PlayerDAO().GetTenPlayersByCategory(category, offset * 10);
                    }
                    if (list.Count == 0)
                    {
                        offset--;
                        return;
                    }
                    list.ForEach(p => PlayersTable.Items.Add(p));

                }
            }
        }
    }
}
