﻿using FootApp.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for RemoveEquipmentWiondow.xaml
    /// </summary>
    public partial class RemoveEquipmentWiondow : Window
    {
        private Equipment equipment;
        public RemoveEquipmentWiondow(Equipment e)
        {
            InitializeComponent();
            equipment = e;
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void btnYes_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(tbxQuantity.Text))
            {
                if (int.TryParse(tbxQuantity.Text, out int quantity))
                {
                    if (quantity < 1 || quantity > equipment.Quantity)
                    {
                        new CustomMessageBox("Wrong info", "Please enter correct number.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                    }
                    else
                    {

                        equipment.Quantity -= quantity;
                        new DAO.EquipmentDAO().UpdateEquipment(equipment);
                        new CustomMessageBox("Success", "Equipment successfuly updated.", CustomMessageBoxButton.Ok).ShowDialog();
                        this.DialogResult = true;
                        this.Close();
                    }
                }
                else
                {
                    new CustomMessageBox("Wrong info", "Please enter number.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                }
            }
            else
            {
                new CustomMessageBox("Enter info", "Please enter required information.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsValid(((TextBox)sender).Text + e.Text);
        }

        public static bool IsValid(string str)
        {
            int i;
            return int.TryParse(str, out i) && i > 0;
        }
    }
}
