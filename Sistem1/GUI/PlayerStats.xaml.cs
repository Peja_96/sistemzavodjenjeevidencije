﻿using FootApp.DAO;
using FootApp.DTO;
using log4net;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for PlayerStats.xaml
    /// </summary>
    public partial class PlayerStats : Window
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        Player player;
        private List<Season> seasons;
        private int selectedIndex;
        public PlayerStats(Player player)
        {
            this.player = player;
            InitializeComponent();
            seasons = new SeasonDAO().GetSeasons();
            SetInit();
            LoadPlayer();
        }
        private void LoadPlayer() {
            tbxName.Text = player.Name + " " + player.Surname;
            tbxBirthDate.Text = player.Date;
            tbxCategory.Text = player.Category;
            SetStatisticsLabels();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ComponentEnable(bool isEnable)
        {
            TbxGoals.IsEnabled = isEnable;
            TbxAssists.IsEnabled = isEnable;
            TbxFouls.IsEnabled = isEnable;
            //TbxCleanSheets.IsEnabled = isEnable;
            TbxYellowCards.IsEnabled = isEnable;
            TbxRedCards.IsEnabled = isEnable;
        }

        private void RightSeasonButton_NextSeason(object sender, RoutedEventArgs e)
        {
            if (!leftSeasonButton.IsEnabled) leftSeasonButton.IsEnabled = true;
            selectedIndex++;
            seasonTextBox.Text = "Season " + seasons[selectedIndex].StartDate.ToString("yy") + "/" + seasons[selectedIndex].EndDate.ToString("yy");
            if (selectedIndex.Equals(seasons.Count - 1))
                rightSeasonButton.IsEnabled = false;
            SetStatisticsLabels();
        }
        private void SetInit()
        {
            rightSeasonButton.IsEnabled = false;
            if (seasons.Count == 1)
            {
                leftSeasonButton.IsEnabled = false;
            }
            else if (seasons.Count == 0)
            {
                seasonTextBox.Text = "No Seasons";
                selectedIndex = -1;
                leftSeasonButton.IsEnabled = false;
                return;
            }
            selectedIndex = seasons.IndexOf(new SeasonDAO().GetCurrentSeason());
            if (selectedIndex < seasons.Count - 1 && selectedIndex != -1)
                rightSeasonButton.IsEnabled = true;
            seasonTextBox.Text = "Season " + seasons[selectedIndex].StartDate.ToString("yy") + "/" + seasons[selectedIndex].EndDate.ToString("yy");
            SetStatisticsLabels();
        }

        private void leftSeasonButton_PreviousSeason(object sender, RoutedEventArgs e)
        {
            if (!rightSeasonButton.IsEnabled)
                rightSeasonButton.IsEnabled = true;
            selectedIndex--;
            seasonTextBox.Text = "Season " + seasons[selectedIndex].StartDate.ToString("yy") + "/" + seasons[selectedIndex].EndDate.ToString("yy");
            if (selectedIndex.Equals(0))
                leftSeasonButton.IsEnabled = false;
            SetStatisticsLabels();
        }
        private void SetStatisticsLabels()
        {
            PlayerStatistics ps = new DAO.PlayerStatisticsDAO().GetPlayerStatisticsForSeason(player,seasons[selectedIndex]);
            if (ps.GamesPlayed != 0)
            {
                TbxGamesPlayed.Text = ps.GamesPlayed.ToString();
                TbxGoals.Text = ps.TotalGoals.ToString();
                TbxGoalsPG.Text = Math.Round((ps.TotalGoals / (double)ps.GamesPlayed), 2).ToString();
                TbxAssists.Text = ps.TotalAssists.ToString();
                TbxAssistsPG.Text = Math.Round((ps.TotalAssists / (double)ps.GamesPlayed), 2).ToString(); ;
                TbxYellowCards.Text = ps.TotalYellowCards.ToString();
                TbxYellowCardsPG.Text = Math.Round((ps.TotalYellowCards / (double)ps.GamesPlayed), 2).ToString();
                TbxRedCards.Text = ps.TotalRedCards.ToString();
                TbxRedCardsPG.Text = Math.Round((ps.TotalRedCards / (double)ps.GamesPlayed), 2).ToString();
                TbxFouls.Text = ps.TotalFouls.ToString();
                TbxFoulsPG.Text = Math.Round((ps.TotalFouls / (double)ps.GamesPlayed), 2).ToString();
                TbxTotalPassesPG.Text = ps.PassesPerGame.ToString();
                TbxTotalPasses.Text = ((int)Math.Ceiling(ps.PassesPerGame * ps.GamesPlayed)).ToString();
                TbxPassesPercentage.Text = Math.Round(ps.SuccessfulPassesPercent, 2) + "%";
                TbxTotalShotsPG.Text = ps.ShotsPerGame.ToString();
                TbxTotalShots.Text = ((int)Math.Ceiling(ps.ShotsPerGame * ps.GamesPlayed)).ToString();
                TbxShotAccuracy.Text = Math.Round(ps.ShotAccuracy, 2) + "%";
            }
            else
            {
                String zero = "0";
                TbxGamesPlayed.Text = zero;
                TbxGoals.Text = zero;
                TbxGoalsPG.Text = zero;
                TbxAssists.Text = zero;
                TbxAssistsPG.Text = zero;
                TbxYellowCards.Text = zero;
                TbxYellowCardsPG.Text = zero;
                TbxRedCards.Text = zero;
                TbxRedCardsPG.Text = zero;
                TbxFouls.Text = zero;
                TbxFoulsPG.Text = zero;
                TbxTotalPassesPG.Text = zero;
                TbxTotalPasses.Text = zero;
                TbxPassesPercentage.Text = zero;
                TbxTotalShotsPG.Text = zero;
                TbxTotalShots.Text = zero;
                TbxShotAccuracy.Text = zero;
            }
            if (!string.IsNullOrWhiteSpace(player.ImageFilePath))
            {
                try
                {
                    playerImg.Source = new BitmapImage(new Uri(player.ImageFilePath.Replace("%20", " ")));
                }
                catch (Exception ex)
                {
                    log.Warn(ex);
                }
            }
        }
        private bool PlayerStatsToPdf(String path)
        {
            PDF.PDFUtil util = new PDF.PDFUtil();
            return util.ExportDataAsPdf(path, util.CreatePlayerStatsPDFAsByteArray(player, seasons));
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Pdf files (*.pdf)|*.pdf";
            if (sfd.ShowDialog() == true)
            {
                PlayerStatsToPdf(sfd.FileName);
            }
        }
    }
}
