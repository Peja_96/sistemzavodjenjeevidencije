﻿using FootApp.DTO;
using FootApp.GUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FootApp
{
    
    public partial class AddSponsorControl : UserControl
    {
        public AddSponsorControl()
        {
            InitializeComponent();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            tbxContactName.Visibility = Visibility.Visible;
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            tbxContactName.Visibility = Visibility.Hidden;
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbxName.Text) && !string.IsNullOrWhiteSpace(tbxContactNumber.Text) && !string.IsNullOrWhiteSpace(tbxMail.Text) &&
                (!cbxIsCompany.IsChecked.Value || !string.IsNullOrWhiteSpace(tbxContactName.Text)))
            {
                if (int.TryParse(tbxContactNumber.Text, out int number))
                {
                    SponsorType sponsorType = cbxIsCompany.IsChecked.Value ? SponsorType.Company : SponsorType.Individual;
                    Sponsor sponsor = new Sponsor
                    {
                        Id = 0,
                        Name = tbxName.Text,
                        ContactMail = tbxMail.Text,
                        ContactNumber = tbxContactNumber.Text,
                        ContactName = tbxContactName.Text,
                        IsSponsorCurrently = true,
                        SponsorType = sponsorType
                    };
                    ContractWindow cw = new ContractWindow(sponsor);
                    cw.ShowDialog();
                }
                else
                {
                    new CustomMessageBox("Wrong data", "Please enter correct phone number.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();

                }
            }
            else 
            {
                new CustomMessageBox("Enter data", "Please enter required data.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsValid(((TextBox)sender).Text + e.Text);
        }

        public static bool IsValid(string str)
        {
            int i;
            return int.TryParse(str, out i) && i >= 0;
        }
    }
}
