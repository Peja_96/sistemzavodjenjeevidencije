﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for CustomMessageBox.xaml
    /// </summary>
    public partial class CustomMessageBox : Window
    {
        public CustomMessageBox(string title, string text, CustomMessageBoxButton buttons = CustomMessageBoxButton.YesNo, CustomMessageBoxIcon icon = CustomMessageBoxIcon.Information)
        {
            InitializeComponent();
            this.ShowInTaskbar = false;
            titleBlock.Text = text;
            titleLabel.Content = title;

            //Set buttons
            if (buttons.Equals(CustomMessageBoxButton.Ok))
            {
                btnOk.Visibility = Visibility.Visible;
                btnYes.Visibility = Visibility.Hidden;
                btnNo.Visibility = Visibility.Hidden;
            }
            else if (buttons.Equals(CustomMessageBoxButton.OkCancel))
            {
                btnYes.Content = "Ok";
                btnNo.Content = "Cancel";
            }

            //Set icon
            if (icon.Equals(CustomMessageBoxIcon.Warning))
                packIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.Warning;
            else if (icon.Equals(CustomMessageBoxIcon.Error))
                packIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.Error;
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
    }
}
