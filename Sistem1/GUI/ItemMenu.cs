﻿using System.Collections.Generic;
using System.Windows.Controls;

namespace FootApp
{
    public class ItemMenu
    {
        
            public ItemMenu(string header, List<SubItem> subItems)
            {
                Header = header;
                SubItems = subItems;
                
            }


            public string Header { get; private set; }
            public List<SubItem> SubItems { get; private set; }
            public UserControl Screen { get; private set; }
    }
    
}