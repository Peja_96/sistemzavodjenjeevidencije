﻿using FootApp.DAO;
using FootApp.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for TransferWindow.xaml
    /// </summary>
    public partial class TransferWindow : Window
    {
        private PlayerTransfer playerTransfer;
        public TransferWindow(PlayerTransfer playerTransfer)
        {
            InitializeComponent();
            this.playerTransfer = playerTransfer;
            this.tbxName.Text = playerTransfer.PlayerName;
            this.tbxSurname.Text = playerTransfer.PlayerSurname;
            this.tbxPrice.Text = Math.Round(playerTransfer.Amount, 2) + "";
            this.tbxOtherClub.Text = playerTransfer.OtherClub;
            this.cbxIsOutgoingTransfer.IsChecked = playerTransfer.TransferType == TransferType.Outgoing ? true : false;
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbxName.Text) || string.IsNullOrWhiteSpace(tbxSurname.Text) || string.IsNullOrWhiteSpace(tbxPrice.Text) ||
                string.IsNullOrWhiteSpace(tbxOtherClub.Text) || !Decimal.TryParse(tbxPrice.Text, out decimal price) || price < 0)
            {
                new CustomMessageBox("Error", "Please provide correct data for transfer", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }

            TransferType transferType = cbxIsOutgoingTransfer.IsChecked.Value ? TransferType.Outgoing : TransferType.Incomming;
            if(playerTransfer.PlayerName.Equals(tbxName.Text) && playerTransfer.PlayerSurname.Equals(tbxSurname.Text) && playerTransfer.Amount == price &&
                playerTransfer.OtherClub.Equals(tbxOtherClub.Text) && playerTransfer.TransferType.Equals(transferType))
            {
                new CustomMessageBox("Error", "You endered same data for transfer.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                return;
            }

            playerTransfer.PlayerName = tbxName.Text;
            playerTransfer.PlayerSurname = tbxSurname.Text;
            playerTransfer.Amount = price;
            playerTransfer.TransferType = transferType;
            playerTransfer.OtherClub = tbxOtherClub.Text;

            new PlayerTransferDAO().ModifyPlayerTransfer(playerTransfer);
            new CustomMessageBox("Information", "Transfer data modified successfully.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Information).ShowDialog();
            DialogResult = true;
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsValid(((TextBox)sender).Text + e.Text);
        }

        public static bool IsValid(string str)
        {
            int i;
            return int.TryParse(str, out i) && i > 0;
        }
    }
}
