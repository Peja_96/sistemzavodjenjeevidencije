﻿using FootApp.DAO;
using FootApp.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for ShowEmployeeControl.xaml
    /// </summary>
    public partial class ShowEmployeeControl : UserControl
    {
        private int offset = 0;
        public ShowEmployeeControl()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            offset = 0;
            EmployeesTable.Items.Clear();
            LoadEmployees();
        }
        private void LoadEmployees()
        {
            EmployeesTable.Items.Clear();
            offset = 0;
            List<Employee> list = new EmployeeDAO().GetTenEmployees(offset);
            list.ForEach(p => EmployeesTable.Items.Add(p));
        }

        private void tbxSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            EmployeesTable.Items.Clear();
            List<Employee> list = new EmployeeDAO().GetEmployeebyName(tbxSearch.Text);
            list.ForEach(p => EmployeesTable.Items.Add(p));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (EmployeesTable.SelectedItem != null)
            {
                bool modifyResult = new ShowEmployeeWindow((Employee)EmployeesTable.SelectedItem).ShowDialog().Value;
                if (modifyResult)
                {
                    LoadEmployees();
                    EmployeesTable.ScrollIntoView(EmployeesTable.Items[0]);
                }

            }
            else
            {
                new CustomMessageBox("Error", "Please select player", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
        }

        private void EmployeesTable_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            ScrollViewer scrollViewer = e.OriginalSource as ScrollViewer;
            if (e.VerticalChange > 0)
            {
                if (scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
                {
                    offset++;

                    List<Employee> list = new EmployeeDAO().GetTenEmployees(offset*10);
                    if (list.Count == 0) 
                    {
                        offset--;
                        return;
                    }
                    list.ForEach(p => EmployeesTable.Items.Add(p));
                }
            }
        }

        private void removeButton_Click(object sender, RoutedEventArgs e)
        {
            if (EmployeesTable.SelectedItem != null)
            {
                Employee employee = (Employee)EmployeesTable.SelectedItem;
                new EmployeeDAO().RemoveEmployee(employee);
                EmployeesTable.Items.Remove(employee);
                new CustomMessageBox("Deleted", "Employee " + employee.Name + " " + employee.Surname + " successfully deleted.", CustomMessageBoxButton.Ok).ShowDialog();
            }
            else
            {
                new CustomMessageBox("Error", "Please select player", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
        }
    }
}
