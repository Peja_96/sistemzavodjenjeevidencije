﻿using FootApp.DAO;
using FootApp.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for NewsControl.xaml
    /// </summary>
    public partial class NewsControl : UserControl
    {
        Object data;
        private int offset = 0;
        private DateTime selectedDate=new DateTime(1900,1,1);
        public NewsControl(Object obj)
        {
            InitializeComponent();
            DataContext = obj;
            data = obj;
            if (obj is Announcement)
                lblTitle.Content = "News";
            else
                lblTitle.Content = "Games";
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            offset = 0;
            listNews.Children.Clear();
            if (data is Announcement)
            {
                List<Announcement> list = new AnnouncementDAO().GetAnnouncementsTen(offset*10);
                foreach (Announcement a in list)
                    listNews.Children.Add(new NewsItemControl(a));
                
            }
            else
            {
                List<Game> list = new GameDAO().getTenGames(offset*10);
                foreach (Game a in list)
                    listNews.Children.Add(new NewsItemControl(a));
            }
        }
        public void getByDate(DateTime date)
        {
            listNews.Children.Clear();
            selectedDate = date;
            if (data is Announcement)
            {
                List<Announcement> list = new AnnouncementDAO().GetAnnouncementsTenByDate(date,offset);
                foreach (Announcement a in list)
                    listNews.Children.Add(new NewsItemControl(a));

            }
            
            else
            {
                List<Game> list = new GameDAO().getTenGamesByDate(date,offset);
                foreach (Game a in list)
                    listNews.Children.Add(new NewsItemControl(a));
            }
        }
        private void OnScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            ScrollViewer scrollViewer = sender as ScrollViewer;
            if (e.VerticalChange > 0)
            {
                if (scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
                {
                    offset++;
                    if(selectedDate.Year == 1900)
                    {
                        if (data is Announcement)
                        {
                            List<Announcement> list = new AnnouncementDAO().GetAnnouncementsTen(offset*10);
                            foreach (Announcement a in list)
                                listNews.Children.Add(new NewsItemControl(a));

                        }
                        else
                        {
                            List<Game> list = new GameDAO().getTenGames(offset*10);
                            foreach (Game a in list)
                                listNews.Children.Add(new NewsItemControl(a));
                        }
                    }
                    else if (data is Announcement)
                    {
                        List<Announcement> list = new AnnouncementDAO().GetAnnouncementsTenByDate(selectedDate,offset*10);
                        if (list.Count == 0) offset--;
                        foreach (Announcement a in list)
                            listNews.Children.Add(new NewsItemControl(a));

                    }
                    else
                    {
                        List<Game> list = new GameDAO().getTenGamesByDate(selectedDate, offset * 10);
                        if (list.Count == 0) offset--;
                        foreach (Game a in list)
                            listNews.Children.Add(new NewsItemControl(a));
                    }
                }
            }

        }
        private void TextBox_MouseEnter(object sender, MouseEventArgs e)
        {
            (MainWindow.workGrid.FindName("effect") as BlurBitmapEffect).Radius = 10;
        }

        private void TextBox_MouseLeave(object sender, MouseEventArgs e)
        {
            (MainWindow.workGrid.FindName("effect") as BlurBitmapEffect).Radius = 0;
        }

    }
}
