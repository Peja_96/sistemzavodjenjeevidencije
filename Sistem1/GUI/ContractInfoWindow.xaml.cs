﻿using FootApp.DAO;
using FootApp.DTO;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for ContractInfoWindow.xaml
    /// </summary>
    public partial class ContractInfoWindow : Window
    {

        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Object data;
        public ContractInfoWindow(Object contract)
        {
            InitializeComponent();
            data = contract;
            SetTbx(data);
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        private void SetTbx(Object data)
        {
            nameTbx.Focusable = false;
            surnameTbx.Focusable = false;
            if (data is PlayerContract)
            {
                PlayerContract playerContract = (data as PlayerContract);
                nameTbx.Text = playerContract.Name;
                surnameTbx.Text = playerContract.Surname;

            }
            else if (data is EmployeeContract)
            {
                EmployeeContract employeeContract = (data as EmployeeContract);
                nameTbx.Text = employeeContract.Name;
                surnameTbx.Text = employeeContract.Surname;
            }//sponsor
            else
            {
                surnameTbx.Visibility = Visibility.Hidden;
                SponsorContract sponsorContract = (data as SponsorContract);
                nameTbx.Text = sponsorContract.Name;
                surnameTbx.Text = "text";
                valueLabel.Content = "Investment: ";
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(nameTbx.Text) || string.IsNullOrEmpty(surnameTbx.Text) ||
                    string.IsNullOrEmpty(startDateTbx.Text) || string.IsNullOrEmpty(endDateTbx.Text)
                    || string.IsNullOrEmpty(valueTbx.Text) 
                )
            {
                new CustomMessageBox("Wrong Data", "Please enter required information.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
            else
            {

                if (startDateTbx.Text.Split('-').Length == 3 && endDateTbx.Text.Split('-').Length == 3)
                {
                    if (DateTime.TryParse(startDateTbx.Text, out DateTime startDate) &&
                        startDate.CompareTo(new DateTime(1901, 1, 1)) > 0 && DateTime.TryParse(endDateTbx.Text, out DateTime endDate)
                        && endDate.CompareTo(new DateTime(1901, 1, 1)) > 0)
                    {
                        if (endDate.CompareTo(startDate) > 0)
                        {
                            if(data is PlayerContract)
                            {
                                PlayerContract playerContract = new PlayerContract
                                {
                                    Name = nameTbx.Text,
                                    Surname = surnameTbx.Text,
                                    StartDate = startDate,
                                    EndDate = endDate,
                                    PlayerID = (data as PlayerContract).PlayerID,
                                    Salary = Convert.ToInt32(valueTbx.Text)
                                };
                                try
                                {
                                    new PlayerContractDAO().AddPlayerContract(playerContract);
                                    new CustomMessageBox("Info", "New contract successful added.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Information).ShowDialog();
                                    DialogResult = true;
                                    Close();
                                }
                                catch (Exception ex)
                                {
                                    log.Warn(ex);
                                    new CustomMessageBox("Wrong Data", "Wrong input.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                                }
                            }
                            else if(data is EmployeeContract)
                            {
                                EmployeeContract employeeContract = new EmployeeContract
                                {
                                    Name = nameTbx.Text,
                                    Surname = surnameTbx.Text,
                                    StartDate = startDate,
                                    EndDate = endDate,
                                    EmployeeID = (data as EmployeeContract).EmployeeID,
                                    Salary = Convert.ToInt32(valueTbx.Text)
                                };
                                try
                                {
                                    new EmployeeContractDAO().AddEmployeeContract(employeeContract);
                                    new CustomMessageBox("Info", "New contract successful added.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Information).ShowDialog();
                                    DialogResult = true;
                                    Close();
                                }
                                catch (Exception ex)
                                {
                                    log.Warn(ex);
                                    new CustomMessageBox("Wrong Data", "Wrong input.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                                }
                            }//sponsor
                            else
                            {
                                SponsorContract sponsorContract = new SponsorContract
                                {
                                    SponsorID = (data as SponsorContract).SponsorID,
                                    Name = nameTbx.Text,
                                    StartDate = startDate,
                                    EndDate = endDate,
                                    Investment = Convert.ToInt32(valueTbx.Text)
                                };
                                try
                                {
                                    new SponsorContractDAO().AddSponsorContract(sponsorContract);
                                    new CustomMessageBox("Info", "New contract successful added.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Information).ShowDialog();
                                    DialogResult = true;
                                    Close();
                                }
                                catch (Exception ex)
                                {
                                    log.Warn(ex);
                                    new CustomMessageBox("Wrong Data", "Wrong input.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                                }
                            }
                        }
                        else
                        {
                            new CustomMessageBox("Error Date", "End date is before start date", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                        }
                    }
                    else
                    {
                        new CustomMessageBox("Error Date", "Date is not valid", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                    }
                }
                else
                {
                    new CustomMessageBox("Error Date", "Date is not valid", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                }
            }
        }
        private void TbxDate_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            DateMatch(textBox, e);
        }
        private void DateMatch(TextBox textBox, TextCompositionEventArgs e)
        {
            if (e.Text.Equals("-") && textBox.Text.Split('-').Length == 3)
            {
                e.Handled = true;
            }
            else if (textBox.CaretIndex > 9)
                e.Handled = true;
            else if (textBox.CaretIndex == 0)
            {
                Regex regex = new Regex("[^0-3]");
                e.Handled = regex.IsMatch(e.Text);
            }
            else if (textBox.CaretIndex == 1)
            {
                if (textBox.Text[0].ToString().Equals("3"))
                {
                    Regex regex = new Regex("[^0-1]");
                    e.Handled = regex.IsMatch(e.Text);
                }
                else
                {
                    Regex regex = new Regex("[^0-9]");
                    e.Handled = regex.IsMatch(e.Text);
                }

            }
            else if (textBox.CaretIndex == 2 || textBox.CaretIndex == 5)
            {
                Regex regex = new Regex("[^-]");
                e.Handled = regex.IsMatch(e.Text);
            }
            else if (textBox.CaretIndex == 3)
            {
                Regex regex = new Regex("[^0-1]");
                e.Handled = regex.IsMatch(e.Text);
            }
            else if (textBox.CaretIndex == 4)
            {
                if (textBox.Text[3].ToString().Equals("0"))
                {
                    Regex regex = new Regex("[^0-9]");
                    e.Handled = regex.IsMatch(e.Text);
                }
                else
                {
                    Regex regex = new Regex("[^0-2]");
                    e.Handled = regex.IsMatch(e.Text);
                }
            }
            else if (textBox.CaretIndex >= 6 && textBox.CaretIndex <= 9)
            {
                Regex regex = new Regex("[^0-9]");
                e.Handled = regex.IsMatch(e.Text);
            }
        }


        private void TbxDate_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            UndoDelete(textBox, e);
        }
        private void UndoDelete(TextBox textBox, KeyEventArgs e)
        {
            if (e.Key == Key.Back)
            {
                if (textBox.CaretIndex > 0 && textBox.Text[textBox.CaretIndex - 1].ToString().Equals("-"))
                {
                    textBox.CaretIndex -= 1;
                    e.Handled = true;
                }
            }
            else if (e.Key == Key.Delete)
            {
                if (textBox.Text.Length != (textBox.CaretIndex) && textBox.Text[textBox.CaretIndex].ToString().Equals("-"))
                {
                    textBox.CaretIndex += 1;
                    e.Handled = true;
                }
            }
            
        }

        private void valueTbx_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsValid(((TextBox)sender).Text + e.Text);
        }

        public static bool IsValid(string str)
        {
            int i;
            return int.TryParse(str, out i) && i > 0;
        }
    }
}
