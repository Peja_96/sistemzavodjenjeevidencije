﻿using FootApp.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for ShowSponsorWindow.xaml
    /// </summary>
    public partial class ShowSponsorWindow : Window
    {
        Sponsor sponsor;
        public ShowSponsorWindow(Sponsor s)
        {
            InitializeComponent();
            sponsor = s;
            PopulateFields();
        }
        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            tbxContactName.Visibility = Visibility.Visible;
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            tbxContactName.Visibility = Visibility.Hidden;
            tbxContactName.Text = "";

        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void PopulateFields()
        {
            tbxName.Text = sponsor.Name;
            tbxContactNumber.Text = sponsor.ContactNumber;
            tbxMail.Text = sponsor.ContactMail;
            if (sponsor.SponsorType == SponsorType.Company)
            {
                cbxIsCompany.IsChecked = true;
                tbxContactName.Text = sponsor.ContactName;
                tbxContactName.Visibility = Visibility.Visible;
            }
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(tbxName.Text) && !string.IsNullOrEmpty(tbxContactNumber.Text) && !string.IsNullOrEmpty(tbxMail.Text) &&
                (cbxIsCompany.IsChecked.GetValueOrDefault() == false || (cbxIsCompany.IsChecked.GetValueOrDefault() && !string.IsNullOrEmpty(tbxContactName.Text))))
            {
                if (!tbxContactNumber.Text.All(char.IsDigit))
                {
                    new CustomMessageBox("Enter info", "Enter correct phone number (only digits).", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                }
                else
                {
                    SponsorType newSponsorType = cbxIsCompany.IsChecked.Value ? SponsorType.Company : SponsorType.Individual;
                    if (tbxName.Text.Equals(sponsor.Name) && tbxContactNumber.Text.Equals(sponsor.ContactNumber) && tbxMail.Text.Equals(sponsor.ContactMail) && tbxContactName.Text.Equals(sponsor.ContactName) && sponsor.SponsorType == newSponsorType)
                    {
                        new CustomMessageBox("Not changed", "Data not changed.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                    }
                    else
                    {
                        sponsor.Name = tbxName.Text;
                        sponsor.ContactNumber = tbxContactNumber.Text;
                        sponsor.SponsorType = cbxIsCompany.IsChecked.Value ? SponsorType.Company : SponsorType.Individual;
                        sponsor.ContactMail = tbxMail.Text;
                        sponsor.ContactName = tbxContactName.Text;
                        new DAO.SponsorDAO().ModifySponsor(sponsor);
                        new CustomMessageBox("Success", "Sponsor successfuly updated.", CustomMessageBoxButton.Ok).ShowDialog();
                        this.DialogResult = true;
                        this.Close();
                    }
                }
            }
            else
            {
                new CustomMessageBox("Enter info", "Please enter required information.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsValid(((TextBox)sender).Text + e.Text);
        }

        public static bool IsValid(string str)
        {
            int i;
            return int.TryParse(str, out i) && i >= 0;
        }
    }
}
