﻿using FootApp.DAO;
using FootApp.DTO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for SeasonStatsControl.xaml
    /// </summary>
    public partial class SeasonStatsControl : UserControl
    {
        private List<Season> seasons;
        private int selectedIndex;
        private bool formLoaded = false;
        public SeasonStatsControl()
        {
            InitializeComponent();
            //seasons = new SeasonDAO().GetSeasons();
            //InitComboBoxes();
            //SetInit();
        }
        private void InitComboBoxes()
        {
            //Teams
            new TeamDAO().GetTeams().ForEach(team => cmbxCategory.Items.Add(team));

            //GameType 
            cmbxGameType.Items.Add("Total Games");
            cmbxGameType.Items.Add("Home Games");
            cmbxGameType.Items.Add("Away Games");

            //Competitions
            new CompetitionDAO().GetCompetitions().ForEach(competition => cmbxCompetiton.Items.Add(competition));
            cmbxCompetiton.Items.Add("All Competitions");
        }
        public class MyViewModel
        {
            public ObservableCollection<MyData> Data { get; set; }
            public int wins = 100;
            public MyViewModel(int wins, int draws, int losts)
            {
                Data = new ObservableCollection<MyData>
                {
                    new MyData {Name="Wins", Count = wins, Background = new SolidColorBrush(Colors.Green) },
                    new MyData {Name="Draws", Count = draws, Background = new SolidColorBrush(Colors.Yellow)},
                    new MyData {Name="Losts", Count = losts, Background = new SolidColorBrush(Colors.Red) },
                };
            }
        }
        public class MyData
        {
            public Brush Background { get; internal set; }
            public string Name { get; internal set; }
            public int Count { get; internal set; }
        }

        private void NextSeason(object sender, RoutedEventArgs e)
        {
            if (!leftButton.IsEnabled) leftButton.IsEnabled = true;
            selectedIndex++;
            seasonTxtBox.Text = "Season " + seasons[selectedIndex].StartDate.ToString("yy") + "/" + seasons[selectedIndex].EndDate.ToString("yy");
            if (selectedIndex.Equals(seasons.Count - 1))
                rightButton.IsEnabled = false;
            SetStatisticsLabels();
        }
        private void SetInit()
        {
            rightButton.IsEnabled = false;
            if (seasons.Count == 1)
            {
                leftButton.IsEnabled = false;
            }
            else if (seasons.Count == 0)
            {
                seasonTxtBox.Text = "No Seasons";
                selectedIndex = -1;
                leftButton.IsEnabled = false;
                return;
            }
            selectedIndex = seasons.IndexOf(new SeasonDAO().GetCurrentSeason());
            if (selectedIndex < seasons.Count - 1 && selectedIndex != -1)
                rightButton.IsEnabled = true;
            seasonTxtBox.Text = "Season " + seasons[selectedIndex].StartDate.ToString("yy") + "/" + seasons[selectedIndex].EndDate.ToString("yy");
            SetStatisticsLabels();
        }

        private void PreviousSeason(object sender, RoutedEventArgs e)
        {
            if (!rightButton.IsEnabled)
                rightButton.IsEnabled = true;
            selectedIndex--;
            seasonTxtBox.Text = "Season " + seasons[selectedIndex].StartDate.ToString("yy") + "/" + seasons[selectedIndex].EndDate.ToString("yy");
            if (selectedIndex.Equals(0))
                leftButton.IsEnabled = false;
            SetStatisticsLabels();
        }

        private void SetStatisticsLabelsForCompetition()
        {
            ClubStatistics stats = new ClubStatisticsDAO().GetClubStatisticsForCompetition(cmbxCategory.SelectedItem as Team, cmbxCompetiton.SelectedItem as Competition, cmbxGameType.SelectedItem as string);
            if (stats != null)
            {
                goalsLabel.Content = "Goals Scored: " + stats.GoalsScored;
                concededGoalsLabel.Content = "Goals Conceded: " + stats.GoalsConceded;
                totalShotsLabel.Content = "Goal Attempts: " + stats.ClubShots;
                shotsOnTargetLabel.Content = "Shots on Target: " + stats.ClubShotsOnGoal;
                shotsOffTargetLabel.Content = "Shots off Target: " + (stats.ClubShots - stats.ClubShotsOnGoal);
                yellowCardsLabel.Content = "Yellow Cards: " + stats.YellowCards;
                redCardsLabel.Content = "Red Cards: " + stats.RedCards;
                penaltiesLabel.Content = "Penalties: " + stats.TotalPenalities;
                goalsPerGameLabel.Content = "Goals Scored: " + Math.Round(stats.GoalsScoredPerGame, 2);
                concededGoalsLabelPerGame.Content = "Goals Conceded: " + Math.Round(stats.GoalsConcededPerGame, 2);
                totalShotsLabelPerGame.Content = "Goal Attempts: " + Math.Round(stats.ClubShotsPerGame, 2);
                shotsOnTargetLabelPerGame.Content = "Shots on Target: " + Math.Round(stats.ClubShotsOnGoalPerGame, 2);
                shotsOffTargetLabelPerGame.Content = "Shots off Target: " + Math.Round((stats.ClubShotsPerGame - stats.ClubShotsOnGoalPerGame), 2);
                yellowCardsLabelPerGame.Content = "Yellow Cards: " + Math.Round(stats.YellowCardsPerGame, 2);
                redCardsLabelPerGame.Content = "Red Cards: " + Math.Round(stats.RedCardsPerGame, 2);
                penaltiesLabelPerGame.Content = "Penalties: " + Math.Round(stats.PenalitiesPerGame, 2);
                PossesionPerGameLabel.Content = "Possesion: " + Math.Round(stats.BallPossessionPerGame, 2) + "%";
                DataContext = new MyViewModel(stats.Wins, stats.Draws, stats.Loses);
            }
            else
            {
                new CustomMessageBox("Error", "There is no statistics for that combination", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                ResetLabels();
            }
        }

        private void ResetLabels()
        {
            goalsLabel.Content = "Goals Scored: N/A";
            concededGoalsLabel.Content = "Goals Conceded:  N/A";
            totalShotsLabel.Content = "Goal Attempts: N/A";
            shotsOnTargetLabel.Content = "Shots on Target:  N/A";
            shotsOffTargetLabel.Content = "Shots off Target:  N/A";
            yellowCardsLabel.Content = "Yellow Cards:  N/A";
            redCardsLabel.Content = "Red Cards:  N/A"; 
            penaltiesLabel.Content = "Penalties:  N/A";
            goalsPerGameLabel.Content = "Goals Scored:  N/A";
            concededGoalsLabelPerGame.Content = "Goals Conceded:  N/A";
            totalShotsLabelPerGame.Content = "Goal Attempts:  N/A";
            shotsOnTargetLabelPerGame.Content = "Shots on Target:  N/A";
            shotsOffTargetLabelPerGame.Content = "Shots off Target:  N/A";
            yellowCardsLabelPerGame.Content = "Yellow Cards:  N/A";
            redCardsLabelPerGame.Content = "Red Cards:  N/A";
            penaltiesLabelPerGame.Content = "Penalties:  N/A";
            PossesionPerGameLabel.Content = "Possesion:  N/A";
            DataContext = new MyViewModel(0, 0, 0);
        }
        private void SetStatisticsLabels()
        {
            ClubStatistics stats = new ClubStatisticsDAO().GetClubStatisticsBySeason(seasons[selectedIndex], cmbxCategory.SelectedItem as Team, cmbxGameType.SelectedItem as string);
            if (stats != null)
            {
                goalsLabel.Content = "Goals Scored: " + stats.GoalsScored;
                concededGoalsLabel.Content = "Goals Conceded: " + stats.GoalsConceded;
                totalShotsLabel.Content = "Goal Attempts: " + stats.ClubShots;
                shotsOnTargetLabel.Content = "Shots on Target: " + stats.ClubShotsOnGoal;
                shotsOffTargetLabel.Content = "Shots off Target: " + (stats.ClubShots - stats.ClubShotsOnGoal);
                yellowCardsLabel.Content = "Yellow Cards: " + stats.YellowCards;
                redCardsLabel.Content = "Red Cards: " + stats.RedCards;
                penaltiesLabel.Content = "Penalties: " + stats.TotalPenalities;
                goalsPerGameLabel.Content = "Goals Scored: " + Math.Round(stats.GoalsScoredPerGame, 2);
                concededGoalsLabelPerGame.Content = "Goals Conceded: " + Math.Round(stats.GoalsConcededPerGame, 2);
                totalShotsLabelPerGame.Content = "Goal Attempts: " + Math.Round(stats.ClubShotsPerGame, 2);
                shotsOnTargetLabelPerGame.Content = "Shots on Target: " + Math.Round(stats.ClubShotsOnGoalPerGame, 2);
                shotsOffTargetLabelPerGame.Content = "Shots off Target: " + Math.Round((stats.ClubShotsPerGame - stats.ClubShotsOnGoalPerGame), 2);
                yellowCardsLabelPerGame.Content = "Yellow Cards: " + Math.Round(stats.YellowCardsPerGame, 2);
                redCardsLabelPerGame.Content = "Red Cards: " + Math.Round(stats.RedCardsPerGame, 2);
                penaltiesLabelPerGame.Content = "Penalties: " + Math.Round(stats.PenalitiesPerGame, 2);
                PossesionPerGameLabel.Content = "Possesion: " + Math.Round(stats.BallPossessionPerGame, 2) + "%";
                DataContext = new MyViewModel(stats.Wins, stats.Draws, stats.Loses);
            }
        }

        private void CategoryComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (formLoaded && cmbxGameType.SelectedItem != null && cmbxCompetiton.SelectedItem != null)
            {
                if(cmbxCompetiton.SelectedItem is string)
                    SetStatisticsLabels();
                else
                    SetStatisticsLabelsForCompetition();
            }
        }

        private void GameTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (formLoaded && cmbxCategory.SelectedItem != null && cmbxCompetiton.SelectedItem != null)
            {
                if (cmbxCompetiton.SelectedItem is string)
                    SetStatisticsLabels();
                else
                    SetStatisticsLabelsForCompetition();
            }
        }

        private void CompetitionComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (formLoaded && cmbxCategory.SelectedItem != null && cmbxGameType.SelectedItem != null)
            {
                if (cmbxCompetiton.SelectedItem is string)
                    SetStatisticsLabels();
                else
                    SetStatisticsLabelsForCompetition();
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            seasons = new SeasonDAO().GetSeasons();
            InitComboBoxes();
            SetInit();
            formLoaded = true;
        }
    }


}



