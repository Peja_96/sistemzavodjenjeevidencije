﻿using FootApp.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FootApp.GUI
{
    /// <summary>
    /// Interaction logic for NewsItemControl.xaml
    /// </summary>
    public partial class NewsItemControl : UserControl
    {

        Object data;
        public NewsItemControl(Object a)
        {
            InitializeComponent();
            data = a;
            DataContext = data;
        }

      
        /*public class Announcement
        {
            public string Content { get; set; }
            public DateTime Published { get; set; }

            public Announcement(string content) {
                Content = content;
                Published = DateTime.Now;
            }
        }*/

        private void TextBox_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (data is Announcement)
                new ShowNewsWindow((Announcement)data, this).ShowDialog();
            else
            {
                new ShowGameWindow((Game)data).ShowDialog();
            }

        }

        private void TextBox_MouseEnter(object sender, MouseEventArgs e)
        {
            tbxText.Background = (Brush)new BrushConverter().ConvertFrom("#FF3B403B");
            tbxText.Background.Opacity = 0.7;
        }

        private void TextBox_MouseLeave(object sender, MouseEventArgs e)
        {
            tbxText.Background.Opacity = 0.3;
        }
    }
}
