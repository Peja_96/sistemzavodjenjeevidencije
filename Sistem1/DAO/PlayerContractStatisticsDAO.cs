﻿using FootApp.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DAO
{
    class PlayerContractStatisticsDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public PlayerContractStatistics GetPlayerContractStatistics()
        {
            PlayerContractStatistics playerContractStatistics = null;
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from playercontract_statistics";
            command.Prepare();
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                playerContractStatistics = new PlayerContractStatistics
                { 
                    MaxSalary=reader.GetDecimal("MaxSalary"),
                    MinSalary=reader.GetDecimal("MinSalary"),
                    SalaryPerPlayer=reader.GetDecimal("SalaryPerPlayer"),
                    NumberOfContract=reader.GetInt32("NumberOfContract"),
                    NumberOfPlayer=reader.GetInt32("NumberOfPlayer")
                };
            }
            reader.Close();
            playerContractStatistics.AllSalary = GetSumSalary();
            return playerContractStatistics;
        }
        private decimal GetSumSalary()
        {
            decimal result = 0;
            List<Player> players = new PlayerDAO().GetPlayers();
            players.AddRange(new PlayerDAO().GetInactivePlayers());
            foreach (Player player in players)
            {

                List<PlayerContract> playerContracts = new PlayerContractDAO().GetPlayerContracts(player);

                foreach (PlayerContract playerContract in playerContracts)
                {
                    while (playerContract.StartDate < playerContract.EndDate)
                    {
                        result += playerContract.Salary;
                        playerContract.StartDate = playerContract.StartDate.AddMonths(1);
                    }
                }
            }
            return result;
        }
        private decimal GetSumSalaryPerPlayer(Player player)
        {
            decimal result = 0;
            List<PlayerContract> playerContracts = new PlayerContractDAO().GetPlayerContracts(player);

            foreach (PlayerContract playerContract in playerContracts)
            {
                while (playerContract.StartDate < playerContract.EndDate)
                {
                    result += playerContract.Salary;
                    playerContract.StartDate = playerContract.StartDate.AddMonths(1);
                }
            }

            return result;
        }
        private decimal GetSumSalaryPerYear(int year)
        {
            decimal result = 0;

            List<PlayerContract> playerContracts = new PlayerContractDAO().GetContractsByYear(year);

            foreach (PlayerContract playerContract in playerContracts)
            {
                while (playerContract.StartDate.Year < year)
                    playerContract.StartDate = playerContract.StartDate.AddMonths(1);
                while (playerContract.StartDate < playerContract.EndDate && playerContract.StartDate.Year.Equals(year))
                {
                    result += playerContract.Salary;
                    playerContract.StartDate = playerContract.StartDate.AddMonths(1);
                }
            }
            return result;
        }
        public List<PlayerContractStatistics> GetPlayerContractStatisticsPerYear()
        {
            List<PlayerContractStatistics> playerContractStatistics = new List<PlayerContractStatistics>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from playercontract_statistics_per_year";
            command.Prepare();
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                playerContractStatistics.Add( new PlayerContractStatistics
                {
                    MaxSalary = reader.GetDecimal("MaxSalary"),
                    MinSalary = reader.GetDecimal("MinSalary"),
                    SalaryPerPlayer = reader.GetDecimal("SalaryPerPlayer"),
                    NumberOfContract = reader.GetInt32("NumberOfContract"),
                    NumberOfPlayer = reader.GetInt32("NumberOfPlayer"),
                    Year= reader.GetInt32("Year")
                });
            }
            reader.Close();
            foreach (var item in playerContractStatistics) item.AllSalary = GetSumSalaryPerYear(item.Year);
            return playerContractStatistics;
        }
    }
}
