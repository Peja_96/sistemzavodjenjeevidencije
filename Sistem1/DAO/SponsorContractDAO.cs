﻿using FootApp.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FootApp.DAO
{
    public class SponsorContractDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<SponsorContract> GetSponsorContracts()
        {
            List<SponsorContract> sponsorContracts = new List<SponsorContract>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from sponsor_contract";

            MySqlDataReader reader = command.ExecuteReader();
            while(reader.Read())
            {
                sponsorContracts.Add(new SponsorContract
                {
                    EndDate = reader.GetDateTime("END"),
                    StartDate = reader.GetDateTime("Start"),
                    SponsorID = reader.GetInt32("SponsorID"),
                    Investment = reader.GetDecimal("Investment"),
                    Name = reader.GetString("Name")

                });
            }
            reader.Close();

            return sponsorContracts;
        }

        public List<SponsorContract> GetCurrentSponsorContracts()
        {
            List<SponsorContract> sponsorContracts = new List<SponsorContract>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select Start, End, Investment, SponsorID,Name from sponsor_contract where iscurrentsponsor = 1";

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                sponsorContracts.Add(new SponsorContract
                {
                    EndDate = reader.GetDateTime("End"),
                    StartDate = reader.GetDateTime("Start"),
                    SponsorID = reader.GetInt32("SponsorID"),
                    Investment = reader.GetDecimal("Investment"),
                    Name = reader.GetString("Name")
                });
            }
            reader.Close();

            return sponsorContracts;
        }

        public void AddSponsorContract(SponsorContract sponsorContract)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "insert into sponsorcontract values (@StartDate, @EndDate, @Investment, @SponsorID)";
            command.Parameters.AddWithValue("@StartDate", sponsorContract.StartDate);
            command.Parameters.AddWithValue("@EndDate", sponsorContract.EndDate);
            command.Parameters.AddWithValue("@Investment", sponsorContract.Investment);
            command.Parameters.AddWithValue("@SponsorID", sponsorContract.SponsorID);
            command.Prepare();
            command.ExecuteNonQuery();
        }

        //SponsorContract parameter already has new Investment
        public void ModifyInvestment(SponsorContract sponsorContract)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "update sponsorcontract set Investment = @Investment where Start = @Start and END = @End and SponsorID = @SponsorID";
            command.Parameters.AddWithValue("@Start", sponsorContract.StartDate);
            command.Parameters.AddWithValue("@End", sponsorContract.EndDate);
            command.Parameters.AddWithValue("@SponsorID", sponsorContract.SponsorID);
            command.Parameters.AddWithValue("@Investment", sponsorContract.Investment);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch(MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }
        public List<SponsorContract> GetTenSponsorContracts(int offset)
        {
            List<SponsorContract> sponsorContracts = new List<SponsorContract>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from sponsor_contract limit 10 offset @Offset";
            command.Parameters.AddWithValue("@Offset",offset);
            command.Prepare();
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                sponsorContracts.Add(new SponsorContract
                {
                    EndDate = reader.GetDateTime("END"),
                    StartDate = reader.GetDateTime("Start"),
                    SponsorID = reader.GetInt32("SponsorID"),
                    Investment = reader.GetDecimal("Investment"),
                    Name = reader.GetString("Name")

                });
            }
            reader.Close();

            return sponsorContracts;
        }

        public List<SponsorContract> GetSponsorContractsBySponsorName(string name)
        {
            List<SponsorContract> sponsorContracts = new List<SponsorContract>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select sponsorcontract.*, name, id from sponsorcontract inner join sponsor on id = sponsorid where name like @NameParam";
            command.Parameters.AddWithValue("@NameParam", new Regex("%" + name + "%"));
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                sponsorContracts.Add(new SponsorContract()
                {
                    StartDate = reader.GetDateTime("Start"),
                    EndDate = reader.GetDateTime("End"),
                    Investment = reader.GetDecimal("Investment"),
                    Name = reader.GetString("Name"),
                    SponsorID = reader.GetInt32("id"),
                });
            }
            reader.Close();

            return sponsorContracts;
        }

        public void RemoveSponsorContract(SponsorContract sponsorContract)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "delete from sponsorcontract where start = @StartDate and end = @EndDate and SponsorID = @SponsorID";
            command.Parameters.AddWithValue("@StartDate", sponsorContract.StartDate);
            command.Parameters.AddWithValue("@EndDate", sponsorContract.EndDate);
            command.Parameters.AddWithValue("@SponsorID", sponsorContract.SponsorID);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }
    }
}
