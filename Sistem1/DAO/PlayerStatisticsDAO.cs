﻿using FootApp.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DAO
{
    class PlayerStatisticsDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public PlayerStatistics GetPlayerStatistics(Player player)
        {
            int gamesPlayed = GetTotalGamesPlayed(player);
            if (gamesPlayed == 0)
                return new PlayerStatistics(player);
            return new PlayerStatistics
            {
                Player = player,
                GamesPlayed = gamesPlayed,
                GoalsPerGame = GetAverageFieldStatistics(player, "Goals"),
                PassesPerGame = GetAverageFieldStatistics(player, "TotalPasses"),
                AssistsPerGame = GetAverageFieldStatistics(player, "Assists"),
                ShotsPerGame = GetAverageFieldStatistics(player, "TotalShots"),
                FoulsPerGame = GetAverageFieldStatistics(player, "Fouls"),
                ShotAccuracy = GetShotAccuracy(player),
                SuccessfulPassesPercent = GetSuccessfulPassesPercent(player),
                TotalAssists = GetTotalFieldStatistics(player, "Assists"),
                TotalGoals = GetTotalFieldStatistics(player, "Goals"),
                TotalRedCards = GetTotalFieldStatistics(player, "RedCard"),
                TotalYellowCards = GetTotalFieldStatistics(player, "YellowCards")
            };
        }

        private int GetTotalGamesPlayed(Player player)
        {
            int gamesPlayed = 0;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select count(*) as GamesPlayed from playerperformance where playerid = @PlayerID  and PlayerCategory = @PlayerCategory";
            command.Parameters.AddWithValue("@PlayerID", player.ID);
            command.Parameters.AddWithValue("@PlayerCategory", player.Category);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
                gamesPlayed = reader.GetInt32("GamesPlayed");
            reader.Close();

            return gamesPlayed;
        }

        private double GetAverageFieldStatistics(Player player, string field)
        {
            double fieldValue = 0;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select round(avg(" + field + "), 2) as FieldValue from playerperformance where playerid = @PlayerID and playercategory = @PlayerCategory";
            command.Parameters.AddWithValue("@PlayerID", player.ID);
            command.Parameters.AddWithValue("@PlayerCategory", player.Category);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
                fieldValue = reader.IsDBNull(0) ? 0 : reader.GetDouble("FieldValue");
            reader.Close();

            return fieldValue;
        }

        private double GetShotAccuracy(Player player)
        {
            double shotsAccuracy = 0;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select round(sum(ShotsOnGoal)/sum(TotalShots) * 100, 2) as ShotAccuracy from playerperformance where playerid = @PlayerID and playercategory = @PlayerCategory";
            command.Parameters.AddWithValue("@PlayerID", player.ID);
            command.Parameters.AddWithValue("@PlayerCategory", player.Category);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
                shotsAccuracy = reader.IsDBNull(0) ? 0 : reader.GetDouble("ShotAccuracy");
            reader.Close();

            return shotsAccuracy;
        }

        private double GetSuccessfulPassesPercent(Player player)
        {
            double successfulPassesPercent = 0;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select round(sum(SuccessfulPasses)/sum(totalpasses) * 100, 2) as PassesAccuracy from playerperformance where playerid = @PlayerID and playercategory = @PlayerCategory";
            command.Parameters.AddWithValue("@PlayerID", player.ID);
            command.Parameters.AddWithValue("@PlayerCategory", player.Category);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
                successfulPassesPercent = reader.IsDBNull(0) ? -1 : reader.GetDouble("PassesAccuracy");
            reader.Close();

            return successfulPassesPercent;
        }

        private int GetTotalFieldStatistics(Player player, string field)
        {
            int fieldValue = 0;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select sum(" + field + ") as FieldValue from playerperformance where playerid = @PlayerID and playercategory = @PlayerCategory";
            command.Parameters.AddWithValue("@PlayerID", player.ID);
            command.Parameters.AddWithValue("@PlayerCategory", player.Category);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
                fieldValue = reader.IsDBNull(0) ? 0 : reader.GetInt32("FieldValue");
            reader.Close();

            return fieldValue;
        }

        public PlayerStatistics GetPlayerStatisticsForSeason(Player player, Season season)
        {
            int gamesPlayed = GetSeasonTotalGamesPlayed(player, season);
            if (gamesPlayed == 0)
                return new PlayerStatistics(player);
            return new PlayerStatistics
            {
                Player = player,
                GamesPlayed = gamesPlayed,
                GoalsPerGame = GetSeasonAverageFieldStatistics(player, season, "Goals"),
                FoulsPerGame = GetSeasonTotalFieldStatistics(player, season, "Fouls"),
                PassesPerGame = GetSeasonAverageFieldStatistics(player, season, "TotalPasses"),
                AssistsPerGame = GetSeasonAverageFieldStatistics(player, season, "Assists"),
                ShotsPerGame = GetSeasonAverageFieldStatistics(player, season, "TotalShots"),
                ShotAccuracy = GetSeasonShotAccuracy(player,season),
                SuccessfulPassesPercent = GetSeasonSuccessfulPassesPercent(player, season),
                TotalAssists = GetSeasonTotalFieldStatistics(player, season, "Assists"),
                TotalGoals = GetSeasonTotalFieldStatistics(player, season, "Goals"),
                TotalRedCards = GetSeasonTotalFieldStatistics(player, season, "RedCard"),
                TotalYellowCards = GetSeasonTotalFieldStatistics(player, season, "YellowCards"),
                TotalFouls= GetSeasonTotalFieldStatistics(player, season, "Fouls")
            };
        }

        private int GetSeasonTotalGamesPlayed(Player player, Season season)
        {
            int gamesPlayed = 0;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select count(*) as GamesPlayed from playerperformance inner join competition on id = competitionid where playerid = @PlayerID  and " +
                "PlayerCategory = @PlayerCategory and SeasonID = @SeasonID";
            command.Parameters.AddWithValue("@PlayerID", player.ID);
            command.Parameters.AddWithValue("@PlayerCategory", player.Category);
            command.Parameters.AddWithValue("@SeasonID", season.Id);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
                gamesPlayed = reader.GetInt32("GamesPlayed");
            reader.Close();

            return gamesPlayed;
        }


        private double GetSeasonAverageFieldStatistics(Player player, Season season, string field)
        {
            double fieldValue = 0;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select round(avg(" + field + "), 2) as FieldValue from playerperformance inner join competition on competitionid = id " +
                "where playerid = @PlayerID and playercategory = @PlayerCategory and seasonid = @SeasonID";
            command.Parameters.AddWithValue("@PlayerID", player.ID);
            command.Parameters.AddWithValue("@PlayerCategory", player.Category);
            command.Parameters.AddWithValue("@SeasonID", season.Id);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
                fieldValue = reader.IsDBNull(0) ? 0 : reader.GetDouble("FieldValue");
            reader.Close();

            return fieldValue;
        }

        private double GetSeasonShotAccuracy(Player player, Season season)
        {
            double shotAccuracy = 0;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select round(sum(ShotsOnGoal)/sum(TotalShots) * 100, 2) as ShotAccuracy from playerperformance inner join competition on id = competitionid " +
                "where playerid = @PlayerID and playercategory = @PlayerCategory and seasonid = @SeasonID";
            command.Parameters.AddWithValue("@PlayerID", player.ID);
            command.Parameters.AddWithValue("@PlayerCategory", player.Category);
            command.Parameters.AddWithValue("@SeasonID", season.Id);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
                shotAccuracy = reader.IsDBNull(0) ? 0 : reader.GetDouble("ShotAccuracy");
            reader.Close();

            return shotAccuracy;
        }

        private double GetSeasonSuccessfulPassesPercent(Player player, Season season)
        {
            double successfulPassesPercent = 0;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select round(sum(SuccessfulPasses)/sum(totalpasses) * 100, 2) as PassesAccuracy from playerperformance " +
                "inner join competition on id = competitionid where playerid = @PlayerID and playercategory = @PlayerCategory and seasonid = @SeasonID";
            command.Parameters.AddWithValue("@PlayerID", player.ID);
            command.Parameters.AddWithValue("@PlayerCategory", player.Category);
            command.Parameters.AddWithValue("@SeasonID", season.Id);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
                successfulPassesPercent = reader.IsDBNull(0) ? 0 : reader.GetDouble("PassesAccuracy");
            reader.Close();

            return successfulPassesPercent;
        }

        private int GetSeasonTotalFieldStatistics(Player player, Season season, string field)
        {
            int fieldValue = 0;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select sum(" + field + ") as FieldValue from playerperformance inner join competition on id = competitionid " +
                "where playerid = @PlayerID and playercategory = @PlayerCategory and seasonid = @SeasonID";
            command.Parameters.AddWithValue("@PlayerID", player.ID);
            command.Parameters.AddWithValue("@PlayerCategory", player.Category);
            command.Parameters.AddWithValue("@SeasonID", season.Id);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
                fieldValue = reader.IsDBNull(0) ? 0 : reader.GetInt32("FieldValue");
            reader.Close();

            return fieldValue;
        }
    }
}
