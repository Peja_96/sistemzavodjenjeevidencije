﻿using FootApp.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FootApp.DAO
{
    class EmployeeDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<Employee> GetEmployees()
        {
            List<Employee> employees = new List<Employee>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from employee inner join person on id = personid where isactive = 1";

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                employees.Add(new Employee()
                {
                    ID = reader.GetInt32("ID"),
                    Name = reader.GetString("Name"),
                    Surname = reader.GetString("Surname"),
                    DateOfBirth = reader.GetDateTime("DateOfBirth"),
                    Position = reader.GetString("Position"),
                    EmploymentRecordBook = reader.GetString("EmploymentRecordBook")
                });
            }
            reader.Close();

            return employees;
        }
        public void AddEmployee(Employee employee)
        {
            new PersonDAO().AddPerson(employee);
            DatabaseConnection db = DatabaseConnection.GetConnection();
            MySqlCommand sqlCommand = db.CreateCommand();
            sqlCommand.CommandText = "insert into employee(PersonID,employmentRecordBook,Position) values(@id,@employmentRecordBook,@position);";
            MySqlParameter id = new MySqlParameter() { ParameterName = "@id", Value = employee.ID, MySqlDbType = MySqlDbType.Int32 };
            MySqlParameter employmentRecordBook = new MySqlParameter() { ParameterName = "@employmentRecordBook", Value =employee.EmploymentRecordBook, MySqlDbType = MySqlDbType.VarChar };
            MySqlParameter position = new MySqlParameter() { ParameterName = "@position", Value = employee.Position, MySqlDbType = MySqlDbType.VarChar };
            sqlCommand.Parameters.AddRange(new object[] { id, employmentRecordBook,position });
            sqlCommand.Prepare();
            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                log.Warn(ex);
            }
        }
        public List<Employee> GetEmployeebyName(string name)
        {
            List<Employee> employees = new List<Employee>();
            DatabaseConnection db = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = db.CreateCommand();
            mySqlCommand.CommandText = "select * from employee inner join person on id = personid where concat(Name, ' ', Surname) like @NameParam and IsActive = 1;";
            mySqlCommand.Parameters.AddWithValue("@NameParam", new Regex("%" + name + "%"));
            MySqlDataReader reader = mySqlCommand.ExecuteReader();
            while (reader.Read())
            {
                employees.Add(new Employee()
                {
                    ID = reader.GetInt32("ID"),
                    Name = reader.GetString("Name"),
                    Surname = reader.GetString("Surname"),
                    DateOfBirth = reader.GetDateTime("DateOfBirth"),
                    Position = reader.GetString("Position"),
                    EmploymentRecordBook = reader.GetString("EmploymentRecordBook")
                });
            }
            reader.Close();

            return employees;
        }
        public List<Employee> GetEmployeebyPosition(string position)
        {
            List<Employee> employees = new List<Employee>();
            DatabaseConnection db = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = db.CreateCommand();
            mySqlCommand.CommandText = "select * from employee inner join person on id = personid where Position='"+ position + "' and IsActive=1;";
            MySqlDataReader reader = mySqlCommand.ExecuteReader();
            while (reader.Read())
            {
                employees.Add(new Employee()
                {
                    ID = reader.GetInt32("ID"),
                    Name = reader.GetString("Name"),
                    Surname = reader.GetString("Surname"),
                    DateOfBirth = reader.GetDateTime("DateOfBirth"),
                    Position = reader.GetString("Position"),
                    EmploymentRecordBook = reader.GetString("EmploymentRecordBook")
                });
            }
            reader.Close();

            return employees;
        }
        public List<Employee> GetTenEmployees(int offset)
        {
            List<Employee> employees = new List<Employee>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from employee inner join person on id = personid where isactive = 1 limit 10 offset @offset";
            command.Parameters.AddWithValue("@offset", offset);
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                employees.Add(new Employee()
                {
                    ID = reader.GetInt32("ID"),
                    Name = reader.GetString("Name"),
                    Surname = reader.GetString("Surname"),
                    DateOfBirth = reader.GetDateTime("DateOfBirth"),
                    Position = reader.GetString("Position"),
                    EmploymentRecordBook = reader.GetString("EmploymentRecordBook")
                });
            }
            reader.Close();

            return employees;
        }
        public void UpdateEmployeeInfo(Employee newEmployee)
        {
            
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "UPDATE employee SET EmploymentRecordBook = @book, Position = @position WHERE PersonID = @id;";
            command.Parameters.AddWithValue("@book", newEmployee.EmploymentRecordBook);
            command.Parameters.AddWithValue("@position", newEmployee.Position);
            command.Parameters.AddWithValue("@id", newEmployee.ID);
            command.Prepare();
            command.ExecuteNonQuery();
            command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "UPDATE person SET Name = @name, Surname = @surname, DateOfBirth = @Date WHERE ID = @id";
            command.Parameters.AddWithValue("@name", newEmployee.Name);
            command.Parameters.AddWithValue("@surname", newEmployee.Surname);
            command.Parameters.AddWithValue("@Date", newEmployee.DateOfBirth);
            command.Parameters.AddWithValue("@id", newEmployee.ID);
            command.Prepare();
            command.ExecuteNonQuery();

        }
        public void RemoveEmployee(Employee employee)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "update employee set isactive = 0 where id = @ID";
            command.Parameters.AddWithValue("@ID", employee.ID);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }
    }
}
