﻿using FootApp.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DAO
{
    class TeamDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<Team> GetTeams()
        {
            List<Team> teams = new List<Team>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from team";
            MySqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                teams.Add(new Team()
                {
                    Category = reader.GetString("Category"),
                    Club = Club.FootballClub
                });
            }
            reader.Close();
            return teams;
        }

        public List<Player> GetPlayersByTeam(Team team)
        {

            List<Player> players = new List<Player>();
            DatabaseConnection db = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = db.CreateCommand();
            mySqlCommand.CommandText = "select * from player inner join person on id = personid where category = @Category and isactive = 1";
            mySqlCommand.Parameters.AddWithValue("@Category", team.Category);
            mySqlCommand.Prepare();

            MySqlDataReader mySqlData = mySqlCommand.ExecuteReader();
            while (mySqlData.Read())
            {
                players.Add(new Player()
                {
                    ID = mySqlData.GetInt32("PersonId"),
                    Name = mySqlData.GetString("Name"),
                    Surname = mySqlData.GetString("Surname"),
                    DateOfBirth = mySqlData.GetDateTime("DateOfBirth"),
                    RegistrationNumber = mySqlData.GetString("RegistrationNumber"),
                    Position = mySqlData.GetString("Position"),
                    IsBought = mySqlData.GetBoolean("IsBought"),
                    IsInjured = mySqlData.GetBoolean("IsInjured"),
                    Height = mySqlData.GetInt32("Height"),
                    Category = team.Category
                });
            }
            mySqlData.Close();

            return players;
        }

        public List<Coach> GetCoachesByTeam(Team team)
        {
            List<Coach> coaches = new List<Coach>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from coach_by_team where category = @Category";
            MySqlParameter category = new MySqlParameter("@Category", MySqlDbType.VarChar) { Value = team.Category };
            command.Parameters.Add(category);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            while(reader.Read())
            {
                coaches.Add(new Coach()
                {
                    CoachLicenceNumber = reader.GetString("CoachLicenceNumber"),
                    DateOfBirth = reader.GetDateTime("DateOfBirth"),
                    EmploymentRecordBook = reader.GetString("EmploymentRecordBook"),
                    ID = reader.GetInt32("ID"),
                    Name = reader.GetString("Name"),
                    Surname = reader.GetString("Surname"),
                    Position = reader.GetString("Position")
                });
            }
            reader.Close();

            return coaches;
        }

        public Team GetTeamByCategory(string category)
        {
            Team team = null;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from team where category = @Category";
            command.Parameters.AddWithValue("@Category", category);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                team = new Team()
                {
                    Category = reader.GetString("Category"),
                    Club = Club.FootballClub
                };
            }
            reader.Close();

            return team;
        }
    }
}
