﻿using FootApp.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FootApp.DAO
{
    //ne valja ako ima vise ugovora
    class PlayerContractDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public decimal GetSalaryForPlayer(Player player)
        {
            decimal salary = 0;
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select salary from playercontract where PlayerId = @Id";
            MySqlParameter id = new MySqlParameter("@Id", MySqlDbType.Int32) { Value = player.ID };
            command.Parameters.Add(id);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
                salary = reader.GetDecimal("salary");
            reader.Close();

            return salary;
        }
        public void AddPlayerContract(PlayerContract playerContract)
        {
            DatabaseConnection db = DatabaseConnection.GetConnection();
            MySqlCommand command = db.CreateCommand();
            command.CommandText = "Insert into playercontract(playerid,startdate,enddate,salary) values(@playerid,@startdate,@enddate,@salary)";
            MySqlParameter idemplyee = new MySqlParameter() { ParameterName = "@playerid", Value = playerContract.PlayerID, MySqlDbType = MySqlDbType.Int32 };
            MySqlParameter startDate = new MySqlParameter() { ParameterName = "@startdate", Value = playerContract.StartDate, MySqlDbType = MySqlDbType.Date };
            MySqlParameter endDate = new MySqlParameter() { ParameterName = "@enddate", Value = playerContract.EndDate, MySqlDbType = MySqlDbType.Date };
            MySqlParameter salary = new MySqlParameter() { ParameterName = "@salary", Value = playerContract.Salary, MySqlDbType = MySqlDbType.Double };
            command.Parameters.AddRange(new object[] { idemplyee, startDate, endDate, salary });
            command.Prepare();
            command.ExecuteNonQuery();
        }
        public List<PlayerContract> GetContracts()
        {
            List<PlayerContract> contracts = new List<PlayerContract>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from player_contract";
            List<Player> players = new PlayerDAO().GetPlayers();

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                contracts.Add(new PlayerContract()
                {
                    PlayerID = reader.GetInt32("PlayerId"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Salary = reader.GetDecimal("Salary"),
                    Name = reader.GetString("Name"),
                    Surname = reader.GetString("Surname")
                });
            }
            reader.Close();

            return contracts;
        }

        public PlayerContract GetPlayerContract(Player player, DateTime startDate, DateTime endDate)
        {
            PlayerContract playerContract = null;
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from playercontract where playerid = @PlayerID and startdate = @StartDate and enddate = @EndDate";
            command.Parameters.AddWithValue("@PlayerID", player.ID);
            command.Parameters.AddWithValue("@StartDate", startDate);
            command.Parameters.AddWithValue("@EndDate", endDate);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                playerContract = new PlayerContract()
                {
                    EndDate = endDate,
                    StartDate = startDate,
                    PlayerID = player.ID,
                    Salary = reader.GetDecimal("Salary")
                };
            }
            reader.Close();

            return playerContract;
        }
        public List<PlayerContract> GetPlayerContracts(Player player)
        {
            List<PlayerContract> playerContracts = new List<PlayerContract>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from playercontract where playerid = @PlayerID";
            command.Parameters.AddWithValue("@PlayerID", player.ID);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                playerContracts.Add(  new PlayerContract()
                {
                    EndDate = reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    PlayerID = player.ID,
                    Salary = reader.GetDecimal("Salary")
                });
            }
            reader.Close();

            return playerContracts;
        }
        public List<PlayerContract> GetContractsByYear(int year)
        {
            List<PlayerContract> contracts = new List<PlayerContract>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from playercontract where year(StartDate)=@Year or year(EndDate)=@Year ";
            command.Parameters.AddWithValue("@Year", year);
            command.Prepare();
            List<Player> players = new PlayerDAO().GetPlayers();

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                contracts.Add(new PlayerContract()
                {
                    PlayerID = reader.GetInt32("PlayerId"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Salary = reader.GetDecimal("Salary")
                });
            }
            reader.Close();

            return contracts;
        }
        public List<PlayerContract> GetTenContracts(int offset)
        {
            List<PlayerContract> contracts = new List<PlayerContract>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from player_contract limit 10 offset @Offset";
            command.Parameters.AddWithValue("@Offset", offset);
            command.Prepare();
            List<Player> players = new PlayerDAO().GetPlayers();

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                contracts.Add(new PlayerContract()
                {
                    PlayerID = reader.GetInt32("PlayerId"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Salary = reader.GetDecimal("Salary"),
                    Name = reader.GetString("Name"),
                    Surname = reader.GetString("Surname")
                });
            }
            reader.Close();

            return contracts;
        }

        public List<PlayerContract> GetPlayerContractsByPlayerName(string name)
        {
            List<PlayerContract> playerContracts = new List<PlayerContract>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select playercontract.*, name, surname from playercontract inner join person on id = playerid where concat(name, ' ', surname) like @NameParam";
            command.Parameters.AddWithValue("@NameParam", new Regex("%" + name + "%"));
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                playerContracts.Add(new PlayerContract()
                {
                    PlayerID = reader.GetInt32("PlayerId"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Salary = reader.GetDecimal("Salary"),
                    Name = reader.GetString("Name"),
                    Surname = reader.GetString("Surname")
                });
            }
            reader.Close();

            return playerContracts;
        }

        public void RemovePlayerContract(PlayerContract playerContract)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "delete from playercontract where startdate = @StartDate and enddate = @EndDate and PlayerID = @PlayerID";
            command.Parameters.AddWithValue("@StartDate", playerContract.StartDate);
            command.Parameters.AddWithValue("@EndDate", playerContract.EndDate);
            command.Parameters.AddWithValue("@PlayerID", playerContract.PlayerID);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }
    }
}
