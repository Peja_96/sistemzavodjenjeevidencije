﻿using FootApp.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DAO
{
    class MembershipFeeValueDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<MembershipFeeValue> GetMembershipFeeHistory()
        {
            List<MembershipFeeValue> membershipFeeValues = new List<MembershipFeeValue>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from membershipfeevalue";

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                membershipFeeValues.Add(new MembershipFeeValue
                {
                    ID = reader.GetInt32("ID"),
                    ClubID = Club.FootballClub.ID,
                    EndDate = reader.IsDBNull(2) ? new DateTime() : reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Value = reader.GetDecimal("Value")
                });
            }
            reader.Close();

            return membershipFeeValues;
        }

        public decimal GetValueForMonth(DateTime date)
        {
            decimal value = 0;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select Value from membershipfeevalue where startdate <= @Date and (enddate >= @Date or enddate is null)";
            command.Parameters.AddWithValue("@Date", date);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
                value = reader.IsDBNull(0) ? 0 : reader.GetDecimal(0);
            reader.Close();

            return value;
        }

        public MembershipFeeValue GetCurrentMembershipFeeValue()
        {
            MembershipFeeValue membershipFeeValue = null;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from membershipfeevalue where startdate <= curdate() and (enddate >= curdate() or enddate is null)";
            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                membershipFeeValue = new MembershipFeeValue
                {
                    ID = reader.GetInt32("ID"),
                    ClubID = Club.FootballClub.ID,
                    EndDate = reader.IsDBNull(2) ? new DateTime() : reader.GetDateTime("EndDate"),
                    StartDate = reader.GetDateTime("StartDate"),
                    Value = reader.GetDecimal("Value")
                };
            }
            reader.Close();

            return membershipFeeValue;
        }

        public void AddNewMembershipFeeValue(MembershipFeeValue membershipFeeValue)
        {
            var currentFee = GetCurrentMembershipFeeValue();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "insert into membershipfeevalue values(null, @StartDate, null, @Value, @ClubID)";
            command.Parameters.AddWithValue("@StartDate", membershipFeeValue.StartDate);
            command.Parameters.AddWithValue("@Value", membershipFeeValue.Value);
            command.Parameters.AddWithValue("@ClubID", membershipFeeValue.ClubID);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
                membershipFeeValue.ID = (int)command.LastInsertedId;
                if (currentFee == null)
                    return;

                DateTime endDate = DateTime.Now.AddMonths(1);
                endDate = new DateTime(endDate.Year, endDate.Month, 1).AddDays(-1);
                command.CommandText = "update membershipfeevalue set enddate = @EndDate where id = @ID";
                command.Parameters.AddWithValue("@ID", GetCurrentMembershipFeeValue().ID);
                command.Parameters.AddWithValue("@EndDate", endDate);
                command.Prepare();
                command.ExecuteNonQuery();
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }

        public bool? ResetMembershipFeeValueChange()
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "delete from membershipfeevalue where startdate > curdate() and enddate is null";
            try
            {
                if (command.ExecuteNonQuery() == 0)
                    return false;
                command.CommandText = "update membershipfeevalue set enddate = null where id = @ID";
                command.Parameters.AddWithValue("@ID", GetCurrentMembershipFeeValue().ID);
                command.Prepare();
                command.ExecuteNonQuery();
                return true;
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
                return null;
            }
        }
    }
}
