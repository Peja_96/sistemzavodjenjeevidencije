﻿using FootApp.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DAO
{
    public class SeasonDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void AddSeason(Season season)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "insert into season values (null, @StartDate, @EndDate)";
            command.Parameters.AddWithValue("@StartDate", season.StartDate);
            command.Parameters.AddWithValue("@EndDate", season.EndDate);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
                season.Id = (int)command.LastInsertedId;
            }
            catch(MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }

        public List<Season> GetSeasons()
        {
            List<Season> seasons = new List<Season>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from season order by StartDate";

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                seasons.Add(new Season
                {
                    EndDate = reader.GetDateTime("EndDate"),
                    Id = reader.GetInt32("ID"),
                    StartDate = reader.GetDateTime("StartDate")
                });
            }
            reader.Close();

            return seasons;
        }

        public Season GetCurrentSeason()
        {
            Season season = null;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from season where StartDate <  curdate() and curdate() < EndDate";

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                season = new Season
                {
                    EndDate = reader.GetDateTime("EndDate"),
                    Id = reader.GetInt32("ID"),
                    StartDate = reader.GetDateTime("StartDate")
                };
            }
            reader.Close();

            return season;
        }

        public Season GetSeasonByID(int id)
        {
            Season season = null;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from season where id = @ID";
            command.Parameters.AddWithValue("@ID", id);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                season = new Season
                {
                    EndDate = reader.GetDateTime("EndDate"),
                    Id = id,
                    StartDate = reader.GetDateTime("StartDate")
                };
            }
            reader.Close();

            return season;
        }
        public Season GetSeasonByDate(int startDate,int endDate)
        {
            Season season = null ;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "SELECT * FROM season where year(StartDate) = @Start and year(EndDate)= @End;";
            command.Parameters.AddWithValue("@Start", startDate);
            command.Parameters.AddWithValue("@End", endDate);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                season = new Season
                {
                    EndDate = reader.GetDateTime("EndDate"),
                    Id = reader.GetInt32("ID"),
                    StartDate = reader.GetDateTime("StartDate")
                };
            }
            reader.Close();

            return season;
        }
    }
}
