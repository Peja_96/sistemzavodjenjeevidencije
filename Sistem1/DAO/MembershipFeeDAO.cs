﻿using FootApp.DAO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    class MembershipFeeDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void AddMembershipFee(MembershipFee membershipFee)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "insert into membershipfee (PlayerID, Category, Month, Year, Value, PaymentDate) VALUES (@PlayerID, @Category, @Month, @Year, @Value,@PaymentDate )";
            command.Parameters.AddWithValue("@PlayerID", membershipFee.PlayerID);
            command.Parameters.AddWithValue("@Category", membershipFee.Category);
            command.Parameters.AddWithValue("@Month", membershipFee.Month);
            command.Parameters.AddWithValue("@Year", membershipFee.Year);
            command.Parameters.AddWithValue("@Value", membershipFee.Value);
            command.Parameters.AddWithValue("@PaymentDate", membershipFee.PaymentDate);
            command.Prepare();
            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                log.Warn(ex);
            }
        }
        public List<MembershipFee> GetMembershipFees()
        {
            List<MembershipFee> membershipFees = new List<MembershipFee>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from membershipfee";
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                membershipFees.Add(new MembershipFee
                {
                    PlayerID = reader.GetInt32("PlayerID"),
                    Category = reader.GetString("Category"),
                    Month = reader.GetInt32("Month"),
                    Year = reader.GetInt32("Year"),
                    PaymentDate = reader.GetDateTime("PaymentDate"),
                    Value = reader.GetDecimal("Value")

                });
            }
            reader.Close();
            return membershipFees;
        }
        public List<MembershipFee> GetLastPaymentForAllPlayers()
        {
            List<MembershipFee> membershipFees = new List<MembershipFee>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from membershipfee m where date(concat(year, '-', month, '-01')) = (select max(date(concat(year, '-', month, '-01'))) from membershipfee where playerid = m.playerid) group by playerid";
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                membershipFees.Add(new MembershipFee
                {
                    PlayerID = reader.GetInt32("PlayerID"),
                    Category = reader.GetString("Category"),
                    Month = reader.GetInt32("Month"),
                    Year = reader.GetInt32("Year"),
                    PaymentDate = reader.GetDateTime("PaymentDate"),
                    Value = reader.GetDecimal("Value")

                });
            }
            reader.Close();
            return membershipFees;
        }

        public void RemoveMembershipFee(MembershipFee membershipFee)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "delete from membershipfee where playerid = @PlayerID and category = @Category and month = @Month and year = @Year";
            command.Parameters.AddWithValue("@PlayerID", membershipFee.PlayerID);
            command.Parameters.AddWithValue("@Category", membershipFee.Category);
            command.Parameters.AddWithValue("@Month", membershipFee.Month);
            command.Parameters.AddWithValue("@Year", membershipFee.Year);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch(MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }

        }
        public MembershipFee GetLastPayemntForPlayer(Player player)
        {
            MembershipFee membershipFee = null;
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from membershipfee m where date(concat(year, '-', month, '-01')) = (select max(date(concat(year, '-', month, '-01'))) from membershipfee where playerid = m.playerid) and playerid = @PlayerId";
            command.Parameters.AddWithValue("@PlayerId", player.ID);
            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                membershipFee= new MembershipFee
                {
                    PlayerID = reader.GetInt32("PlayerID"),
                    Category = reader.GetString("Category"),
                    Month = reader.GetInt32("Month"),
                    Year = reader.GetInt32("Year"),
                    PaymentDate = reader.GetDateTime("PaymentDate"),
                    Value = reader.GetDecimal("Value")
                };
            }
            reader.Close();
            return membershipFee;
        }
        public List<Debt> GetAllDebts()
        {
            List<Debt> debts = new List<Debt>();
            List<MembershipFee> membershipFees = new MembershipFeeDAO().GetLastPaymentForAllPlayers();
            foreach (var memb in membershipFees)
            {
                Player player = new PlayerDAO().GetPlayerByID(memb.PlayerID);
                DateTime dateTime = new DateTime(memb.Year, memb.Month, 1).AddMonths(1);
                DateTime endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                StringBuilder date = new StringBuilder();
                while (dateTime <= endDate)
                {
                    date.Append(dateTime.ToString("MM.yyyy") + ", ");
                    dateTime = dateTime.AddMonths(1);

                }
                if (date.Length > 0)
                {
                    date.Remove(date.Length - 2, 2);
                    debts.Add(new Debt
                    {
                        Name = player.Name,
                        Surname = player.Surname,
                        Date = date.ToString()
                    });
                }
            }
            return debts;
        }
        public decimal GetDue(Player player)
        {
            decimal result= 0;
            MembershipFee last = GetLastPayemntForPlayer(player);
            MembershipFeeValueDAO membershipFeeValueDAO = new MembershipFeeValueDAO();
            DateTime dateTime = DateTime.Now;
            DateTime payDate = new DateTime(last.Year, last.Month, 1);
            payDate=payDate.AddMonths(1);
            while (payDate < dateTime)
            {
                result += membershipFeeValueDAO.GetValueForMonth(payDate);
                payDate = payDate.AddMonths(1);
            }
            return result;
        }
        public void AddNextPaymentForPlayer(Player player)
        {
            MembershipFee membership = GetLastPayemntForPlayer(player);
            DateTime newDate = new DateTime(membership.Year, membership.Month, 1);
            newDate = newDate.AddMonths(1);
            decimal value = new MembershipFeeValueDAO().GetValueForMonth(newDate);
            membership.PaymentDate = DateTime.Now;
            membership.Year = newDate.Year;
            membership.Value = value;
            membership.Month = newDate.Month;
            AddMembershipFee(membership);
        }
        public int GetNumberOfPayment(Player player)
        {
            int result = 0;
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select count(*) as numberOfPayment from membershipfee  where  playerid = @PlayerId";
            command.Parameters.AddWithValue("@PlayerId", player.ID);
            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                result = reader.GetInt32("numberOfPayment");
                reader.Close();
                return result;
            }
            return result;
        }
        public bool RemoveLastPaymentForPlayer(Player player)
        {
            if (GetNumberOfPayment(player) > 1)
            {
                MembershipFee lastPayment = GetLastPayemntForPlayer(player);
                RemoveMembershipFee(lastPayment);
                return true;
            }
            return false;
        }
    }
}
