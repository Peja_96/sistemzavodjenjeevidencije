﻿using FootApp.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FootApp.DAO
{
    class CoachDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<Coach> GetCoachs()
        {
            List<Coach> coaches = new List<Coach>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from coach natural inner join employee natural inner join person where isactive = 1";

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                coaches.Add(new Coach
                {
                    ID = reader.GetInt32("ID"),
                    Name = reader.GetString("Name"),
                    Surname = reader.GetString("Surname"),
                    DateOfBirth = reader.GetDateTime("DateOfBirth"),
                    Position = reader.GetString("Position"),
                    EmploymentRecordBook = reader.GetString("EmploymentRecordBook"),
                    CoachLicenceNumber = reader.GetString("CoachLicenceNumber")
                });
            }
            reader.Close();

            return coaches;
        }
        public void AddCoach(Coach coach)
        {
            new EmployeeDAO().AddEmployee(coach);
            DatabaseConnection db = DatabaseConnection.GetConnection();
            MySqlCommand command = db.CreateCommand();
            command.CommandText = "Insert into coach(ID,coachLicenceNumber) values(@ID,@coachLicenceNumber)";
            MySqlParameter personID = new MySqlParameter() { ParameterName = "@ID", Value = coach.ID, MySqlDbType = MySqlDbType.Int32 };
            MySqlParameter coachLicenceNumber = new MySqlParameter() { ParameterName = "@coachLicenceNumber", Value = coach.CoachLicenceNumber, MySqlDbType = MySqlDbType.VarChar };
            command.Parameters.AddRange(new object[] { personID,coachLicenceNumber });
            command.Prepare();
            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                log.Warn(ex);
            }
        }
        public List<Coach> GetCoachByName(string name)
        {
            List<Coach> coaches = new List<Coach>();
            DatabaseConnection db = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = db.CreateCommand();
            mySqlCommand.CommandText = "select * from coach natural join employee inner join person on id = personid where concat(Name, ' ', Surname) like @NameParam and IsActive=1 ;";
            mySqlCommand.Parameters.AddWithValue("@NameParam", new Regex("%" + name + "%"));
            MySqlDataReader reader = mySqlCommand.ExecuteReader();
            while (reader.Read())
            {
                coaches.Add(new Coach
                {
                    ID = reader.GetInt32("ID"),
                    Name = reader.GetString("Name"),
                    Surname = reader.GetString("Surname"),
                    DateOfBirth = reader.GetDateTime("DateOfBirth"),
                    Position = reader.GetString("Position"),
                    EmploymentRecordBook = reader.GetString("EmploymentRecordBook"),
                    CoachLicenceNumber = reader.GetString("CoachLicenceNumber")
                });
            }
            reader.Close();

            return coaches;
        }

        public Coach GetCoachById(int id)
        {
            Coach coach = null;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from coach natural inner join employee inner join person on id = personid where isactive = 1 and id = @ID";
            command.Parameters.AddWithValue("@ID", id);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                coach = new Coach()
                {
                    ID = reader.GetInt32("ID"),
                    Name = reader.GetString("Name"),
                    Surname = reader.GetString("Surname"),
                    DateOfBirth = reader.GetDateTime("DateOfBirth"),
                    Position = reader.GetString("Position"),
                    EmploymentRecordBook = reader.GetString("EmploymentRecordBook"),
                    CoachLicenceNumber = reader.GetString("CoachLicenceNumber")
                };
            }
            reader.Close();

            return coach;
        }
    }
}
