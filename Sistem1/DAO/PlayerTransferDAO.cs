﻿using FootApp.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FootApp.DAO
{
    public class PlayerTransferDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<PlayerTransfer> GetPlayerTransfers()
        {
            List<PlayerTransfer> playerTransfers = new List<PlayerTransfer>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from playertransfer inner join financialentry on id = financialentryid where isDeleted = 0";

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                playerTransfers.Add(new PlayerTransfer
                {
                    Amount = reader.GetDecimal("Amount"),
                    ClubID = Club.FootballClub.ID,
                    ID = reader.GetInt32("ID"),
                    Count = reader.GetInt32("Count"),
                    Description = reader.GetString("Description"),
                    OtherClub = reader.GetString("OtherClub"),
                    PlayerName = reader.GetString("PlayerName"),
                    PlayerSurname = reader.GetString("PlayerSurname"),
                    TransferType = reader.GetString("TransferType").Equals("Outgoing") ? TransferType.Outgoing : TransferType.Incomming,
                    Time = reader.GetDateTime("Time"),
                    Type = reader.GetString("Type").Equals("Expense") ? FinancialEntryType.Expense : FinancialEntryType.Income
                });
            }
            reader.Close();

            return playerTransfers;
        }

        public void AddPlayerTransfer(PlayerTransfer playerTransfer)
        {
            new FinancialEntryDAO().AddFinancialEntry(playerTransfer);
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "insert into playertransfer values (@FinancialEntryID, @PlayerName, @PlayerSurname, @OtherClub, @TransferType)";
            command.Parameters.AddWithValue("@FinancialEntryID", playerTransfer.ID);
            command.Parameters.AddWithValue("@PlayerName", playerTransfer.PlayerName);
            command.Parameters.AddWithValue("@PlayerSurname", playerTransfer.PlayerSurname);
            command.Parameters.AddWithValue("@OtherClub", playerTransfer.OtherClub);
            command.Parameters.AddWithValue("@TransferType", playerTransfer.TransferType.ToString());
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch(MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }

        public void RemovePlayerTransfer(PlayerTransfer playerTransfer)
        {
            new FinancialEntryDAO().DeleteFinancialEntry(playerTransfer);
        }

        public void ModifyPlayerTransfer(PlayerTransfer playerTransfer)
        {
            RemovePlayerTransfer(playerTransfer);
            AddPlayerTransfer(playerTransfer);
        }

        public List<PlayerTransfer> GetPlayerTransfersByPlayer(string name)
        {
            List<PlayerTransfer> playerTransfers = new List<PlayerTransfer>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from playertransfer inner join financialentry on id = financialentryid where isdeleted = 0 and concat(PlayerName, ' ', PlayerSurname) like @NameParam";
            command.Parameters.AddWithValue("@NameParam", new Regex("%" + name + "%"));
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                playerTransfers.Add(new PlayerTransfer
                {
                    Amount = reader.GetDecimal("Amount"),
                    ClubID = Club.FootballClub.ID,
                    ID = reader.GetInt32("ID"),
                    Count = reader.GetInt32("Count"),
                    Description = reader.GetString("Description"),
                    OtherClub = reader.GetString("OtherClub"),
                    PlayerName = reader.GetString("PlayerName"),
                    PlayerSurname = reader.GetString("PlayerSurname"),
                    TransferType = reader.GetString("TransferType").Equals("Outgoing") ? TransferType.Outgoing : TransferType.Incomming,
                    Time = reader.GetDateTime("Time"),
                    Type = reader.GetString("Type").Equals("Expense") ? FinancialEntryType.Expense : FinancialEntryType.Income
                });
            }
            reader.Close();

            return playerTransfers;
        }
    }
}
