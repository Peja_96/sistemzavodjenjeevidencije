﻿using FootApp.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DAO
{
    public class FinancialEntryDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<FinancialEntry> GetFinancialEntries()
        {
            List<FinancialEntry> financialEntries = new List<FinancialEntry>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from financialentry where isdeleted = 1";

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                financialEntries.Add(new FinancialEntry
                {
                    Amount = reader.GetDecimal("Amount"),
                    ClubID = reader.GetInt32("ClubID"),
                    Description = reader.GetString("Description"),
                    Count = reader.GetInt32("Count"),
                    ID = reader.GetInt32("ID"),
                    Time = reader.GetDateTime("Time"),
                    Type = reader.GetString("Type").Equals("Expense") ? FinancialEntryType.Expense : FinancialEntryType.Income
                });
            }
            reader.Close();

            return financialEntries;
        }

        public FinancialEntry GetFinancialEntryByID(int id)
        {
            FinancialEntry financialEntry = null;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from financialentry where id = @ID and isdeleted = 0";
            command.Parameters.AddWithValue("@ID", id);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                financialEntry = new FinancialEntry
                {
                    Amount = reader.GetDecimal("Amount"),
                    ClubID = reader.GetInt32("ClubID"),
                    Description = reader.GetString("Description"),
                    Count = reader.GetInt32("Count"),
                    ID = reader.GetInt32("ID"),
                    Time = reader.GetDateTime("Time"),
                    Type = reader.GetString("Type").Equals("Expense") ? FinancialEntryType.Expense : FinancialEntryType.Income
                };
            }
            reader.Close();

            return financialEntry;
        }
        public void AddFinancialEntry(FinancialEntry financialEntry)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "insert into financialentry values (null, @ClubID, @Description, @Time, @Count, @Amount, @Type, 0)";
            command.Parameters.AddWithValue("@ClubID", financialEntry.ClubID);
            command.Parameters.AddWithValue("@Description", financialEntry.Description);
            command.Parameters.AddWithValue("@Count", financialEntry.Count);
            command.Parameters.AddWithValue("@Time", financialEntry.Time);
            command.Parameters.AddWithValue("@Amount", financialEntry.Amount);
            command.Parameters.AddWithValue("@Type", financialEntry.Type.ToString());
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
                financialEntry.ID = (int)command.LastInsertedId;
            }
            catch(MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }

        /// <summary>
        /// Do not touch ID!
        /// </summary>
        /// <param name="newFinancialEntry">Already modified entry. </param>
        /// <param name="oldfinancialEntry">Old financial entry. You need to sent two different objects(use clone for newFinancialEntry, or just create new object)</param>
        public void UpdateFinancialEntry(FinancialEntry newFinancialEntry, FinancialEntry oldfinancialEntry)
        {
            BudgetDAO budgetDAO = new BudgetDAO();
            budgetDAO.UpdateCashFlowForEntry(oldfinancialEntry, new BudgetDAO().GetBudgetForYear(oldfinancialEntry.Time.Year));
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "update financialentry set Description = @Description, Time = @Time, Amount = @Amount, Count = @Count, " +
                "Type = @Type where id = @ID";
            budgetDAO.UpdateCashFlowForEntry(newFinancialEntry, new BudgetDAO().GetBudgetForYear(newFinancialEntry.Time.Year), false);
            command.Parameters.AddWithValue("@Description", newFinancialEntry.Description);
            command.Parameters.AddWithValue("@Time", newFinancialEntry.Time);
            command.Parameters.AddWithValue("@Amount", newFinancialEntry.Amount);
            command.Parameters.AddWithValue("@Count", newFinancialEntry.Count);
            command.Parameters.AddWithValue("@Type", newFinancialEntry.Type.ToString());
            command.Parameters.AddWithValue("@ID", newFinancialEntry.ID);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }

        public void DeleteFinancialEntry(FinancialEntry financialEntry)
        {
            new BudgetDAO().UpdateCashFlowForEntry(financialEntry, new BudgetDAO().GetBudgetForYear(financialEntry.Time.Year));
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "update financialentry set isdeleted = 1 where id = @ID";
            command.Parameters.AddWithValue("@ID", financialEntry.ID);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }
        /// <summary>
        /// This happanes at beginning of the month
        /// </summary>
        public void AddSalaryEntries()
        {
            List<PlayerContract> contracts = new PlayerContractDAO().GetContracts();
            contracts.ForEach(contract => {
                if (contract.StartDate <= DateTime.Now && contract.EndDate >= DateTime.Now)
                {
                    FinancialEntry entry = new FinancialEntry
                    {
                        ClubID = Club.FootballClub.ID,
                        Amount = contract.Salary,
                        Time = DateTime.Now,
                        Type = FinancialEntryType.Expense,
                        Description = "Player Salary - " + contract.Name + " " + contract.Surname,
                        ID = 0
                    };
                    AddFinancialEntry(entry);
                }
            });

            List<EmployeeContract> employeeContracts = new EmployeeContractDAO().GetContracts();
            employeeContracts.ForEach(contract => {
                if (contract.StartDate <= DateTime.Now && contract.EndDate >= DateTime.Now)
                {
                    FinancialEntry entry = new FinancialEntry
                    {
                        ClubID = Club.FootballClub.ID,
                        Amount = contract.Salary,
                        Time = DateTime.Now,
                        Type = FinancialEntryType.Expense,
                        Description = "Employee Salary - " + contract.Name + " " + contract.Surname,
                        ID = 0
                    };
                    AddFinancialEntry(entry);
                }
            });

            List<SponsorContract> sponsorContracts = new SponsorContractDAO().GetSponsorContracts();
            sponsorContracts.ForEach(contract => {
                if (contract.StartDate <= DateTime.Now && contract.EndDate >= DateTime.Now)
                {
                    FinancialEntry entry = new FinancialEntry
                    {
                        ClubID = Club.FootballClub.ID,
                        Amount = contract.Investment,
                        Time = DateTime.Now,
                        Type = FinancialEntryType.Income,
                        Description = "Sponsor Investment - " + contract.Name,
                        ID = 0
                    };
                    AddFinancialEntry(entry);
                }
            });
        }

        public List<FinancialEntry> GetFinancialEntriesForPeriod(DateTime start, DateTime end)
        {
            List<FinancialEntry> financialEntries = new List<FinancialEntry>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from financialentry where @Start <= time and @End >= time and isdeleted = 0";
            command.Parameters.AddWithValue("@Start", start);
            command.Parameters.AddWithValue("@End", end);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                financialEntries.Add(new FinancialEntry
                {
                    Amount = reader.GetDecimal("Amount"),
                    ClubID = reader.GetInt32("ClubID"),
                    Description = reader.GetString("Description"),
                    Count = reader.GetInt32("Count"),
                    ID = reader.GetInt32("ID"),
                    Time = reader.GetDateTime("Time"),
                    Type = reader.GetString("Type").Equals("Expense") ? FinancialEntryType.Expense : FinancialEntryType.Income
                });
            }
            reader.Close();

            return financialEntries;
        }

        public List<FinancialEntry> GetFinancialEntriesForYear(int year)
        {
            return GetFinancialEntriesForPeriod(new DateTime(year, 1, 1), new DateTime(year, 12, 31));
        }

        public List<FinancialEntry> GetFinancialEntriesForCurrentMonth()
        {
            int lastDay = 31;
            int curMonth = DateTime.Now.Month;
            if (curMonth == 4 || curMonth == 6 || curMonth == 9 || curMonth == 11)
                lastDay = 30;
            else if(curMonth == 2)
            {
                if (DateTime.IsLeapYear(DateTime.Now.Year))
                    lastDay = 29;
                else
                    lastDay = 28;
            }
            return GetFinancialEntriesForPeriod(new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), new DateTime(DateTime.Now.Year, DateTime.Now.Month, lastDay));
        }

        public List<FinancialEntry> GetFinancialEntriesForCurrentYear()
        {
            return GetFinancialEntriesForYear(DateTime.Now.Year);
        }
    }
}
