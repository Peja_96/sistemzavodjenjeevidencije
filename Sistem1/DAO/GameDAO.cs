﻿using FootApp.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DAO
{
    class GameDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<Game> GetAllGames()
        {
            List<Game> games = new List<Game>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from game";
            MySqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                games.Add(new Game()
                {
                    CompetitionID = reader.GetInt32("CompetitionID"),
                    ClubBallPossession = reader.GetInt16("ClubBallPossession"),
                    Opponent = reader.GetString("Opponent"),
                    OpponentBallPossession = reader.GetInt16("OpponentBallPossession"),
                    ClubFreeKicks = reader.GetInt16("ClubFreeKicks"),
                    ClubRedCards = reader.GetInt16("ClubRedCards"),
                    ClubResult = reader.GetInt16("ClubResult"),
                    ClubYellowCards = reader.GetInt16("ClubYellowCards"),
                    OpponentFreeKicks = reader.GetInt16("OpponentFreeKicks"),
                    OpponentRedCards = reader.GetInt16("OpponentRedCards"),
                    OpponentResult = reader.GetInt16("OpponentResult"),
                    OpponentYellowCards = reader.GetInt16("OpponentYellowCards"),
                    StartTime = reader.GetDateTime("StartTime"),
                    TotalPenalities = reader.GetInt16("TotalPenalities"),
                    ClubShots = reader.GetInt16("ClubShots"),
                    ClubShotsOnGoal = reader.GetInt16("ClubShotsOnGoal"),
                    OpponentShots = reader.GetInt16("OpponentShots"),
                    OpponentShotsOnGoal = reader.GetInt16("OpponentShotsOnGoal"),
                    Round = reader.GetString("Round"),
                    ClubCorners = reader.GetInt16("ClubCorners"),
                    TicketsSold = reader.GetInt32("TicketsSold"),
                    OpponentCorners = reader.GetInt16("OpponentCorners"),
                    IsHost = reader.GetBoolean("IsHost")
                });
            }
            reader.Close();

            return games;
        }

        public List<Game> GetGamesByCompetition(Competition competition)
        {
            List<Game> games = new List<Game>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from game where competitionid = @CompetitionID";
            command.Parameters.AddWithValue("@CompetitionID", competition.Id);
            command.Prepare();
            MySqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                games.Add(new Game()
                {
                    CompetitionID = competition.Id,
                    ClubBallPossession = reader.GetInt16("ClubBallPossession"),
                    Opponent = reader.GetString("Opponent"),
                    OpponentBallPossession = reader.GetInt16("OpponentBallPossession"),
                    ClubFreeKicks = reader.GetInt16("ClubFreeKicks"),
                    ClubRedCards = reader.GetInt16("ClubRedCards"),
                    ClubResult = reader.GetInt16("ClubResult"),
                    ClubYellowCards = reader.GetInt16("ClubYellowCards"),
                    OpponentFreeKicks = reader.GetInt16("OpponentFreeKicks"),
                    OpponentRedCards = reader.GetInt16("OpponentRedCards"),
                    OpponentResult = reader.GetInt16("OpponentResult"),
                    OpponentYellowCards = reader.GetInt16("OpponentYellowCards"),
                    StartTime = reader.GetDateTime("StartTime"),
                    TotalPenalities = reader.GetInt16("TotalPenalities"),
                    ClubShots = reader.GetInt16("ClubShots"),
                    ClubShotsOnGoal = reader.GetInt16("ClubShotsOnGoal"),
                    OpponentShots = reader.GetInt16("OpponentShots"),
                    OpponentShotsOnGoal = reader.GetInt16("OpponentShotsOnGoal"),
                    Round = reader.GetString("Round"),
                    IsHost = reader.GetBoolean("IsHost"),
                    TicketsSold = reader.GetInt32("TicketsSold"),
                    ClubCorners = reader.GetInt16("ClubCorners"),
                    OpponentCorners = reader.GetInt16("OpponentCorners")
                });
            }
            reader.Close();

            return games;
        }

        public void AddGame(Game game)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "insert into game values  (@CompetitionID, @StartTime, @Round, @Opponent, @ClubResult, @OpponentResult, @ClubYellowCards," +
                "@OpponentYellowCards, @ClubRedCards, @OpponentRedCards, @ClubFreeKicks, @OpponentFreeKicks, @ClubBallPossession, @OpponentBallPossession, @TotalPenalities, " +
                "@ClubShots, @OpponentShots, @ClubShotsOnGoal, @OpponentShotsOnGoal, @IsHost, @ClubCorners, @OpponentCorners, @TicketsSold)";
            command.Parameters.AddWithValue("@Opponent", game.Opponent);
            command.Parameters.AddWithValue("@CompetitionID", game.CompetitionID);
            command.Parameters.AddWithValue("@StartTime", game.StartTime);
            command.Parameters.AddWithValue("@ClubResult", game.ClubResult);
            command.Parameters.AddWithValue("@OpponentResult", game.OpponentResult);
            command.Parameters.AddWithValue("@ClubYellowCards", game.ClubYellowCards);
            command.Parameters.AddWithValue("@OpponentYellowCards", game.OpponentYellowCards);
            command.Parameters.AddWithValue("@ClubRedCards", game.ClubRedCards);
            command.Parameters.AddWithValue("@OpponentRedCards", game.OpponentRedCards);
            command.Parameters.AddWithValue("@ClubFreeKicks", game.ClubFreeKicks);
            command.Parameters.AddWithValue("@OpponentFreeKicks", game.OpponentFreeKicks);
            command.Parameters.AddWithValue("@ClubBallPossession", game.ClubBallPossession);
            command.Parameters.AddWithValue("@OpponentBallPossession", game.OpponentBallPossession);
            command.Parameters.AddWithValue("@TotalPenalities", game.TotalPenalities);
            command.Parameters.AddWithValue("@ClubShots", game.ClubShots);
            command.Parameters.AddWithValue("@OpponentShots", game.OpponentShots);
            command.Parameters.AddWithValue("@ClubShotsOnGoal", game.ClubShotsOnGoal);
            command.Parameters.AddWithValue("@OpponentShotsOnGoal", game.OpponentShotsOnGoal);
            command.Parameters.AddWithValue("@Round", game.Round);
            command.Parameters.AddWithValue("@IsHost", game.IsHost);
            command.Parameters.AddWithValue("@ClubCorners", game.ClubCorners);
            command.Parameters.AddWithValue("@OpponentCorners", game.OpponentCorners);
            command.Parameters.AddWithValue("@TicketsSold", game.TicketsSold);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch(MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }
        public void ModifyGame(Game oldGame,Game newGame)
        {
            DeleteGame(oldGame);
            AddGame(newGame);
        }

        public void DeleteGame(Game game)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "delete from game where StartTime = @StartTime and CompetitionID = @CompetitionID";
            command.Parameters.AddWithValue("@CompetitionID", game.CompetitionID);
            command.Parameters.AddWithValue("@StartTime", game.StartTime);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }

        public Game GetSpecificGame(Team team, DateTime startTime, Competition competition)
        {
            Game game = null;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from game where competitionid = @CompetitionID and starttime = @StartTime";
            command.Parameters.AddWithValue("@CompetitionID", competition.Id);
            command.Parameters.AddWithValue("@StartTime", startTime);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                game = new Game()
                {
                    CompetitionID = competition.Id,
                    ClubBallPossession = reader.GetInt16("ClubBallPossession"),
                    Opponent = reader.GetString("Opponent"),
                    OpponentBallPossession = reader.GetInt16("OpponentBallPossession"),
                    ClubFreeKicks = reader.GetInt16("ClubFreeKicks"),
                    ClubRedCards = reader.GetInt16("ClubRedCards"),
                    ClubResult = reader.GetInt16("ClubResult"),
                    ClubYellowCards = reader.GetInt16("ClubYellowCards"),
                    OpponentFreeKicks = reader.GetInt16("OpponentFreeKicks"),
                    OpponentRedCards = reader.GetInt16("OpponentRedCards"),
                    OpponentResult = reader.GetInt16("OpponentResult"),
                    OpponentYellowCards = reader.GetInt16("OpponentYellowCards"),
                    StartTime = reader.GetDateTime("StartTime"),
                    TotalPenalities = reader.GetInt16("TotalPenalities"),
                    ClubShots = reader.GetInt16("ClubShots"),
                    ClubShotsOnGoal = reader.GetInt16("ClubShotsOnGoal"),
                    OpponentShots = reader.GetInt16("OpponentShots"),
                    OpponentShotsOnGoal = reader.GetInt16("OpponentShotsOnGoal"),
                    Round = reader.GetString("Round"),
                    IsHost = reader.GetBoolean("IsHost"),
                    TicketsSold = reader.GetInt32("TicketsSold"),
                    ClubCorners = reader.GetInt16("ClubCorners"),
                    OpponentCorners = reader.GetInt16("OpponentCorners")
                };
            }
            reader.Close();

            return game;
        }

        public List<Game> GetHostGames()
        {
            List<Game> games = new List<Game>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from game where ishost = 1";
            MySqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                games.Add(new Game()
                {
                    CompetitionID = reader.GetInt32("CompetitionID"),
                    ClubBallPossession = reader.GetInt16("ClubBallPossession"),
                    Opponent = reader.GetString("Opponent"),
                    OpponentBallPossession = reader.GetInt16("OpponentBallPossession"),
                    ClubFreeKicks = reader.GetInt16("ClubFreeKicks"),
                    ClubRedCards = reader.GetInt16("ClubRedCards"),
                    ClubResult = reader.GetInt16("ClubResult"),
                    ClubYellowCards = reader.GetInt16("ClubYellowCards"),
                    OpponentFreeKicks = reader.GetInt16("OpponentFreeKicks"),
                    OpponentRedCards = reader.GetInt16("OpponentRedCards"),
                    OpponentResult = reader.GetInt16("OpponentResult"),
                    OpponentYellowCards = reader.GetInt16("OpponentYellowCards"),
                    StartTime = reader.GetDateTime("StartTime"),
                    TotalPenalities = reader.GetInt16("TotalPenalities"),
                    ClubShots = reader.GetInt16("ClubShots"),
                    ClubShotsOnGoal = reader.GetInt16("ClubShotsOnGoal"),
                    OpponentShots = reader.GetInt16("OpponentShots"),
                    OpponentShotsOnGoal = reader.GetInt16("OpponentShotsOnGoal"),
                    Round = reader.GetString("Round"),
                    IsHost = reader.GetBoolean("IsHost"),
                    TicketsSold = reader.GetInt32("TicketsSold"),
                    ClubCorners = reader.GetInt16("ClubCorners"),
                    OpponentCorners = reader.GetInt16("OpponencCorners")
                });
            }
            reader.Close();

            return games;
        }

        public List<Game> GetGuestGames()
        {
            List<Game> games = new List<Game>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from game where ishost = 0";
            MySqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                games.Add(new Game()
                {
                    CompetitionID = reader.GetInt32("CompetitionID"),
                    ClubBallPossession = reader.GetInt16("ClubBallPossession"),
                    Opponent = reader.GetString("Opponent"),
                    OpponentBallPossession = reader.GetInt16("OpponentBallPossession"),
                    ClubFreeKicks = reader.GetInt16("ClubFreeKicks"),
                    ClubRedCards = reader.GetInt16("ClubRedCards"),
                    ClubResult = reader.GetInt16("ClubResult"),
                    ClubYellowCards = reader.GetInt16("ClubYellowCards"),
                    OpponentFreeKicks = reader.GetInt16("OpponentFreeKicks"),
                    OpponentRedCards = reader.GetInt16("OpponentRedCards"),
                    OpponentResult = reader.GetInt16("OpponentResult"),
                    OpponentYellowCards = reader.GetInt16("OpponentYellowCards"),
                    StartTime = reader.GetDateTime("StartTime"),
                    TotalPenalities = reader.GetInt16("TotalPenalities"),
                    ClubShots = reader.GetInt16("ClubShots"),
                    ClubShotsOnGoal = reader.GetInt16("ClubShotsOnGoal"),
                    OpponentShots = reader.GetInt16("OpponentShots"),
                    OpponentShotsOnGoal = reader.GetInt16("OpponentShotsOnGoal"),
                    Round = reader.GetString("Round"),
                    IsHost = reader.GetBoolean("IsHost"),
                    TicketsSold = reader.GetInt32("TicketsSold"),
                    ClubCorners = reader.GetInt16("ClubCorners"),
                    OpponentCorners = reader.GetInt16("OpponentCorners")
                });
            }
            reader.Close();

            return games;
        }
        public List<Game> getTenGames(int offset)
        {
            List<Game> games = new List<Game>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "SELECT * FROM game order by StartTime desc limit 10 offset @offset";
            command.Parameters.AddWithValue("@offset", offset);
            MySqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                games.Add(new Game()
                {
                    CompetitionID = reader.GetInt32("CompetitionID"),
                    ClubBallPossession = reader.GetInt16("ClubBallPossession"),
                    Opponent = reader.GetString("Opponent"),
                    OpponentBallPossession = reader.GetInt16("OpponentBallPossession"),
                    ClubFreeKicks = reader.GetInt16("ClubFreeKicks"),
                    ClubRedCards = reader.GetInt16("ClubRedCards"),
                    ClubResult = reader.GetInt16("ClubResult"),
                    ClubYellowCards = reader.GetInt16("ClubYellowCards"),
                    OpponentFreeKicks = reader.GetInt16("OpponentFreeKicks"),
                    OpponentRedCards = reader.GetInt16("OpponentRedCards"),
                    OpponentResult = reader.GetInt16("OpponentResult"),
                    OpponentYellowCards = reader.GetInt16("OpponentYellowCards"),
                    StartTime = reader.GetDateTime("StartTime"),
                    TotalPenalities = reader.GetInt16("TotalPenalities"),
                    ClubShots = reader.GetInt16("ClubShots"),
                    ClubShotsOnGoal = reader.GetInt16("ClubShotsOnGoal"),
                    OpponentShots = reader.GetInt16("OpponentShots"),
                    OpponentShotsOnGoal = reader.GetInt16("OpponentShotsOnGoal"),
                    Round = reader.GetString("Round"),
                    IsHost = reader.GetBoolean("IsHost"),
                    TicketsSold = reader.GetInt32("TicketsSold"),
                    ClubCorners = reader.GetInt16("ClubCorners"),
                    OpponentCorners = reader.GetInt16("OpponentCorners")
                });
            }
            reader.Close();
            return games;
        }
        public List<Game> getTenGamesByDate(DateTime date,int offset)
        {
            List<Game> games = new List<Game>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "SELECT * FROM game where date(StartTime) =@Date limit 10 offset @Offset";
            command.Parameters.AddWithValue("@Offset", offset);
            command.Parameters.AddWithValue("@Date", date);
            MySqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                games.Add(new Game()
                {
                    CompetitionID = reader.GetInt32("CompetitionID"),
                    ClubBallPossession = reader.GetInt16("ClubBallPossession"),
                    Opponent = reader.GetString("Opponent"),
                    OpponentBallPossession = reader.GetInt16("OpponentBallPossession"),
                    ClubFreeKicks = reader.GetInt16("ClubFreeKicks"),
                    ClubRedCards = reader.GetInt16("ClubRedCards"),
                    ClubResult = reader.GetInt16("ClubResult"),
                    ClubYellowCards = reader.GetInt16("ClubYellowCards"),
                    OpponentFreeKicks = reader.GetInt16("OpponentFreeKicks"),
                    OpponentRedCards = reader.GetInt16("OpponentRedCards"),
                    OpponentResult = reader.GetInt16("OpponentResult"),
                    OpponentYellowCards = reader.GetInt16("OpponentYellowCards"),
                    StartTime = reader.GetDateTime("StartTime"),
                    TotalPenalities = reader.GetInt16("TotalPenalities"),
                    ClubShots = reader.GetInt16("ClubShots"),
                    ClubShotsOnGoal = reader.GetInt16("ClubShotsOnGoal"),
                    OpponentShots = reader.GetInt16("OpponentShots"),
                    OpponentShotsOnGoal = reader.GetInt16("OpponentShotsOnGoal"),
                    Round = reader.GetString("Round"),
                    IsHost = reader.GetBoolean("IsHost"),
                    TicketsSold = reader.GetInt32("TicketsSold"),
                    ClubCorners = reader.GetInt16("ClubCorners"),
                    OpponentCorners = reader.GetInt16("OpponentCorners")
                });
            }
            reader.Close();

            return games;
        }
        public List<Game> getThreeGames(int offset)
        {
            List<Game> games = new List<Game>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "SELECT * FROM game order by StartTime desc limit 3 offset @offset";
            command.Parameters.AddWithValue("@offset", offset);
            MySqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                games.Add(new Game()
                {
                    CompetitionID = reader.GetInt32("CompetitionID"),
                    ClubBallPossession = reader.GetInt16("ClubBallPossession"),
                    Opponent = reader.GetString("Opponent"),
                    OpponentBallPossession = reader.GetInt16("OpponentBallPossession"),
                    ClubFreeKicks = reader.GetInt16("ClubFreeKicks"),
                    ClubRedCards = reader.GetInt16("ClubRedCards"),
                    ClubResult = reader.GetInt16("ClubResult"),
                    ClubYellowCards = reader.GetInt16("ClubYellowCards"),
                    OpponentFreeKicks = reader.GetInt16("OpponentFreeKicks"),
                    OpponentRedCards = reader.GetInt16("OpponentRedCards"),
                    OpponentResult = reader.GetInt16("OpponentResult"),
                    OpponentYellowCards = reader.GetInt16("OpponentYellowCards"),
                    StartTime = reader.GetDateTime("StartTime"),
                    TotalPenalities = reader.GetInt16("TotalPenalities"),
                    ClubShots = reader.GetInt16("ClubShots"),
                    ClubShotsOnGoal = reader.GetInt16("ClubShotsOnGoal"),
                    OpponentShots = reader.GetInt16("OpponentShots"),
                    OpponentShotsOnGoal = reader.GetInt16("OpponentShotsOnGoal"),
                    Round = reader.GetString("Round"),
                    IsHost = reader.GetBoolean("IsHost"),
                    TicketsSold = reader.GetInt32("TicketsSold"),
                    ClubCorners = reader.GetInt16("ClubCorners"),
                    OpponentCorners = reader.GetInt16("OpponentCorners")
                });
            }
            reader.Close();

            return games;
        }

        public bool CompetitionHasGame(Competition competition)
        {
            bool hasGames = false;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from game where competitionid = @ID limit 1";
            command.Parameters.AddWithValue("@ID", competition.Id);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
                hasGames = true;
            reader.Close();

            return hasGames;
        }
    }
}
