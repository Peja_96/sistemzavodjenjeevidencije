﻿using FootApp.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FootApp.DAO
{
    class EmployeeContractDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<EmployeeContract> GetContracts()
        {
            List<EmployeeContract> contracts = new List<EmployeeContract>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from employee_contract";

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                contracts.Add(new EmployeeContract()
                {
                    EmployeeID = reader.GetInt32("EmployeeID"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Salary = reader.GetDecimal("Salary"),
                    Name = reader.GetString("Name"),
                    Surname = reader.GetString("Surname")
                });
            }
            reader.Close();

            return contracts;
        }

        public decimal GetSalaryForEmployee(Employee employee)
        {
            decimal salary = 0;
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select salary from employeecontract where IdEmployee = @Id";
            MySqlParameter id = new MySqlParameter("@Id", MySqlDbType.Int32) { Value = employee.ID };
            command.Parameters.Add(id);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
                salary = reader.GetDecimal("salary");
            reader.Close();

            return salary;
        }
        public void AddEmployeeContract(EmployeeContract employeeContract)
        {
            DatabaseConnection db = DatabaseConnection.GetConnection();
            MySqlCommand command = db.CreateCommand();
            command.CommandText = "Insert into employeecontract(EmployeeID,StartDate,EndDate,Salary) values(@idemplyee,@startdate,@enddate,@salary)";
            MySqlParameter idemplyee = new MySqlParameter() { ParameterName = "@idemplyee", Value = employeeContract.EmployeeID, MySqlDbType = MySqlDbType.Int32 };
            MySqlParameter startDate = new MySqlParameter() { ParameterName = "@startdate", Value = employeeContract.StartDate, MySqlDbType = MySqlDbType.Date };
            MySqlParameter endDate = new MySqlParameter() { ParameterName = "@enddate", Value = employeeContract.EndDate, MySqlDbType = MySqlDbType.Date };
            MySqlParameter salary = new MySqlParameter() { ParameterName = "@salary", Value = employeeContract.Salary, MySqlDbType = MySqlDbType.Double };
            command.Parameters.AddRange(new object[] { idemplyee, startDate, endDate, salary });
            command.Prepare();
            command.ExecuteNonQuery();
        }

        public EmployeeContract GetEmployeeContract(Employee employee, DateTime startDate, DateTime endDate)
        {
            EmployeeContract employeeContract = null;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from employeecontract where employeeid = @EmployeeID and startdate = @StartDate and enddate = @EndDate";
            command.Parameters.AddWithValue("@EmployeeID", employee.ID);
            command.Parameters.AddWithValue("@StartDate", startDate);
            command.Parameters.AddWithValue("@EndDate", endDate);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                employeeContract = new EmployeeContract()
                {
                    EndDate = endDate,
                    StartDate = startDate,
                    EmployeeID = employee.ID,
                    Salary = reader.GetDecimal("Salary")
                };
            }
            reader.Close();

            return employeeContract;
        }
        public List<EmployeeContract> GetTenContracts(int offset)
        {
            List<EmployeeContract> contracts = new List<EmployeeContract>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from employee_contract limit 10 offset @Offset";
            command.Parameters.AddWithValue("@Offset", offset);
            command.Prepare();
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                contracts.Add(new EmployeeContract()
                {
                    EmployeeID = reader.GetInt32("EmployeeID"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Salary = reader.GetDecimal("Salary"),
                    Name = reader.GetString("Name"),
                    Surname = reader.GetString("Surname")
                });
            }
            reader.Close();

            return contracts;
        }

        public List<EmployeeContract> GetEmployeeContractsByPlayerName(string name)
        {
            List<EmployeeContract> employeeContracts = new List<EmployeeContract>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select employeecontract.*, name, surname from employeecontract inner join person on id = employeeid where concat(name, ' ', surname) like @NameParam";
            command.Parameters.AddWithValue("@NameParam", new Regex("%" + name + "%"));
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                employeeContracts.Add(new EmployeeContract()
                {
                    EmployeeID = reader.GetInt32("EmployeeID"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Salary = reader.GetDecimal("Salary"),
                    Name = reader.GetString("Name"),
                    Surname = reader.GetString("Surname")
                });
            }
            reader.Close();

            return employeeContracts;
        }

        public void RemoveEmployeeContract(EmployeeContract employeeContract)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "delete from employeecontract where startdate = @StartDate and enddate = @EndDate and employeeid = @EmployeeID";
            command.Parameters.AddWithValue("@StartDate", employeeContract.StartDate);
            command.Parameters.AddWithValue("@EndDate", employeeContract.EndDate);
            command.Parameters.AddWithValue("@EmployeeID", employeeContract.EmployeeID);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch(MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }
    }
}
