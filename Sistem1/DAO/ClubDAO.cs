﻿using FootApp.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DAO
{
    class ClubDAO
    {
        private static readonly log4net.ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void InsertClubInfo()
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "insert into club values(null, @Name, @StadionName, @StadionSeats, @Address)";
            command.Parameters.AddWithValue("@Name", Club.FootballClub.Name);
            command.Parameters.AddWithValue("@StadionName", Club.FootballClub.StadionName);
            command.Parameters.AddWithValue("@StadionSeats", Club.FootballClub.StadionSeats);
            command.Parameters.AddWithValue("@Address", Club.FootballClub.Address);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
                Club.FootballClub.ID = (int)command.LastInsertedId;
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }
        public Club ReadClubInfo()
        {
            Club club = null;
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from club";
            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                club = new Club()
                {
                    ID = reader.GetInt32("ID"),
                    Name = reader.GetString("Name"),
                    StadionName = reader.GetString("StadionName"),
                    StadionSeats = reader.GetInt32("StadionSeats"),
                    Address = reader.GetString("Address"),
                };
            }
            reader.Close();

            return club;
        }

        public void ModifyClubInfo()
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "update club set name = @Name, stadionname = @StadionName, stadionseats = @StadionSeats, address = @Address where id = @ID";
            command.Parameters.AddWithValue("@Name", Club.FootballClub.Name);
            command.Parameters.AddWithValue("@StadionName", Club.FootballClub.StadionName);
            command.Parameters.AddWithValue("@StadionSeats", Club.FootballClub.StadionSeats);
            command.Parameters.AddWithValue("@Address", Club.FootballClub.Address);
            command.Parameters.AddWithValue("@ID", Club.FootballClub.ID);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch(MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }
    }
}
