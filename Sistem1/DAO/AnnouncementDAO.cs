﻿using FootApp.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DAO
{
    class AnnouncementDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<Announcement> GetAnnouncements(string personType = "all")
        {
            List<Announcement> announcements = new List<Announcement>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            if ("all".Equals(personType))
                command.CommandText = "select * from announcement natural inner join announcementtable order by announcement.Release desc";
            else
            {
                command.CommandText = "select * from announcement natural inner join announcementtable where PersonType = @PersonType order by announcement.Release desc";
                command.Parameters.AddWithValue("@PersonType", personType);
                command.Prepare();
            }
            MySqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                announcements.Add(new Announcement()
                {
                    AnnouncementTableID = reader.GetInt32("ID"),
                    PersonType = reader.GetString("PersonType"),
                    Position = reader.GetString("Position"),
                    Release = reader.GetDateTime("Release"),
                    Text = reader.GetString("Text"),
                    Title = reader.GetString("Title")
                });
            }
            reader.Close();

            return announcements;
        }

        public void AddAnnouncement(Announcement announcement)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "insert into announcement values (@Text, @Release, @PersonType, @Title, @Position, @ID)";
            command.Parameters.AddWithValue("@ID", announcement.AnnouncementTableID);
            command.Parameters.AddWithValue("@Text", announcement.Text);
            command.Parameters.AddWithValue("@Release", announcement.Release);
            command.Parameters.AddWithValue("@PersonType", announcement.PersonType);
            command.Parameters.AddWithValue("@Title", announcement.Title);
            command.Parameters.AddWithValue("@Position", announcement.Position);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch(MySqlException myesqlex)
            {
                log.Warn(myesqlex);
            }
        }

        public void ModifyAnnouncement(Announcement announcement)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "update announcement set Text = @Text, PersonType = @PersonType, Title = @Title, Position = @Position where Release = @Release and ID = @ID";
            command.Parameters.AddWithValue("@ID", announcement.AnnouncementTableID);
            command.Parameters.AddWithValue("@Text", announcement.Text);
            command.Parameters.AddWithValue("@Release", announcement.Release);
            command.Parameters.AddWithValue("@PersonType", announcement.PersonType);
            command.Parameters.AddWithValue("@Title", announcement.Title);
            command.Parameters.AddWithValue("@Position", announcement.Position);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException myesqlex)
            {
                log.Warn(myesqlex);
            }
        }

        public void DeleteAnnouncement(Announcement announcement)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "delete from announcement where Release = @Release and ID = @ID";
            command.Parameters.AddWithValue("@ID", announcement.AnnouncementTableID);
            command.Parameters.AddWithValue("@Release", announcement.Release);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException myesqlex)
            {
                log.Warn(myesqlex);
            }
        }

        public Announcement GetAnnouncement(int announcementTableID, DateTime release)
        {
            Announcement announcement = null;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from announcement where announcementTableID = @AnnouncementTableID and release = @Release";
            command.Parameters.AddWithValue("@AnnouncemenetTableID", announcementTableID);
            command.Parameters.AddWithValue("@Release", release);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                announcement = new Announcement()
                {
                    AnnouncementTableID = reader.GetInt32("ID"),
                    PersonType = reader.GetString("PersonType"),
                    Position = reader.GetString("Position"),
                    Release = reader.GetDateTime("Release"),
                    Text = reader.GetString("Text"),
                    Title = reader.GetString("Title")
                };
            }
            reader.Close();

            return announcement;
        }
        public List<Announcement> GetAnnouncementsTenByDate( DateTime release,int offset)
        {
            List<Announcement> announcements = new List<Announcement>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from announcement where  date(`release`) = @Release order by announcement.Release desc limit 10 offset @Offset ";
            command.Parameters.AddWithValue("@Release", release);
            command.Parameters.AddWithValue("@Offset", offset);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                announcements.Add( new Announcement
                {
                    AnnouncementTableID = reader.GetInt32("AnnouncementTableID"),
                    PersonType = reader.GetString("PersonType"),
                    Position = reader.GetString("Position"),
                    Release = reader.GetDateTime("Release"),
                    Text = reader.GetString("Text"),
                    Title = reader.GetString("Title")
                });
            }
            reader.Close();

            return announcements;
        }
        public List<Announcement> GetAnnouncementsTen( int offset)
        {
            List<Announcement> announcements = new List<Announcement>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from announcement order by announcement.Release desc limit 10 offset @Offset";
            command.Parameters.AddWithValue("@Offset", offset);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                announcements.Add(new Announcement
                {
                    AnnouncementTableID = reader.GetInt32("AnnouncementTableID"),
                    PersonType = reader.GetString("PersonType"),
                    Position = reader.GetString("Position"),
                    Release = reader.GetDateTime("Release"),
                    Text = reader.GetString("Text"),
                    Title = reader.GetString("Title")
                });
            }
            reader.Close();

            return announcements;
        }
    }
}
