﻿using FootApp.DTO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DAO
{
    class AnnouncementTableDAO
    {

        //Kasnije izmijeniti za više oglasnih tabli!
        public AnnouncementTable GetAnnouncementTable()
        {
            AnnouncementTable announcementTable = null;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from announcementTable";
            MySqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {
                announcementTable = new AnnouncementTable()
                {
                    ID = reader.GetInt32("ID"),
                    ClubID = Club.FootballClub.ID
                };
            }
            reader.Close();

            return announcementTable;
        }
    }
}
