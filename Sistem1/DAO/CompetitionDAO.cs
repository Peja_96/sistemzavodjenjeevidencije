﻿using FootApp.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DAO
{
    class CompetitionDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<Competition> GetCompetitions()
        {
            List<Competition> competitions = new List<Competition>();
            competitions.AddRange(new CupDAO().GetCups());
            competitions.AddRange(new LeagueDAO().GetLeagues());
            return competitions;
        }

        public void AddCompetition(Competition competition)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "insert into competition values (null, @Title, @Description, @SeasonID, @StartDate, @EndDate, 0, 0, 0, @ClubID, @Category)";
            command.Parameters.AddWithValue("@Title", competition.Title);
            command.Parameters.AddWithValue("@Description", competition.Description);
            command.Parameters.AddWithValue("@SeasonID", competition.SeasonID);
            command.Parameters.AddWithValue("@StartDate", competition.StartDate);
            command.Parameters.AddWithValue("@EndDate", competition.EndDate);
            command.Parameters.AddWithValue("@ClubID", competition.ClubID);
            command.Parameters.AddWithValue("@Category", competition.Category);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
                competition.Id = (int)command.LastInsertedId;
            }
            catch(MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }

        public Competition GetCompetitionByID(int id)
        {
            Competition cup = new CupDAO().GetCupByID(id);
            return cup ?? new LeagueDAO().GetLeagueByID(id);
        }

        public List<Competition> GetCompetitionsForCurrentSeason()
        {
            return GetCompetitionsBySeason(new SeasonDAO().GetCurrentSeason());

        }
       
        public List<Competition> GetCompetitionsBySeason(Season season)
        {
            List<Competition> competitions = new List<Competition>();
            competitions.AddRange(new CupDAO().GetCupsBySeason(season));
            competitions.AddRange(new LeagueDAO().GetLeaguesBySeason(season));
            return competitions;
        }

        public List<Competition> GetCompetitionsBySeasonAndTeam(Season season, Team team)
        {
            List<Competition> competitions = new List<Competition>();
            competitions.AddRange(new CupDAO().GetCupsBySeasonAndTeam(season, team));
            competitions.AddRange(new LeagueDAO().GetLeaguesBySeasonAndTeam(season, team));
            return competitions;
        }

        public void DeleteCompetition(Competition competition)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "delete from competition where id = @ID";
            command.Parameters.AddWithValue("@ID", competition.Id);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch(MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }

        public void ModifyCompetitionInfo(Competition competition)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "update competition set title = @Title, description = @Description, StartDate = @StartDate, EndDate = @EndDate" +
                " where id = @ID";
            command.Parameters.AddWithValue("@Title", competition.Title);
            command.Parameters.AddWithValue("@Description", competition.Description);
            command.Parameters.AddWithValue("@StartDate", competition.StartDate);
            command.Parameters.AddWithValue("@EndDate", competition.EndDate);
            command.Parameters.AddWithValue("@ID", competition.Id);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch(MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }

        public List<Competition> GetCompetitionsByTitle(string title)
        {
            List<Competition> competitions = new List<Competition>();
            competitions.AddRange(new CupDAO().GetCupsByTitle(title));
            competitions.AddRange(new LeagueDAO().GetLeaguesByTitle(title));
            return competitions;
        }
    }
}
