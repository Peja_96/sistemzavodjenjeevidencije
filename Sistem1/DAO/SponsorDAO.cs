﻿using FootApp.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FootApp.DAO
{
    public class SponsorDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<Sponsor> GetSponsors()
        {
            List<Sponsor> sponsors = new List<Sponsor>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from sponsor";

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                sponsors.Add(new Sponsor
                {
                    ClubID = Club.FootballClub.ID,
                    ContactMail = reader.GetString("ContactMail"),
                    ContactName = reader.GetString("ContactName"),
                    ContactNumber = reader.GetString("ContactNumber"),
                    Id = reader.GetInt32("ID"),
                    IsSponsorCurrently = reader.GetBoolean("IsCurrentSponsor"),
                    Name = reader.GetString("Name"),
                    SponsorType = reader.GetString("SponsorType").Equals("Company") ? SponsorType.Company : SponsorType.Individual
                });
            }
            reader.Close();

            return sponsors;
        }

        public List<Sponsor> GetCurrentSponsors()
        {
            List<Sponsor> sponsors = new List<Sponsor>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from sponsor where iscurrentsponsor = 1";

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                sponsors.Add(new Sponsor
                {
                    ClubID = Club.FootballClub.ID,
                    ContactMail = reader.GetString("ContactMail"),
                    ContactName = reader.GetString("ContactName"),
                    ContactNumber = reader.GetString("ContactNumber"),
                    Id = reader.GetInt32("ID"),
                    IsSponsorCurrently = reader.GetBoolean("IsCurrentSponsor"),
                    Name = reader.GetString("Name"),
                    SponsorType = reader.GetString("SponsorType").Equals("Company") ? SponsorType.Company : SponsorType.Individual
                });
            }
            reader.Close();

            return sponsors;
        }

        public void AddSponsor(Sponsor sponsor)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "insert into sponsor values(null, @Name, @SponsorType, @ClubID, @IsCurrentSponsor, @ContactMail, @ContactNumber, @ContactName)";
            command.Parameters.AddWithValue("@Name", sponsor.Name);
            command.Parameters.AddWithValue("@SponsorType", sponsor.SponsorType.ToString());
            command.Parameters.AddWithValue("@ClubID", Club.FootballClub.ID);
            command.Parameters.AddWithValue("@IsCurrentSponsor", sponsor.IsSponsorCurrently);
            command.Parameters.AddWithValue("@ContactMail", sponsor.ContactMail);
            command.Parameters.AddWithValue("@ContactNumber", sponsor.ContactNumber);
            command.Parameters.AddWithValue("@ContactName", sponsor.ContactName);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
                sponsor.Id = (int)command.LastInsertedId;
            }
            catch(MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }

        public void ModifySponsor(Sponsor sponsor)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "update sponsor set Name = @Name, SponsorType = @SponsorType, ClubID = @ClubID, IsCurrentSponsor = @IsCurrentSponsor, " +
                "ContactMail = @ContactMail, ContactNumber = @ContactNumber, ContactName = @ContactName where ID = @ID";
            command.Parameters.AddWithValue("@Name", sponsor.Name);
            command.Parameters.AddWithValue("@SponsorType", sponsor.SponsorType.ToString());
            command.Parameters.AddWithValue("@ClubID", Club.FootballClub.ID);
            command.Parameters.AddWithValue("@IsCurrentSponsor", sponsor.IsSponsorCurrently);
            command.Parameters.AddWithValue("@ContactMail", sponsor.ContactMail);
            command.Parameters.AddWithValue("@ContactNumber", sponsor.ContactNumber);
            command.Parameters.AddWithValue("@ContactName", sponsor.ContactName);
            command.Parameters.AddWithValue("@ID", sponsor.Id);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }

        public Sponsor GetSponsorByID(int id)
        {
            Sponsor sponsor = null;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from sponsor where ID = @ID";
            command.Parameters.AddWithValue("@ID", id);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                sponsor = new Sponsor
                {
                    ClubID = Club.FootballClub.ID,
                    ContactMail = reader.GetString("ContactMail"),
                    ContactName = reader.GetString("ContactName"),
                    ContactNumber = reader.GetString("ContactNumber"),
                    Id = reader.GetInt32("ID"),
                    IsSponsorCurrently = reader.GetBoolean("IsCurrentSponsor"),
                    Name = reader.GetString("Name"),
                    SponsorType = reader.GetString("SponsorType").Equals("Company") ? SponsorType.Company : SponsorType.Individual
                };
            }
            reader.Close();

            return sponsor;
        }

        public List<Sponsor> GetSponsorsByName(String name)
        {
            List<Sponsor> sponsors = new List<Sponsor>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from sponsor where Name like @NameParam";
            command.Parameters.AddWithValue("@NameParam", new Regex("%" + name + "%"));
            command.Prepare();
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                sponsors.Add( new Sponsor
                {
                    ClubID = Club.FootballClub.ID,
                    ContactMail = reader.GetString("ContactMail"),
                    ContactName = reader.GetString("ContactName"),
                    ContactNumber = reader.GetString("ContactNumber"),
                    Id = reader.GetInt32("ID"),
                    IsSponsorCurrently = reader.GetBoolean("IsCurrentSponsor"),
                    Name = reader.GetString("Name"),
                    SponsorType = reader.GetString("SponsorType").Equals("Company") ? SponsorType.Company : SponsorType.Individual
                });
            }
            reader.Close();

            return sponsors;
        }

        public void RemoveSponsor(Sponsor sponsor)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "update sponsor set iscurrentsponsor = 0 where id = @ID";
            command.Parameters.AddWithValue("@ID", sponsor.Id);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch(MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }
    }
}
