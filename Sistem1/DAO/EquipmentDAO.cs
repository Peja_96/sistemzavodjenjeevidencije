﻿using FootApp.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FootApp.DAO
{
    class EquipmentDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void AddEquipment(Equipment equipment)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "insert into equipment(Type,Quantity,ClubID) values(@Type, @Quantity, @ClubID)";
            MySqlParameter type = new MySqlParameter("@Type", MySqlDbType.VarChar) { Value = equipment.EquipmentType };
            MySqlParameter quantity = new MySqlParameter("@Quantity", MySqlDbType.Double) { Value = equipment.Quantity };
            MySqlParameter clubID = new MySqlParameter("@ClubID", MySqlDbType.Int32) { Value = equipment.ClubID };
            command.Parameters.AddRange(new object[] { type, quantity, clubID });
            command.Prepare();
            try
            {
                command.ExecuteNonQuery();
                equipment.ID = (int)command.LastInsertedId;
            }
            catch (MySqlException ex)
            {
                log.Warn(ex);
            }
        }

        public List<Equipment> GetEquipment()
        {
            List<Equipment> equipment = new List<Equipment>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from equipment";

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                equipment.Add(new Equipment()
                {
                    ID = reader.GetInt32("ID"),
                    EquipmentType = reader.GetString("Type"),
                    Quantity = reader.GetDouble("Quantity"),
                    ClubID = Club.FootballClub.ID
                });
            }
            reader.Close();

            return equipment;
        }
        public List<Equipment> GetEquipmentByType(String type)
        {
            List<Equipment> equipment = new List<Equipment>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from equipment where type like @TypeParam";
            command.Parameters.AddWithValue("@TypeParam", new Regex("%" + type + "%"));
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                equipment.Add(new Equipment()
                {
                    ID = reader.GetInt32("ID"),
                    EquipmentType = reader.GetString("Type"),
                    Quantity = reader.GetDouble("Quantity"),
                    ClubID = Club.FootballClub.ID
                });
            }
            reader.Close();
            return equipment;
        }
        public Equipment GetEquipmentByTypeEqual(String type)
        {
            Equipment equipment = null;
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from equipment where type=@TypeParam";
            command.Parameters.AddWithValue("@TypeParam", type);
            command.Prepare();
            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                equipment=new Equipment()
                {
                    ID = reader.GetInt32("ID"),
                    EquipmentType = reader.GetString("Type"),
                    Quantity = reader.GetDouble("Quantity"),
                    ClubID = Club.FootballClub.ID
                };
            }
            reader.Close();
            return equipment;
        }
        public void UpdateEquipment(Equipment equipment)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "UPDATE equipment SET Quantity = @Quantity WHERE ID = @ID;";
            command.Parameters.AddWithValue("@Quantity", equipment.Quantity);
            command.Parameters.AddWithValue("@ID", equipment.ID);
            command.Prepare();
            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                log.Warn(ex);
            }
        }
    }
}
