﻿using FootApp.DTO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DAO
{
    class ClubStatisticsDAO
    {
        public ClubStatistics GetClubStatisticsBySeason(Season season, Team team, string homeAway)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            if (homeAway.Equals("Total Games"))
                command.CommandText = "select * from ClubTotalStatistics where SeasonID=@SeasonID and Category = @Category";
            else if(homeAway.Equals("Home Games"))
                command.CommandText = "select * from ClubHomeStatistics where SeasonID=@SeasonID and Category = @Category";
            else
                command.CommandText = "select * from ClubAwayStatistics where SeasonID=@SeasonID and Category = @Category";
            command.Parameters.AddWithValue("@SeasonID", season.Id);
            command.Parameters.AddWithValue("@Category", team.Category);
            command.Prepare();
            ClubStatistics clubStatistics = new ClubStatistics() ;
            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                clubStatistics = new ClubStatistics
                {
                    Category = team.Category,
                    BallPossessionPerGame = reader.GetDecimal("BallPossessionPerGame"),
                    ClubShots = reader.GetInt32("ClubShots"),
                    ClubShotsOnGoal = reader.GetInt32("ClubShotsOnGoal"),
                    ClubShotsOnGoalPerGame = reader.GetDecimal("ClubShotsOnGoalPerGame"),
                    ClubShotsPerGame = reader.GetDecimal("ClubShotsPerGame"),
                    RedCards = reader.GetInt32("RedCards"),
                    RedCardsPerGame = reader.GetDecimal("RedCardsPerGame"),
                    YellowCards = reader.GetInt32("YellowCards"),
                    YellowCardsPerGame = reader.GetDecimal("YellowCardsPerGame"),
                    TotalPenalities = reader.GetInt32("TotalPenalities"),
                    PenalitiesPerGame = reader.GetDecimal("TotalPenalitiesPerGame"),
                    GoalsScored = reader.GetInt32("GoalsScored"),
                    GoalsScoredPerGame = reader.GetDecimal("GoalsScoredPerGame"),
                    GoalsConceded = reader.GetInt32("GoalsConceded"),
                    GoalsConcededPerGame = reader.GetDecimal("GoalsConcededPerGame"),
                    ClubCorners = reader.GetInt32("ClubCorners"),
                    ClubCornersPerGame = reader.GetDecimal("ClubCornersPerGame"),
                    ClubFreeKicks = reader.GetInt32("ClubFreeKicks"),
                    ClubFreeKicksPerGame = reader.GetDecimal("ClubFreeKicksPerGame"),
                    TicketsSoldPerGame = reader.GetDecimal("TicketsSoldPerGame"),
                    HomeWins = reader.GetInt32("HomeWins"),
                    HomeDraw = reader.GetInt32("HomeDraw"),
                    HomeLoses = reader.GetInt32("HomeLoses"),
                    Wins = reader.GetInt32("Wins"),
                    Draws = reader.GetInt32("Draws"),
                    Loses = reader.GetInt32("Loses")
                };
            }
            reader.Close();
            return clubStatistics;

        }
        public ClubStatistics GetClubStatistics(Team team, string homeAway)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            if (homeAway.Equals("Total Games"))
                command.CommandText = "select * from ClubTotalStatistics where Category = @Category";
            else if(homeAway.Equals("Home Games"))
                command.CommandText = "select * from ClubHomeStatistics where Category = @Category";
            else
                command.CommandText = "select * from ClubAwayStatistics where Category = @Category";
            command.Parameters.AddWithValue("@Category", team.Category);
            command.Prepare();
            ClubStatistics clubStatistics = new ClubStatistics();
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                clubStatistics = new ClubStatistics
                {
                    Category = team.Category,
                    BallPossessionPerGame = reader.GetDecimal("BallPossessionPerGame"),
                    ClubShots = reader.GetInt32("ClubShots"),
                    ClubShotsOnGoal = reader.GetInt32("ClubShotsOnGoal"),
                    ClubShotsOnGoalPerGame = reader.GetDecimal("ClubShotsOnGoalPerGame"),
                    ClubShotsPerGame = reader.GetDecimal("ClubShotsPerGame"),
                    RedCards = reader.GetInt32("RedCards"),
                    RedCardsPerGame = reader.GetDecimal("RedCardsPerGame"),
                    YellowCards = reader.GetInt32("YellowCards"),
                    YellowCardsPerGame = reader.GetDecimal("YellowCardsPerGame"),
                    TotalPenalities = reader.GetInt32("TotalPenalities"),
                    PenalitiesPerGame = reader.GetDecimal("TotalPenalitiesPerGame"),
                    GoalsScored = reader.GetInt32("GoalsScored"),
                    GoalsScoredPerGame = reader.GetDecimal("GoalsScoredPerGame"),
                    TicketsSoldPerGame = reader.GetDecimal("TicketsSoldPerGame"),
                    GoalsConceded = reader.GetInt32("GoalsConceded"),
                    GoalsConcededPerGame = reader.GetDecimal("GoalsConcededPerGame"),
                    HomeWins = reader.GetInt32("HomeWins"),
                    HomeDraw = reader.GetInt32("HomeDraw"),
                    HomeLoses = reader.GetInt32("HomeLoses"),
                    Wins = reader.GetInt32("Wins"),
                    Draws = reader.GetInt32("Draws"),
                    Loses = reader.GetInt32("Loses")
                };
            }
            reader.Close();
            return clubStatistics;

        }
        public ClubStatistics GetClubStatisticsForCompetition(Team team, Competition competition, string homeAway)
        {
            ClubStatistics clubStatistics = null;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            if (homeAway.Equals("Total Games"))
                command.CommandText = "select * from ClubTotalStatistics where Category = @Category and CompetitionID = @CompetitionID";
            else if (homeAway.Equals("Home Games"))
                command.CommandText = "select * from ClubHomeStatistics where Category = @Category and CompetitionID = @CompetitionID";
            else
                command.CommandText = "select * from ClubAwayStatistics where Category = @Category and CompetitionID = @CompetitionID";
            command.Parameters.AddWithValue("@Category", team.Category);
            command.Parameters.AddWithValue("@CompetitionID", competition.Id);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                clubStatistics = new ClubStatistics
                {
                    Category = team.Category,
                    BallPossessionPerGame = reader.GetDecimal("BallPossessionPerGame"),
                    ClubShots = reader.GetInt32("ClubShots"),
                    ClubShotsOnGoal = reader.GetInt32("ClubShotsOnGoal"),
                    ClubShotsOnGoalPerGame = reader.GetDecimal("ClubShotsOnGoalPerGame"),
                    ClubShotsPerGame = reader.GetDecimal("ClubShotsPerGame"),
                    RedCards = reader.GetInt32("RedCards"),
                    RedCardsPerGame = reader.GetDecimal("RedCardsPerGame"),
                    YellowCards = reader.GetInt32("YellowCards"),
                    YellowCardsPerGame = reader.GetDecimal("YellowCardsPerGame"),
                    TotalPenalities = reader.GetInt32("TotalPenalities"),
                    PenalitiesPerGame = reader.GetDecimal("TotalPenalitiesPerGame"),
                    GoalsScored = reader.GetInt32("GoalsScored"),
                    GoalsScoredPerGame = reader.GetDecimal("GoalsScoredPerGame"),
                    TicketsSoldPerGame = reader.GetDecimal("TicketsSoldPerGame"),
                    GoalsConceded = reader.GetInt32("GoalsConceded"),
                    GoalsConcededPerGame = reader.GetDecimal("GoalsConcededPerGame"),
                    HomeWins = reader.GetInt32("HomeWins"),
                    HomeDraw = reader.GetInt32("HomeDraw"),
                    HomeLoses = reader.GetInt32("HomeLoses"),
                    Wins = reader.GetInt32("Wins"),
                    Draws = reader.GetInt32("Draws"),
                    Loses = reader.GetInt32("Loses")
                };
            }
            reader.Close();

            return clubStatistics;
        }
    }
}
