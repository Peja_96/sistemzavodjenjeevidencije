﻿using FootApp.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DAO
{
    class BudgetDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public Budget GetBudgetForYear(int year)
        {
            Budget budget = null;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from budget";

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                budget = new Budget
                {
                    Amount = reader.GetDecimal("Amount"),
                    ClubID = reader.GetInt32("ClubID"),
                    Year = reader.GetInt32("Year")
                };
            }
            reader.Close();

            return budget;
        }

        public void AddNewBudget(Budget budget)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "insert into budget values(@Year, @Amount, @ClubID)";
            command.Parameters.AddWithValue("@Year", budget.Year);
            command.Parameters.AddWithValue("@Amount", budget.Amount);
            command.Parameters.AddWithValue("@ClubID", budget.ClubID);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }

        /// <summary>
        /// Restores currentcashflow value for entry
        /// </summary>
        /// <param name="financialEntry">FinancialEntry object</param>
        /// <param name="remove">True if you need to remove amaount from cash flow. False if you need to add it</param>
        public void UpdateCashFlowForEntry(FinancialEntry financialEntry, Budget budget, bool remove = true)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            if (remove)
                command.CommandText = "update budget set currentcashflow = currentcashflow - @Amount * (@Type = 'Income') + @Amount * (@Type = 'Epense') where year = @Year";
            else
                command.CommandText = "update budget set currentcashflow = currentcashflow + @Amount * (@Type = 'Income') - @Amount * (@Type = 'Epense') where year = @Year";
            command.Parameters.AddWithValue("@Amount", financialEntry.Amount);
            command.Parameters.AddWithValue("@Type", financialEntry.Type.ToString());
            command.Parameters.AddWithValue("@Year", budget.Year);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }

        private DateTime GetLastCashFlowComputeDate(int year)
        {
            DateTime lastComputeDate = DateTime.Now;
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select LastCashFlowCompute from budget where Year = @Year";
            command.Parameters.AddWithValue("@Year", year);
            command.Prepare();
           
            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
                lastComputeDate = reader.IsDBNull(0) ? lastComputeDate : reader.GetDateTime("LastCashFlowCompute");
            reader.Close();

            return lastComputeDate;
        }

        public void UpdateMonthlyCashFlow()
        {
            DateTime lastCompute = GetLastCashFlowComputeDate(DateTime.Now.Year);
            if ((lastCompute.Year == DateTime.Now.Year && lastCompute.Month < DateTime.Now.Month) || lastCompute.Year < DateTime.Now.Year)
            {
                new FinancialEntryDAO().AddSalaryEntries();
                MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
                command.CommandText = "update budget set lastcashflowcompute = now() where Year = @Year";
                command.Parameters.AddWithValue("@Year", DateTime.Now.Year);
                command.Prepare();

                try
                {
                    command.ExecuteNonQuery();
                }
                catch(MySqlException mysqlex)
                {
                    log.Warn(mysqlex);
                }
            }
        }
    }
}
