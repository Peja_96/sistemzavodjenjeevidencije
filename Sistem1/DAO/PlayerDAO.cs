﻿    using FootApp.DAO;
using FootApp.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FootApp.DAO
{
    class PlayerDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void AddPlayer(Player player)
        {
            new PersonDAO().AddPerson(player);
            DatabaseConnection db = DatabaseConnection.GetConnection();
            MySqlCommand sqlCommand = db.CreateCommand(); 
            sqlCommand.CommandText = "insert into player(PersonID,Category,RegistrationNumber,isInjured,isBought,Position,Height,DateJoined,PrimaryFoot, ImageFilePath) values(@id,@category,@regNumber,@isInjured,@isBought,@position, @height,@dateJoined,@primaryFoot, @imagepath);";
            MySqlParameter id = new MySqlParameter() { ParameterName = "@id", Value = player.ID, MySqlDbType = MySqlDbType.Int32 };
            MySqlParameter category = new MySqlParameter() { ParameterName = "@category", Value = player.Category, MySqlDbType = MySqlDbType.VarChar };
            MySqlParameter regNumber = new MySqlParameter() { ParameterName = "@regNumber", Value = player.RegistrationNumber, MySqlDbType = MySqlDbType.VarChar };
            MySqlParameter isInjured = new MySqlParameter() { ParameterName = "@isInjured", Value = player.IsInjured, MySqlDbType = MySqlDbType.Byte };
            MySqlParameter isBought = new MySqlParameter() { ParameterName = "@isBought", Value = player.IsBought, MySqlDbType = MySqlDbType.Byte };
            MySqlParameter position = new MySqlParameter() { ParameterName = "@position", Value = player.Position, MySqlDbType = MySqlDbType.VarChar };
            MySqlParameter height = new MySqlParameter() { ParameterName = "@height", Value = player.Height, MySqlDbType = MySqlDbType.Int32 };
            MySqlParameter primaryFoot = new MySqlParameter() { ParameterName = "@primaryFoot", Value = player.PrimaryFoot, MySqlDbType = MySqlDbType.VarChar };
            MySqlParameter dateJoined = new MySqlParameter() { ParameterName = "@dateJoined", Value = player.DateJoined, MySqlDbType = MySqlDbType.DateTime };
            MySqlParameter imagePath = new MySqlParameter() { ParameterName = "@imagepath", Value = player.ImageFilePath, MySqlDbType = MySqlDbType.String };
            sqlCommand.Parameters.AddRange(new object[] { id, category, regNumber, isInjured, isBought, position, height ,dateJoined,primaryFoot, imagePath});
            sqlCommand.Prepare();
            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                log.Warn(ex);
            }
        }
        public List<Player> GetPlayers()
        {
            List<Player> players=new List<Player>();
            DatabaseConnection db = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = db.CreateCommand();
            mySqlCommand.CommandText = "select * from player inner join person on id = personid where isactive=1";
            MySqlDataReader mySqlData = mySqlCommand.ExecuteReader();
            while (mySqlData.Read())
            {
                players.Add(new Player()
                {
                    ID = mySqlData.GetInt32("PersonID"),
                    Name = mySqlData.GetString("Name"),
                    Surname = mySqlData.GetString("Surname"),
                    DateOfBirth = mySqlData.GetDateTime("DateOfBirth"),
                    RegistrationNumber = mySqlData.GetString("RegistrationNumber"),
                    Position = mySqlData.GetString("Position"),
                    IsBought = mySqlData.GetBoolean("IsBought"),
                    IsInjured = mySqlData.GetBoolean("IsInjured"),
                    Category = mySqlData.GetString("Category"),
                    Height = mySqlData.GetInt32("Height"),
                    DateJoined= mySqlData.GetDateTime("DateJoined"),
                    PrimaryFoot= mySqlData.GetString("PrimaryFoot"),
                    ImageFilePath = mySqlData.GetString("ImageFilePath")
                });
            }
            mySqlData.Close();
            return players;
        }
        public List<Player> GetInactivePlayers()
        {
            List<Player> players = new List<Player>();
            DatabaseConnection db = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = db.CreateCommand();
            mySqlCommand.CommandText = "select * from player inner join person on id = personid where isactive=0";
            MySqlDataReader mySqlData = mySqlCommand.ExecuteReader();
            while (mySqlData.Read())
            {
                players.Add(new Player()
                {
                    ID = mySqlData.GetInt32("PersonID"),
                    Name = mySqlData.GetString("Name"),
                    Surname = mySqlData.GetString("Surname"),
                    DateOfBirth = mySqlData.GetDateTime("DateOfBirth"),
                    RegistrationNumber = mySqlData.GetString("RegistrationNumber"),
                    Position = mySqlData.GetString("Position"),
                    IsBought = mySqlData.GetBoolean("IsBought"),
                    IsInjured = mySqlData.GetBoolean("IsInjured"),
                    Category = mySqlData.GetString("Category"),
                    Height = mySqlData.GetInt32("Height"),
                    DateJoined = mySqlData.GetDateTime("DateJoined"),
                    PrimaryFoot = mySqlData.GetString("PrimaryFoot"),
                    ImageFilePath = mySqlData.GetString("ImageFilePath")
                });
            }
            mySqlData.Close();
            return players;
        }

        public Player GetPlayerByID(int id)
        {
            Player player = null;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from player inner join person on id = personid where isactive=1 and id = @ID";
            command.Parameters.AddWithValue("@ID", id);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                player = new Player()
                {
                    ID = reader.GetInt32("PersonID"),
                    Name = reader.GetString("Name"),
                    Surname = reader.GetString("Surname"),
                    DateOfBirth = reader.GetDateTime("DateOfBirth"),
                    RegistrationNumber = reader.GetString("RegistrationNumber"),
                    Position = reader.GetString("Position"),
                    IsBought = reader.GetBoolean("IsBought"),
                    IsInjured = reader.GetBoolean("IsInjured"),
                    Category = reader.GetString("Category"),
                    Height = reader.GetInt32("Height"),
                    DateJoined = reader.GetDateTime("DateJoined"),
                    PrimaryFoot = reader.GetString("PrimaryFoot"),
                    ImageFilePath = reader.GetString("ImageFilePath")
                };
            }
            reader.Close();

            return player;
        }

        public List<Player> GetPlayersByName(string name)
        {
            List<Player> players = new List<Player>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from player inner join person on id = personid where concat(Name, ' ', Surname) like @NameParam and isactive = 1";
            command.Parameters.AddWithValue("@NameParam", new Regex("%" + name + "%"));
            MySqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                players.Add(new Player()
                {
                    ID = reader.GetInt32("PersonID"),
                    Name = reader.GetString("Name"),
                    Surname = reader.GetString("Surname"),
                    DateOfBirth = reader.GetDateTime("DateOfBirth"),
                    RegistrationNumber = reader.GetString("RegistrationNumber"),
                    Position = reader.GetString("Position"),
                    IsBought = reader.GetBoolean("IsBought"),
                    IsInjured = reader.GetBoolean("IsInjured"),
                    Category = reader.GetString("Category"),
                    Height = reader.GetInt32("Height"),
                    DateJoined = reader.GetDateTime("DateJoined"),
                    PrimaryFoot = reader.GetString("PrimaryFoot"),
                    ImageFilePath = reader.GetString("ImageFilePath")
                });
            }
            reader.Close();

            return players;
        }


        public void DeletePlayer(Player player)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "update person set isactive = 0 where id = @ID";
            command.Parameters.AddWithValue("@ID", player.ID);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch(MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="player">Modified player</param>
        public void UpdatePlayerInfo(Player player)
        {
            new PersonDAO().UpdatePersonInfo(player);

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "update player set RegistrationNumber = @RegistrationNumber, Position = @Position, Height = @Height, " +
                "DateJoined = @DateJoined, PrimaryFoot = @PrimaryFoot, IsInjured = @IsInjured, IsBought = @IsBought, ImageFilePath = @ImageFilePath where PersonID = @PersonID and Category = @Category";
            command.Parameters.AddWithValue("@RegistrationNumber", player.RegistrationNumber);
            command.Parameters.AddWithValue("@Position", player.Position);
            command.Parameters.AddWithValue("@Height", player.Height);
            command.Parameters.AddWithValue("@DateJoined", player.DateJoined);
            command.Parameters.AddWithValue("@PrimaryFoot", player.PrimaryFoot);
            command.Parameters.AddWithValue("@IsInjured", player.IsInjured);
            command.Parameters.AddWithValue("@IsBought", player.IsBought);
            command.Parameters.AddWithValue("@PersonID", player.ID);
            command.Parameters.AddWithValue("@Category", player.Category);
            command.Parameters.AddWithValue("@ImageFilePath", player.ImageFilePath ?? "default");
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }
        public List<Player> GetTenPlayersByCategory(String category,int offset)
        {
            List<Player> players = new List<Player>();
            DatabaseConnection db = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = db.CreateCommand();
            mySqlCommand.CommandText = "select * from player inner join person on id = personid where isactive=1 and Category=@Category limit 10 offset @Offset";
            mySqlCommand.Parameters.AddWithValue("@Category",category);
            mySqlCommand.Parameters.AddWithValue("@Offset", offset);
            MySqlDataReader mySqlData = mySqlCommand.ExecuteReader();
            while (mySqlData.Read())
            {
                players.Add(new Player()
                {
                    ID = mySqlData.GetInt32("PersonID"),
                    Name = mySqlData.GetString("Name"),
                    Surname = mySqlData.GetString("Surname"),
                    DateOfBirth = mySqlData.GetDateTime("DateOfBirth"),
                    RegistrationNumber = mySqlData.GetString("RegistrationNumber"),
                    Position = mySqlData.GetString("Position"),
                    IsBought = mySqlData.GetBoolean("IsBought"),
                    IsInjured = mySqlData.GetBoolean("IsInjured"),
                    Category = mySqlData.GetString("Category"),
                    Height = mySqlData.GetInt32("Height"),
                    DateJoined = mySqlData.GetDateTime("DateJoined"),
                    PrimaryFoot = mySqlData.GetString("PrimaryFoot"),
                    ImageFilePath = mySqlData.GetString("ImageFilePath")
                });
            }
            mySqlData.Close();
            return players;
        }
        public List<Player> GetTenPlayers(int offset)
        {
            List<Player> players = new List<Player>();
            DatabaseConnection db = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = db.CreateCommand();
            mySqlCommand.CommandText = "select * from player inner join person on id = personid where isactive=1 limit 10 offset @Offset";
            mySqlCommand.Parameters.AddWithValue("@Offset",offset);
            MySqlDataReader mySqlData = mySqlCommand.ExecuteReader();
            while (mySqlData.Read())
            {
                players.Add(new Player()
                {
                    ID = mySqlData.GetInt32("PersonID"),
                    Name = mySqlData.GetString("Name"),
                    Surname = mySqlData.GetString("Surname"),
                    DateOfBirth = mySqlData.GetDateTime("DateOfBirth"),
                    RegistrationNumber = mySqlData.GetString("RegistrationNumber"),
                    Position = mySqlData.GetString("Position"),
                    IsBought = mySqlData.GetBoolean("IsBought"),
                    IsInjured = mySqlData.GetBoolean("IsInjured"),
                    Category = mySqlData.GetString("Category"),
                    Height = mySqlData.GetInt32("Height"),
                    DateJoined = mySqlData.GetDateTime("DateJoined"),
                    PrimaryFoot = mySqlData.GetString("PrimaryFoot"),
                    ImageFilePath = mySqlData.GetString("ImageFilePath")
                });
            }
            mySqlData.Close();
            return players;
        }
        public List<Player> GetPlayersByCategoty(String category)
        {
            List<Player> players = new List<Player>();
            DatabaseConnection db = DatabaseConnection.GetConnection();
            MySqlCommand mySqlCommand = db.CreateCommand();
            mySqlCommand.CommandText = "select * from player inner join person on id = personid where isactive=1 and Category=@Category ";
            mySqlCommand.Parameters.AddWithValue("@Category", category);
            MySqlDataReader mySqlData = mySqlCommand.ExecuteReader();
            while (mySqlData.Read())
            {
                players.Add(new Player()
                {
                    ID = mySqlData.GetInt32("PersonID"),
                    Name = mySqlData.GetString("Name"),
                    Surname = mySqlData.GetString("Surname"),
                    DateOfBirth = mySqlData.GetDateTime("DateOfBirth"),
                    RegistrationNumber = mySqlData.GetString("RegistrationNumber"),
                    Position = mySqlData.GetString("Position"),
                    IsBought = mySqlData.GetBoolean("IsBought"),
                    IsInjured = mySqlData.GetBoolean("IsInjured"),
                    Category = mySqlData.GetString("Category"),
                    Height = mySqlData.GetInt32("Height"),
                    DateJoined = mySqlData.GetDateTime("DateJoined"),
                    PrimaryFoot = mySqlData.GetString("PrimaryFoot"),
                    ImageFilePath = mySqlData.GetString("ImageFilePath")
                });
            }
            mySqlData.Close();
            return players;
        }

        public List<Player> GetPlayerByNameAndCategory(string name, string category)
        {
            List<Player> players = new List<Player>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from player inner join person on id = personid where concat(Name, ' ', Surname) like @NameParam and isactive = 1 and  category = @Category";
            command.Parameters.AddWithValue("@NameParam", new Regex("%" + name + "%"));
            command.Parameters.AddWithValue("@Category", category);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                players.Add(new Player()
                {
                    ID = reader.GetInt32("PersonID"),
                    Name = reader.GetString("Name"),
                    Surname = reader.GetString("Surname"),
                    DateOfBirth = reader.GetDateTime("DateOfBirth"),
                    RegistrationNumber = reader.GetString("RegistrationNumber"),
                    Position = reader.GetString("Position"),
                    IsBought = reader.GetBoolean("IsBought"),
                    IsInjured = reader.GetBoolean("IsInjured"),
                    Category = reader.GetString("Category"),
                    Height = reader.GetInt32("Height"),
                    DateJoined = reader.GetDateTime("DateJoined"),
                    PrimaryFoot = reader.GetString("PrimaryFoot"),
                    ImageFilePath = reader.GetString("ImageFilePath")
                });
            }
            reader.Close();

            return players;
        }
    }
}
