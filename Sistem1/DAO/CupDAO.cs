﻿using FootApp.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FootApp.DAO
{
    public class CupDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<Cup> GetCups()
        {
            List<Cup> cups = new List<Cup>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from cup inner join competition on id = competitionid";

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                cups.Add(new Cup
                {
                    FinalResult = reader.IsDBNull(1) ? "N/A" : reader.GetString("FinalResult"),
                    ClubID = Club.FootballClub.ID,
                    Id = reader.GetInt32("ID"),
                    Description = reader.GetString("Description"),
                    Draws = reader.GetInt16("Draws"),
                    Loses = reader.GetInt16("Loses"),
                    Wins = reader.GetInt16("Wins"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Title = reader.GetString("Title"),
                    Category = reader.GetString("Category"), 
                    Type = "Cup",
                    SeasonID = reader.GetInt32("SeasonID")
                });
            }
            reader.Close();

            return cups;
        }

        public void AddCup(Cup cup)
        {
            new CompetitionDAO().AddCompetition(cup);
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "insert into cup values (@ID, null)";
            command.Parameters.AddWithValue("@ID", cup.Id);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch(MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }

        public Cup GetCupByID(int id)
        {
            Cup cup = null;
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from cup inner join competition on id = competitionid where id = @ID";
            command.Parameters.AddWithValue("@ID", id);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();

            if(reader.Read())
            {
                cup = new Cup
                {
                    FinalResult = reader.IsDBNull(1) ? "N/A" : reader.GetString("FinalResult"),
                    ClubID = Club.FootballClub.ID,
                    Id = reader.GetInt32("ID"),
                    Description = reader.GetString("Description"),
                    Draws = reader.GetInt16("Draws"),
                    Loses = reader.GetInt16("Loses"),
                    Wins = reader.GetInt16("Wins"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Title = reader.GetString("Title"),
                    Category = reader.GetString("Category"),
                    Type = "Cup",
                    SeasonID = reader.GetInt32("SeasonID")
                };
            }
            reader.Close();

            return cup;
        }

        public List<Cup> GetCupsBySeason(Season season)
        {
            List<Cup> cups = new List<Cup>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from cup inner join competition on id = competitionid where seasonid = @ID";
            command.Parameters.AddWithValue("@ID", season.Id);
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                cups.Add(new Cup
                {
                    FinalResult = reader.IsDBNull(1) ? "N/A" : reader.GetString("FinalResult"),
                    ClubID = Club.FootballClub.ID,
                    Id = reader.GetInt32("ID"),
                    Description = reader.GetString("Description"),
                    Draws = reader.GetInt16("Draws"),
                    Loses = reader.GetInt16("Loses"),
                    Wins = reader.GetInt16("Wins"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Title = reader.GetString("Title"),
                    Category = reader.GetString("Category"),
                    Type = "Cup",
                    SeasonID = reader.GetInt32("SeasonID")
                });
            }
            reader.Close();

            return cups;
        }

        public List<Cup> GetCupsForCurrentSeason()
        {
            return GetCupsBySeason(new SeasonDAO().GetCurrentSeason());
        }

        public List<Cup> GetCupsBySeasonAndTeam(Season season, Team team)
        {
            List<Cup> cups = new List<Cup>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from cup inner join competition on id = competitionid where seasonid = @ID and category = @Category";
            command.Parameters.AddWithValue("@ID", season.Id);
            command.Parameters.AddWithValue("@Category", team.Category);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                cups.Add(new Cup
                {
                    FinalResult = reader.IsDBNull(1) ? "N/A" : reader.GetString("FinalResult"),
                    ClubID = Club.FootballClub.ID,
                    Id = reader.GetInt32("ID"),
                    Description = reader.GetString("Description"),
                    Draws = reader.GetInt16("Draws"),
                    Loses = reader.GetInt16("Loses"),
                    Wins = reader.GetInt16("Wins"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Title = reader.GetString("Title"),
                    Category = reader.GetString("Category"),
                    Type = "Cup",
                    SeasonID = reader.GetInt32("SeasonID")
                });
            }
            reader.Close();

            return cups;
        }

        public void DeleteCup(Cup cup)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "delete from cup where competitionid = @ID";
            command.Parameters.AddWithValue("@ID", cup.Id);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
                new CompetitionDAO().DeleteCompetition(cup);
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }

        public List<Cup> GetCupsByTitle(string title)
        {
            List<Cup> cups = new List<Cup>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from cup inner join competition on id = competitionid where title like @TitleParam";
            command.Parameters.AddWithValue("@TitleParam", new Regex("%" + title + "%"));
            command.Prepare();
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                cups.Add(new Cup
                {
                    FinalResult = reader.IsDBNull(1) ? "N/A" : reader.GetString("FinalResult"),
                    ClubID = Club.FootballClub.ID,
                    Id = reader.GetInt32("ID"),
                    Description = reader.GetString("Description"),
                    Draws = reader.GetInt16("Draws"),
                    Loses = reader.GetInt16("Loses"),
                    Wins = reader.GetInt16("Wins"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Title = reader.GetString("Title"),
                    Category = reader.GetString("Category"),
                    Type = "Cup",
                    SeasonID = reader.GetInt32("SeasonID")
                });
            }
            reader.Close();

            return cups;
        }
    }
}
