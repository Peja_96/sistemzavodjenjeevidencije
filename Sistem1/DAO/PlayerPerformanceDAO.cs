﻿using FootApp.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DAO
{
    class PlayerPerformanceDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public PlayerPerformance GetPlayerPerformance(Game game, Player player)
        {
            PlayerPerformance playerPerformance = null;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from playerperformance where playerid = @PlayerID and playercategory = @PlayerCategory and " +
                "competitionid = @CompetitionID and starttime = @StartTime";
            command.Parameters.AddWithValue("@PlayerID", player.ID);
            command.Parameters.AddWithValue("@PlayerCategory", player.Category);
            command.Parameters.AddWithValue("@CompetitionID", game.CompetitionID);
            command.Parameters.AddWithValue("@StartTime", game.StartTime);

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                playerPerformance = new PlayerPerformance
                {
                    Assists = reader.GetInt16("Assists"),
                    Category = reader.GetString("Category"),
                    CompetitionID = game.CompetitionID,
                    Fouls = reader.GetInt16("Fouls"),
                    Goals = reader.GetInt16("Goals"),
                    PlayerCategory = player.Category,
                    PlayerID = player.ID,
                    RedCard = reader.GetInt16("RedCard") > 0 ? true : false,
                    ShotsOnGoal = reader.GetInt16("ShotsOnGoal"),
                    StartTime = game.StartTime,
                    SuccessfulPasses = reader.GetInt16("SuccessfulPasses"),
                    TotalPasses = reader.GetInt16("TotalPasses"),
                    TotalShots = reader.GetInt16("TotalShots"),
                    YellowCards = reader.GetInt16("YellowCards")
                };
            }
            reader.Close();

            return playerPerformance;
        }

        public List<PlayerPerformance> GetPerformancesByPlayer(Player player)
        {
            List<PlayerPerformance> playerPerformances = new List<PlayerPerformance>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from playerperformance where playerid = @PlayerID and playercategory = @PlayerCategory";
            command.Parameters.AddWithValue("@PlayerID", player.ID);
            command.Parameters.AddWithValue("@PlayerCategory", player.Category);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                playerPerformances.Add(new PlayerPerformance
                {
                    Assists = reader.GetInt16("Assists"),
                    Category = reader.GetString("Category"),
                    CompetitionID = reader.GetInt32("CompetitionID"),
                    Fouls = reader.GetInt16("Fouls"),
                    Goals = reader.GetInt16("Goals"),
                    PlayerCategory = player.Category,
                    PlayerID = player.ID,
                    RedCard = reader.GetInt16("RedCard") > 0 ? true : false,
                    ShotsOnGoal = reader.GetInt16("ShotsOnGoal"),
                    StartTime = reader.GetDateTime("StartTime"),
                    SuccessfulPasses = reader.GetInt16("SuccessfulPasses"),
                    TotalPasses = reader.GetInt16("TotalPasses"),
                    TotalShots = reader.GetInt16("TotalShots"),
                    YellowCards = reader.GetInt16("YellowCards")
                });
            }
            reader.Close();

            return playerPerformances;
        }

        public void AddPerformanceForPlayer(PlayerPerformance performance)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "insert into playerperformance values (@PlayerID, @PlayerCategory, @CompetitionID, @StartTime, @Category, @Goals, @Fouls, " +
                "@YellowCards, @RedCard, @TotalPasses, @SuccessfulPasses, @TotalShots, @ShotsOnGoal, @Assists)";

            command.Parameters.AddWithValue("@PlayerID", performance.PlayerID);
            command.Parameters.AddWithValue("@PlayerCategory", performance.PlayerCategory);
            command.Parameters.AddWithValue("@CompetitionID", performance.CompetitionID);
            command.Parameters.AddWithValue("@StartTime", performance.StartTime);
            command.Parameters.AddWithValue("@Category", performance.Category);
            command.Parameters.AddWithValue("@Goals", performance.Goals);
            command.Parameters.AddWithValue("@Fouls", performance.Fouls);
            command.Parameters.AddWithValue("@YellowCards", performance.YellowCards);
            command.Parameters.AddWithValue("@RedCard", performance.RedCard);
            command.Parameters.AddWithValue("@TotalPasses", performance.TotalPasses);
            command.Parameters.AddWithValue("@SuccessfulPasses", performance.SuccessfulPasses);
            command.Parameters.AddWithValue("@TotalShots", performance.TotalShots);
            command.Parameters.AddWithValue("@ShotsOnGoal", performance.ShotsOnGoal);
            command.Parameters.AddWithValue("@Assists", performance.Assists);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch(MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }

        public void ModifyPlayerPerformance(PlayerPerformance playerPerformance)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "update playerperformance set Goals = @Goals, Fouls = @Fouls, Assists = @Assists, YellowCards = @YellowCards, RedCard = @RedCard, " +
                "TotalPasses = @TotalPasses, SuccessfulPasses = @SuccessfulPasses, TotalShots = @TotalShots, ShotsOnGoal = @ShotsOnGoal where " +
                "PlayerID = @PlayerID and PlayerCategory = @PlayerCategory and CompetitionID = @CompetitionID and StartTime = @StartTime and Category = @Category";
            command.Parameters.AddWithValue("@PlayerID", playerPerformance.PlayerID);
            command.Parameters.AddWithValue("@PlayerCategory", playerPerformance.PlayerCategory);
            command.Parameters.AddWithValue("@CompetitionID", playerPerformance.CompetitionID);
            command.Parameters.AddWithValue("@StartTime", playerPerformance.StartTime);
            command.Parameters.AddWithValue("@Category", playerPerformance.Category);
            command.Parameters.AddWithValue("@Goals", playerPerformance.Goals);
            command.Parameters.AddWithValue("@Fouls", playerPerformance.Fouls);
            command.Parameters.AddWithValue("@YellowCards", playerPerformance.YellowCards);
            command.Parameters.AddWithValue("@RedCard", playerPerformance.RedCard);
            command.Parameters.AddWithValue("@TotalPasses", playerPerformance.TotalPasses);
            command.Parameters.AddWithValue("@SuccessfulPasses", playerPerformance.SuccessfulPasses);
            command.Parameters.AddWithValue("@TotalShots", playerPerformance.TotalShots);
            command.Parameters.AddWithValue("@ShotsOnGoal", playerPerformance.ShotsOnGoal);
            command.Parameters.AddWithValue("@Assists", playerPerformance.Assists);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch(MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }
    }
}
