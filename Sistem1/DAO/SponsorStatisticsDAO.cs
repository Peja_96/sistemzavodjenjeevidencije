﻿using FootApp.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DAO
{
    class SponsorStatisticsDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public SponsorStatistics GetSponsorStatistics()
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from sponsorcontract_statistics";
            command.Prepare();
            SponsorStatistics sponsorStatistics = null;
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                sponsorStatistics = new SponsorStatistics
                {
                    AllInvestment = reader.GetDouble("AllInvestment"),
                    InvestmentPerSponsor = reader.GetDouble("InvestmentPerSponsor"),
                    MaxInvestment = reader.GetDouble("MaxInvestment"),
                    MinInvestment = reader.GetDouble("MinInvestment"),
                    NumberContracts = reader.GetInt32("NumberContracts"),
                    Sponsors = reader.GetInt32("Sponsors")
                };
            }
            reader.Close();

            return sponsorStatistics;
        }
        public List<SponsorStatistics> GetSponsorStatisticsPerYears()
        {
            List<SponsorStatistics> sponsorStatistics = new List<SponsorStatistics>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from sponsorcontract_statistics_per_year";
            command.Prepare();
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                sponsorStatistics.Add(new SponsorStatistics
                {
                    AllInvestment = reader.GetDouble("AllInvestment"),
                    InvestmentPerSponsor = reader.GetDouble("InvestmentPerSponsor"),
                    MaxInvestment = reader.GetDouble("MaxInvestment"),
                    MinInvestment = reader.GetDouble("MinInvestment"),
                    NumberContracts = reader.GetInt32("NumberContracts"),
                    Sponsors = reader.GetInt32("Sponsors"),
                    Year = reader.GetInt32("Year")
                });
            }
            reader.Close();

            return sponsorStatistics;
        }
        public List<SponsorStatistics> GetSponsorStatisticsPerYearsBySponsor(Sponsor sponsor)
        {
            List<SponsorStatistics> sponsorStatistics = new List<SponsorStatistics>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from sponsorcontract_statistics_per_year_by_sponsor where SponsorID=@SponsorID";
            command.Parameters.AddWithValue("@SponsorID", sponsor.Id);
            command.Prepare();
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                sponsorStatistics.Add(new SponsorStatistics
                {
                    AllInvestment = reader.GetDouble("AllInvestment"),
                    InvestmentPerSponsor = reader.GetDouble("InvestmentPerSponsor"),
                    MaxInvestment = reader.GetDouble("MaxInvestment"),
                    MinInvestment = reader.GetDouble("MinInvestment"),
                    NumberContracts = reader.GetInt32("NumberContracts"),
                    Sponsors = reader.GetInt32("Sponsors"),
                    Year = reader.GetInt32("Year")
                });
            }
            reader.Close();

            return sponsorStatistics;
        }
        public SponsorStatistics GetSponsorStatisticsBySposnsor(Sponsor sponsor)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from sponsorcontract_statistics_by_sponsor where SponsorID=@SponsorID";
            command.Parameters.AddWithValue("@SponsorID", sponsor.Id);
            command.Prepare();
            SponsorStatistics sponsorStatistics = null;
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                sponsorStatistics = new SponsorStatistics
                {
                    AllInvestment = reader.GetDouble("AllInvestment"),
                    InvestmentPerSponsor = reader.GetDouble("InvestmentPerSponsor"),
                    MaxInvestment = reader.GetDouble("MaxInvestment"),
                    MinInvestment = reader.GetDouble("MinInvestment"),
                    NumberContracts = reader.GetInt32("NumberContracts"),
                    Sponsors = reader.GetInt32("Sponsors")
                };
            }
            reader.Close();

            return sponsorStatistics;
        }

    }
}
