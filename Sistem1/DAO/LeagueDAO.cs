﻿using FootApp.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FootApp.DAO
{
    class LeagueDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<League> GetLeagues()
        {
            List<League> leagues = new List<League>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();

            command.CommandText = "select * from league inner join competition on id = competitionid";
            
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                leagues.Add(new League
                {
                    Id = reader.GetInt32("CompetitionID"),
                    Points = reader.GetInt16("Points"),
                    FinalPlace = reader.IsDBNull(2) ? (short)0 : reader.GetInt16("FinalPlace"),
                    ClubID = Club.FootballClub.ID,
                    Description = reader.GetString("Description"),
                    Draws = reader.GetInt16("Draws"),
                    Loses = reader.GetInt16("Loses"),
                    Wins = reader.GetInt16("Wins"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Title = reader.GetString("Title"),
                    Category = reader.GetString("Category"),
                    Type = "League",
                    SeasonID = reader.GetInt32("SeasonID")
                });
            }
            reader.Close();
            
            return leagues;
        }

        public void AddLeague(League league)
        {
            new CompetitionDAO().AddCompetition(league);
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "insert into league values (@ID, 0, null)";
            command.Parameters.AddWithValue("@ID", league.Id);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
                
            }
            catch(MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }

        public League GetLeagueByID(int id)
        {
            League league = null;

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from league inner join competition on id = competitionid where id = @ID";
            command.Parameters.AddWithValue("@ID", id);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                league = new League
                {
                    Id = reader.GetInt32("CompetitionID"),
                    Points = reader.GetInt16("Points"),
                    FinalPlace = reader.IsDBNull(2) ? (short)0 : reader.GetInt16("FinalPlace"),
                    ClubID = Club.FootballClub.ID,
                    Description = reader.GetString("Description"),
                    Draws = reader.GetInt16("Draws"),
                    Loses = reader.GetInt16("Loses"),
                    Wins = reader.GetInt16("Wins"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Title = reader.GetString("Title"),
                    Category = reader.GetString("Category"),
                    Type = "League", 
                    SeasonID = reader.GetInt32("SeasonID")
                };
            }
            reader.Close();

            return league;
        }

        public List<League> GetLeaguesBySeason(Season season)
        {
            List<League> leagues = new List<League>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from cup inner join competition on id = competitionid where seasonid = @ID";
            command.Parameters.AddWithValue("@ID", season.Id);
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                leagues.Add(new League
                {
                    ClubID = Club.FootballClub.ID,
                    Id = reader.GetInt32("ID"),
                    Points = reader.GetInt16("Points"),
                    FinalPlace = reader.IsDBNull(2) ? (short)0 : reader.GetInt16("FinalPlace"),
                    Description = reader.GetString("Description"),
                    Draws = reader.GetInt16("Draws"),
                    Loses = reader.GetInt16("Loses"),
                    Wins = reader.GetInt16("Wins"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Title = reader.GetString("Title"),
                    Category = reader.GetString("Category"),
                    Type = "Cup",
                    SeasonID = reader.GetInt32("SeasonID")
                });
            }
            reader.Close();

            return leagues;
        }

        public List<League> GetLeaguesForCurrentSeason()
        {
            return GetLeaguesBySeason(new SeasonDAO().GetCurrentSeason());
        }

        public List<League> GetLeaguesBySeasonAndTeam(Season season, Team team)
        {
            List<League> leagues = new List<League>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();

            command.CommandText = "select * from league inner join competition on id = competitionid where season" +
                "id = @ID and category = @Category";
            command.Parameters.AddWithValue("@ID", season.Id);
            command.Parameters.AddWithValue("@Category", team.Category);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                leagues.Add(new League
                {
                    Id = reader.GetInt32("CompetitionID"),
                    Points = reader.GetInt16("Points"),
                    FinalPlace = reader.IsDBNull(2) ? (short)0 : reader.GetInt16("FinalPlace"),
                    ClubID = Club.FootballClub.ID,
                    Description = reader.GetString("Description"),
                    Draws = reader.GetInt16("Draws"),
                    Loses = reader.GetInt16("Loses"),
                    Wins = reader.GetInt16("Wins"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Title = reader.GetString("Title"),
                    Category = reader.GetString("Category"),
                    Type = "League",
                    SeasonID = reader.GetInt32("SeasonID")
                });
            }
            reader.Close();

            return leagues;
        }

        public void DeleteLeague(League league)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "delete from league where competitionid = @ID";
            command.Parameters.AddWithValue("@ID", league.Id);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
                new CompetitionDAO().DeleteCompetition(league);
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }

        public List<League> GetLeaguesByTitle(string title)
        {
            List<League> leagues = new List<League>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();

            command.CommandText = "select * from league inner join competition on id = competitionid where title like @TitleParam";
            command.Parameters.AddWithValue("@TitleParam", new Regex("%" + title + "%"));
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                leagues.Add(new League
                {
                    Id = reader.GetInt32("CompetitionID"),
                    Points = reader.GetInt16("Points"),
                    FinalPlace = reader.IsDBNull(2) ? (short)0 : reader.GetInt16("FinalPlace"),
                    ClubID = Club.FootballClub.ID,
                    Description = reader.GetString("Description"),
                    Draws = reader.GetInt16("Draws"),
                    Loses = reader.GetInt16("Loses"),
                    Wins = reader.GetInt16("Wins"),
                    StartDate = reader.GetDateTime("StartDate"),
                    EndDate = reader.GetDateTime("EndDate"),
                    Title = reader.GetString("Title"),
                    Category = reader.GetString("Category"),
                    Type = "League",
                    SeasonID = reader.GetInt32("SeasonID")
                });
            }
            reader.Close();

            return leagues;
        }
    }
}
