﻿using FootApp.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.Creator
{
    class PersonCreator
    {
        public Person Create(int IdPerson, string Name, string Surname, string Position, DateTime DateOfBirth)
        {
            return new Person(IdPerson, Name, Surname, DateOfBirth);
        }
    }
}
