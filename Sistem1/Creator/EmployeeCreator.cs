﻿using FootApp.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.Creator
{
    class EmployeeCreator:PersonCreator
    {
        public Person Create(int IdPerson, string Name, string Surname, string Position, DateTime DateOfBirth, string EmploymentRecordBook)
        {
            return new Employee(IdPerson, Name, Surname, Position, DateOfBirth, EmploymentRecordBook);
        }
    }
}
