﻿using FootApp.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.Creator
{
    class CoachCreator : EmployeeCreator
    {
        public Person Create(int IdPerson, string Name, string Surname, string Position, DateTime DateOfBirth,string EmploymentRecordBook, String coachLicenceNumber)
        {
            return new Coach(IdPerson, Name, Surname, Position, DateOfBirth, EmploymentRecordBook,coachLicenceNumber);
        }
    }
}
