﻿using FootApp.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.Creator
{
    class PlayerCreator:PersonCreator
    {
        public Person Create(int IdPerson, string Name, string Surname, string Position, DateTime DateOfBirth, string team, string RegistrationNumber, DateTime DateJoined, string PrimaryFoot, int height, bool IsInjured = false, bool IsBought = false)
        {
            return new Player(IdPerson,Name,Surname,Position,DateOfBirth,team,RegistrationNumber,height, DateJoined, PrimaryFoot, IsInjured,IsBought);
        }
    }
}
