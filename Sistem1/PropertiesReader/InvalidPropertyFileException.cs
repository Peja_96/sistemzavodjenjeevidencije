﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp
{
    class InvalidPropertyFileException : Exception
    {
        public InvalidPropertyFileException(string message) : base(message) { }
    }
}
