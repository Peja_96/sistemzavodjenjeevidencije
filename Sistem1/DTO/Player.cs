﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    public class Player : Person
    {
        private string registrationNumber;
        private bool isInjured;
        private bool isBought;
        private string category;
        private string position;
        private int height;
        private DateTime dateJoined;
        private String primaryFoot;
        private string imagePath;

        
        public string Category { get => category; set => category = value; }
        public string RegistrationNumber { get => registrationNumber; set => registrationNumber = value; }

        public bool IsInjured { get => isInjured; set => isInjured = value; }
        public bool IsBought { get => isBought; set => isBought = value; }
        public string Position { get => position; set => position = value; }
        public int Height { get => height; set => height = value; }
        public DateTime DateJoined { get => dateJoined; set => dateJoined = value; }
        public string PrimaryFoot { get => primaryFoot; set => primaryFoot = value; }
        public string ImageFilePath { get => imagePath; set => imagePath = value; }

        public Player(int IdPerson, string Name, string Surname, string Position, DateTime DateOfBirth, string Category, string RegistrationNumber, int Height,DateTime dateJoined,String primaryFoot, bool IsInjured = false, bool IsBought = false) : base(IdPerson, Name, Surname, DateOfBirth)
        {
            this.ID = IdPerson;
            this.Name = Name;
            this.Surname = Surname;
            this.Position = Position;
            this.DateOfBirth = DateOfBirth;
            this.Category = Category;
            this.RegistrationNumber = RegistrationNumber;
            this.Height = Height;
            this.IsInjured = IsInjured;
            this.IsBought = IsBought;
            this.DateJoined = dateJoined;
            this.PrimaryFoot = primaryFoot;
        }   

        public Player() { }
        
        

        
    }
}
