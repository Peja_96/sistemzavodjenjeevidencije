﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    class PlayerContractStatistics
    {
        private decimal maxSalary;
        private decimal minSalary;
        private decimal salaryPerPlayer;
        private int numberOfContract;
        private int numberOfPlayer;
        private decimal allSalary;
        private int year;

        public decimal MaxSalary { get => maxSalary; set => maxSalary = value; }
        public decimal MinSalary { get => minSalary; set => minSalary = value; }
        public decimal SalaryPerPlayer { get => salaryPerPlayer; set => salaryPerPlayer = value; }
        public int NumberOfContract { get => numberOfContract; set => numberOfContract = value; }
        public int NumberOfPlayer { get => numberOfPlayer; set => numberOfPlayer = value; }
        public decimal AllSalary { get => allSalary; set => allSalary = value; }
        public int Year { get => year; set => year = value; }
    }
}
