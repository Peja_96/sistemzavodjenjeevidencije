﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    class MembershipFee
    {
        private int playerID;
        private String category;
        private int month;
        private int year;
        private decimal value;
        private DateTime paymentDate;

        public int PlayerID { get => playerID; set => playerID = value; }
        public string Category { get => category; set => category = value; }
        public int Month { get => month; set => month = value; }
        public int Year { get => year; set => year = value; }
        public decimal Value { get => value; set => this.value = value; }
        public DateTime PaymentDate { get => paymentDate; set => paymentDate = value; }
    }
}
