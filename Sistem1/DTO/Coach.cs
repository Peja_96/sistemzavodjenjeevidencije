﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    class Coach:Employee
    {
        private string coachLicenceNumber;
        public Coach(int IdPerson, string Name, string Surname, string Position, DateTime DateOfBirth, string EmploymentRecordBook,String coachLicenceNumber) : base(IdPerson, Name, Surname, Position, DateOfBirth, EmploymentRecordBook)
        {
            this.CoachLicenceNumber = coachLicenceNumber;
        }
        public Coach() { }
        public string CoachLicenceNumber { get => coachLicenceNumber; set => coachLicenceNumber = value; }
    }
}
