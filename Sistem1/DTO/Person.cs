﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    public class Person
    {
        private int id;
        private string name;
        private string surname;
        private DateTime dateOfBirth;
        
        

        public int ID { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Surname { get => surname; set => surname = value; }
        public DateTime DateOfBirth { get => dateOfBirth; set => dateOfBirth = value; }
        public string Date { get => DateOfBirth.ToString("dd.MM.yyyy."); }

        public Person (int IdPerson, string Name, string Surname, DateTime DateOfBirth)
        {
            this.ID = IdPerson;
            this.Name = Name;
            this.Surname = Surname;
            this.DateOfBirth = DateOfBirth;
        }
        public Person() { }
       

        
    }
}
