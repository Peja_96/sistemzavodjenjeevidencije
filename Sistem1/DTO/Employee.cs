﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    public class Employee : Person
    {
        private string employmentRecordBook;
        private string position;
        
        public Employee(int IdPerson, string Name, string Surname, string Position, DateTime DateOfBirth, string EmploymentRecordBook):base(IdPerson, Name, Surname, DateOfBirth)
        {
            this.EmploymentRecordBook = EmploymentRecordBook;
            this.Position = Position;
        }

        public Employee() { }

        public string EmploymentRecordBook { get => employmentRecordBook; set => employmentRecordBook = value; }
        public string Position { get => position; set => position = value; }
    }
}
