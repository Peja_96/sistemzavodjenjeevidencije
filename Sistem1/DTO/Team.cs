﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    public class Team
    {
        private string category;
        private Club club;

        public string Category { get => category; set => category = value; }
        public Club Club { get => club; set => club = value; }

        public Team(string category, Club club)
        {
            this.Category = category;
            this.Club = club;
        }

        public Team() { }

        public override string ToString()
        {
            return "  " + Category;
        }

        public override bool Equals(object obj)
        {
            if (obj is Team)
                return Category.Equals((obj as Team).Category);
            return false;
        }

        public override int GetHashCode()
        {
            return Category.GetHashCode();
        }
    }
}
