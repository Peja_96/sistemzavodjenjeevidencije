﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    class PlayerPerformance
    {
        private int playerID;
        private string playerCategory;
        private int competitionID;
        private DateTime startTime;
        private string category;
        private short goals;
        private short fouls;
        private short yellowCards;
        private bool redCard;
        private short totalPasses;
        private short successfulPasses;
        private short totalShots;
        private short shotsOnGoal;
        private short assists;

        public int PlayerID { get => playerID; set => playerID = value; }
        public string PlayerCategory { get => playerCategory; set => playerCategory = value; }
        public int CompetitionID { get => competitionID; set => competitionID = value; }
        public DateTime StartTime { get => startTime; set => startTime = value; }
        public string Category { get => category; set => category = value; }
        public short Goals { get => goals; set => goals = value; }
        public short Fouls { get => fouls; set => fouls = value; }
        public short YellowCards { get => yellowCards; set => yellowCards = value; }
        public bool RedCard { get => redCard; set => redCard = value; }
        public short TotalPasses { get => totalPasses; set => totalPasses = value; }
        public short SuccessfulPasses { get => successfulPasses; set => successfulPasses = value; }
        public short TotalShots { get => totalShots; set => totalShots = value; }
        public short ShotsOnGoal { get => shotsOnGoal; set => shotsOnGoal = value; }
        public short Assists { get => assists; set => assists = value; }
    }
}
