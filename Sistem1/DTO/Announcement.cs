﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    public class Announcement
    {
        private int announcementTableID;
        private string text;
        private DateTime release;
        private string personType;
        private string title;
        private string position;

        public string Text { get => text; set => text = value; }
        public DateTime Release { get => release; set => release = value; }
        public string PersonType { get => personType; set => personType = value; }
        public string Title { get => title; set => title = value; }
        public string Position { get => position; set => position = value; }
        public int AnnouncementTableID { get => announcementTableID; set => announcementTableID = value; }

    //Provjeravamo mrdž
    }
}
