﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    public class SponsorContract
    {
        private DateTime startDate;
        private DateTime endDate;
        private int sponsorID;
        private decimal investment;
        private String name;

        public DateTime StartDate { get => startDate; set => startDate = value; }
        public DateTime EndDate { get => endDate; set => endDate = value; }
        public int SponsorID { get => sponsorID; set => sponsorID = value; }
        public decimal Investment { get => Math.Round(investment); set => investment = value; }
        public string Name { get => name; set => name = value; }
        public string Signed { get => StartDate.ToString("dd.MM.yyyy."); }
        public string Expires { get => EndDate.ToString("dd.MM.yyyy."); }
    }
}
