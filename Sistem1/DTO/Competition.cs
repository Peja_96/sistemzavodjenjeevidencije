﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    public class Competition
    {
        private int id;
        private string title;
        private string description;
        private DateTime startDate;
        private DateTime endDate;
        private int seasonID;
        private short wins;
        private short draws;
        private short loses;
        private int clubID;
        private string category;
        private string type;

        public int Id { get => id; set => id = value; }
        public string Title { get => title; set => title = value; }
        public string Description { get => description; set => description = value; }
        public DateTime StartDate { get => startDate; set => startDate = value; }
        public DateTime EndDate { get => endDate; set => endDate = value; }
        public short Wins { get => wins; set => wins = value; }
        public short Draws { get => draws; set => draws = value; }
        public short Loses { get => loses; set => loses = value; }
        public int SeasonID { get => seasonID; set => seasonID = value; }
        public int ClubID { get => clubID; set => clubID = value; }
        public string Category { get => category; set => category = value; }
        public string Type { get => type; set => type = value; }

        public override string ToString()
        {
            return "  " + Title;
        }

        public override bool Equals(object obj)
        {
            if ((obj as Competition) != null)
                return (obj as Competition).Id == Id;
            return false;
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
    }
}
