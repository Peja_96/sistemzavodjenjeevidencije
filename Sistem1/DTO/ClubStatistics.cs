﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    class ClubStatistics
    {
        private string category;
        private int yellowCards;
        private decimal yellowCardsPerGame;
        private int redCards;
        private decimal redCardsPerGame;
        private decimal ballPossessionPerGame;
        private int totalPenalities;
        private decimal penalitiesPerGame;
        private int clubShots;
        private decimal clubShotsPerGame;
        private int clubShotsOnGoal;
        private decimal clubShotsOnGoalPerGame;
        private int goalsScored;
        private decimal goalsScoredPerGame;
        private int goalsConceded;
        private decimal goalsConcededPerGame;
        private int clubCorners;
        private decimal clubCornersPerGame;
        private decimal ticketsSoldPerGame;
        private int clubFreeKicks;
        private decimal clubFreeKicksPerGame;
        private int homeWins;
        private int homeDraw;
        private int homeLoses;
        private int wins;
        private int draws;
        private int loses;

        public string Category { get => category; set => category = value; }
        public int YellowCards { get => yellowCards; set => yellowCards = value; }
        public decimal YellowCardsPerGame { get => yellowCardsPerGame; set => yellowCardsPerGame = value; }
        public int RedCards { get => redCards; set => redCards = value; }
        public decimal RedCardsPerGame { get => redCardsPerGame; set => redCardsPerGame = value; }
        public decimal BallPossessionPerGame { get => ballPossessionPerGame; set => ballPossessionPerGame = value; }
        public int TotalPenalities { get => totalPenalities; set => totalPenalities = value; }
        public decimal PenalitiesPerGame { get => penalitiesPerGame; set => penalitiesPerGame = value; }
        public int ClubShots { get => clubShots; set => clubShots = value; }
        public decimal ClubShotsPerGame { get => clubShotsPerGame; set => clubShotsPerGame = value; }
        public int ClubShotsOnGoal { get => clubShotsOnGoal; set => clubShotsOnGoal = value; }
        public decimal ClubShotsOnGoalPerGame { get => clubShotsOnGoalPerGame; set => clubShotsOnGoalPerGame = value; }
        public int GoalsScored { get => goalsScored; set => goalsScored = value; }
        public decimal GoalsScoredPerGame { get => goalsScoredPerGame; set => goalsScoredPerGame = value; }
        public int GoalsConceded { get => goalsConceded; set => goalsConceded = value; }
        public decimal GoalsConcededPerGame { get => goalsConcededPerGame; set => goalsConcededPerGame = value; }
        public int HomeWins { get => homeWins; set => homeWins = value; }
        public int HomeDraw { get => homeDraw; set => homeDraw = value; }
        public int HomeLoses { get => homeLoses; set => homeLoses = value; }
        public int Wins { get => wins; set => wins = value; }
        public int Draws { get => draws; set => draws = value; }
        public int Loses { get => loses; set => loses = value; }
        public int ClubCorners { get => clubCorners; set => clubCorners = value; }
        public decimal ClubCornersPerGame { get => clubCornersPerGame; set => clubCornersPerGame = value; }
        public decimal TicketsSoldPerGame { get => ticketsSoldPerGame; set => ticketsSoldPerGame = value; }
        public decimal ClubFreeKicksPerGame { get => clubFreeKicksPerGame; set => clubFreeKicksPerGame = value; }
        public int ClubFreeKicks { get => clubFreeKicks; set => clubFreeKicks = value; }
    }
}