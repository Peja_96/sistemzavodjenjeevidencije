﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    public class PlayerContract
    {
        private DateTime startDate; 
        private DateTime endDate;
        private decimal salary;
        private int playerID;
        private String name;
        private String surname;

        public DateTime StartDate { get => startDate; set => startDate = value; }
        public DateTime EndDate { get => endDate; set => endDate = value; }
        public decimal Salary { get => Math.Round(salary); set => salary = value; }
        public int PlayerID { get => playerID; set => playerID = value; }
        public string Name { get => name; set => name = value; }
        public string Surname { get => surname; set => surname = value; }
        public string Signed { get => StartDate.ToString("dd.MM.yyyy."); }
        public string Expires { get => EndDate.ToString("dd.MM.yyyy."); }
    }
}
