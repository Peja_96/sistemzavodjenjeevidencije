﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    public class Season
    {
        private int id;
        private DateTime startDate;
        private DateTime endDate;

        public int Id { get => id; set => id = value; }
        public DateTime StartDate { get => startDate; set => startDate = value; }
        public DateTime EndDate { get => endDate; set => endDate = value; }

        public override bool Equals(object obj)
        {
            if (obj is Season)
                return id == (obj as Season).id;
            return false;
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
    }
}
