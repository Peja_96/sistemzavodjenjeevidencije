﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    public class PlayerStatistics
    {
        private Player player;
        private int gamesPlayed;
        private double foulsPerGame;
        private double passesPerGame;
        private double successfulPassesPercent;
        private double goalsPerGame;
        private double assistsPerGame;
        private double shotsPerGame;
        private double shotAccuracy;
        private int totalGoals;
        private int totalAssists;
        private int totalRedCards;
        private int totalYellowCards;
        private int totalFouls;

        public Player Player { get => player; set => player = value; }
        public int GamesPlayed { get => gamesPlayed; set => gamesPlayed = value; }

        public double PassesPerGame { get => passesPerGame; set => passesPerGame = value; }
        public double SuccessfulPassesPercent { get => successfulPassesPercent; set => successfulPassesPercent = value; }
        public double GoalsPerGame { get => goalsPerGame; set => goalsPerGame = value; }
        public double AssistsPerGame { get => assistsPerGame; set => assistsPerGame = value; }
        public double ShotsPerGame { get => shotsPerGame; set => shotsPerGame = value; }
        public double ShotAccuracy { get => shotAccuracy; set => shotAccuracy = value; }
        public int TotalGoals { get => totalGoals; set => totalGoals = value; }
        public int TotalAssists { get => totalAssists; set => totalAssists = value; }
        public int TotalRedCards { get => totalRedCards; set => totalRedCards = value; }
        public int TotalYellowCards { get => totalYellowCards; set => totalYellowCards = value; }
        public double FoulsPerGame { get => foulsPerGame; set => foulsPerGame = value; }
        public int TotalFouls { get => totalFouls; set => totalFouls = value; }

        public PlayerStatistics()
        {

        }

        public PlayerStatistics(Player player)
        {
            this.Player = player;
            this.GamesPlayed = 0;
            this.PassesPerGame = 0;
            this.TotalAssists = 0;
            this.ShotAccuracy = 0;
            this.TotalGoals = 0;
            this.TotalRedCards = 0;
            this.TotalYellowCards = 0;
            this.SuccessfulPassesPercent = 0;
            this.GoalsPerGame = 0;
            this.AssistsPerGame = 0;
            this.ShotsPerGame = 0;
        }
    }
}
