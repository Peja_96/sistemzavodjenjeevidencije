﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    public class Game
    { 
        private int competitionID;
        private string opponent;
        private string round;
        private DateTime startTime;
        private short clubResult;
        private short opponentResult;
        private short clubYellowCards;
        private short opponentYellowCards;
        private short clubRedCards;
        private short opponentRedCards;
        private short clubFreeKicks;
        private short clubShots;
        private short clubBallPossession;
        private short opponentBallPossession;
        private short opponentShots;
        private short clubShotsOnGoal;
        private short opponentShotsOnGoal;
        private short opponentFreeKicks;
        private short totalPenalities;
        private short clubCorners;
        private short opponentCorners;
        private int ticketsSold;
        private bool isHost = true;
        
        public string Opponent { get => opponent; set => opponent = value; }
        public DateTime StartTime { get => startTime; set => startTime = value; }
        public short ClubResult { get => clubResult; set => clubResult = value; }
        public string Round { get => round; set => round = value; }
        public short OpponentResult { get => opponentResult; set => opponentResult = value; }
        public short ClubYellowCards { get => clubYellowCards; set => clubYellowCards = value; }
        public short OpponentYellowCards { get => opponentYellowCards; set => opponentYellowCards = value; }
        public short ClubRedCards { get => clubRedCards; set => clubRedCards = value; }
        public short OpponentRedCards { get => opponentRedCards; set => opponentRedCards = value; }
        public short ClubFreeKicks { get => clubFreeKicks; set => clubFreeKicks = value; }
        public short OpponentFreeKicks { get => opponentFreeKicks; set => opponentFreeKicks = value; }
        public short ClubBallPossession { get => clubBallPossession; set => clubBallPossession = value; }
        public short OpponentBallPossession { get => opponentBallPossession; set => opponentBallPossession = value; }
        public short ClubShots { get => clubShots; set => clubShots = value; }
        public short OpponentShots { get => opponentShots; set => opponentShots = value; }
        public short ClubShotsOnGoal { get => clubShotsOnGoal; set => clubShotsOnGoal = value; }
        public short OpponentShotsOnGoal { get => opponentShotsOnGoal; set => opponentShotsOnGoal = value; }
        public short TotalPenalities { get => totalPenalities; set => totalPenalities = value; }
        public int CompetitionID { get => competitionID; set => competitionID = value; }
        public bool IsHost { get => isHost; set => isHost = value; }

        public string Title { get => Club.FootballClub.Name + " " + ClubResult + "-" + OpponentResult + " " + Opponent ; }
        public short ClubCorners { get => clubCorners; set => clubCorners = value; }
        public short OpponentCorners { get => opponentCorners; set => opponentCorners = value; }
        public int TicketsSold { get => ticketsSold; set => ticketsSold = value; }

        public Game(int CompetitionID, string Opponent, string Category, string Round, DateTime StartTime, short ClubResult, short OpponentResult,
    short ClubYellowCards, short OpponentYellowCards, short ClubRedCards, short OpponentRedCards, short ClubFreeKicks, short OpponentFreeKicks,
    short ClubShots, short OpponentShots, short ClubBallPossession, short OpponentBallPossession, short ClubShotsOnGoal, short OpponentShotsOnGoal,
    short TotalPenalities, short ClubCorners, short OpponentCorners, int TicketsSold, bool IsHost) //short ClubOffsides, short OpponentOffsides, short ClubFreeKicks, short OpponentFreeKicks, )
        {
            this.CompetitionID = CompetitionID;
            this.Opponent = Opponent;
            this.Round = Round;
            this.StartTime = StartTime;
            this.ClubResult = ClubResult;
            this.OpponentResult = OpponentResult;
            this.ClubYellowCards = ClubYellowCards;
            this.OpponentYellowCards = OpponentYellowCards;
            this.ClubRedCards = ClubRedCards;
            this.OpponentRedCards = OpponentRedCards;
            this.ClubFreeKicks = ClubFreeKicks;
            this.ClubShots = ClubShots;
            this.ClubBallPossession = ClubBallPossession;
            this.OpponentBallPossession = OpponentBallPossession;
            this.OpponentShots = OpponentShots;
            this.ClubShotsOnGoal = ClubShotsOnGoal;
            this.OpponentShotsOnGoal = OpponentShotsOnGoal;
            this.OpponentFreeKicks = OpponentFreeKicks;
            this.TotalPenalities = TotalPenalities;
            this.ClubCorners = ClubCorners;
            this.TicketsSold = TicketsSold;
            this.OpponentCorners = OpponentCorners;
            this.IsHost = IsHost;
        }
        public Game()
        {

        }

        public override string ToString()
        {
            return Club.FootballClub.Name + " vs " + this.Opponent + " " + this.StartTime;
        }
    }
}
