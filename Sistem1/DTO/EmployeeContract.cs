﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    class EmployeeContract
    {
        private int employeeID;
        private DateTime startDate;
        private DateTime endDate;
        private decimal salary;
        private String name;
        private String surname;

        public DateTime StartDate { get => startDate; set => startDate = value; }
        public DateTime EndDate { get => endDate; set => endDate = value; }
        public decimal Salary { get => Math.Round(salary); set => salary = value; }
        public int EmployeeID { get => employeeID; set => employeeID = value; }
        public string Name { get => name; set => name = value; }
        public string Surname { get => surname; set => surname = value; }
        public string Signed { get => StartDate.ToString("dd.MM.yyyy."); }
        public string Expires { get => EndDate.ToString("dd.MM.yyyy."); }

        public EmployeeContract() { }
        public EmployeeContract(int employeeID, DateTime startDate, DateTime endDate, decimal salary)
        {
            this.EmployeeID = employeeID;
            this.StartDate = startDate;
            this.EndDate = endDate;
            this.Salary = salary;
        }
    }
}
