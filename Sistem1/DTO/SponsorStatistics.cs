﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    class SponsorStatistics
    {
        private double allInvestment;
        private double investmentPerSponsor;
        private double maxInvestment;
        private double minInvestment;
        private int numberContracts;
        private int sponsors;
        private int year;

        public double AllInvestment { get => allInvestment; set => allInvestment = value; }
        public double InvestmentPerSponsor { get => investmentPerSponsor; set => investmentPerSponsor = value; }
        public double MaxInvestment { get => maxInvestment; set => maxInvestment = value; }
        public double MinInvestment { get => minInvestment; set => minInvestment = value; }
        public int NumberContracts { get => numberContracts; set => numberContracts = value; }
        public int Sponsors { get => sponsors; set => sponsors = value; }
        public int Year { get => year; set => year = value; }
    }
}
