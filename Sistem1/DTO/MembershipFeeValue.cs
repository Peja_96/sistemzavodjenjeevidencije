﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    class MembershipFeeValue
    {
        private int id;
        private DateTime startDate;
        private DateTime endDate;
        private decimal value;
        private int clubID;

        public int ID { get => id; set => id = value; }
        public DateTime StartDate { get => startDate; set => startDate = value; }
        public DateTime EndDate { get => endDate; set => endDate = value; }
        public decimal Value { get => value; set => this.value = value; }
        public int ClubID { get => clubID; set => clubID = value; }
    }
}
