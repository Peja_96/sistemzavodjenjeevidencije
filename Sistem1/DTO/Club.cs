﻿using FootApp.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    public class Club
    {
        
        private int id;
        private String name;
        private String stadionName;
        private int stadionSeats;
        private string address;

        public int ID { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string StadionName { get => stadionName; set => stadionName = value; }
        public int StadionSeats { get => stadionSeats; set => stadionSeats = value; }

        public static Club FootballClub { get; set; }
        public string Address { get => address; set => address = value; }
        //static Club()
        //{
        //    FootballClub = new ClubDAO().ReadClubInfo();
        //}
    }
}
