﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    class Debt
    {
        private String name;
        private String surname;
        private String date;

        public string Name { get => name; set => name = value; }
        public string Surname { get => surname; set => surname = value; }
        public string Date { get => date; set => date = value; }
    }
}
