﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    public class PlayerTransfer: FinancialEntry
    {
        private string playerName;
        private string playerSurname;
        private string otherClub;
        private TransferType transferType;

        public string PlayerName { get => playerName; set => playerName = value; }
        public string PlayerSurname { get => playerSurname; set => playerSurname = value; }
        public string OtherClub { get => otherClub; set => otherClub = value; }
        public TransferType TransferType { get => transferType; set => transferType = value; }
    }
}
