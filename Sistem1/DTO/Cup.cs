﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    public class Cup:Competition
    {
        private string finalResult;
        public string FinalResult { get => finalResult; set => finalResult = value; }
    }
}
