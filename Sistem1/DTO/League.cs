﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    public class League:Competition
    {
        private short points;
        private short finalPlace;

        public short Points { get => points; set => points = value; }
        public short FinalPlace { get => finalPlace; set => finalPlace = value; }
    }
}
