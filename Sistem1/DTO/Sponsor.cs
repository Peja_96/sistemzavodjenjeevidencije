﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    public class Sponsor
    {
        private int id;
        private string name;
        private SponsorType sponsorType;
        private int clubID;
        private bool isSponsorCurrently;
        private string contactMail;
        private string contactNumber;
        private string contactName;

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public SponsorType SponsorType { get => sponsorType; set => sponsorType = value; }
        public int ClubID { get => clubID; set => clubID = value; }
        public bool IsSponsorCurrently { get => isSponsorCurrently; set => isSponsorCurrently = value; }
        public string ContactMail { get => contactMail; set => contactMail = value; }

        public string ContactNumber { get => contactNumber; set => contactNumber = value; }
        public string ContactName { get => contactName; set => contactName = value; }
    }
}
