﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    public class Budget
    {
        private int year;
        private decimal amount;
        private int clubID;
        
        public int Year { get => year; set => year = value; }
        public decimal Amount { get => amount; set => amount = value; }
        public int ClubID { get => clubID; set => clubID = value; }
    }
}
