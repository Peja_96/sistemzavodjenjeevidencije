﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.DTO
{
    class AnnouncementTable
    {
        private int id;
        private int clubID;

        public int ID { get => id; set => id = value; }
        internal int ClubID { get => clubID; set => clubID = value; }
    }
}
