﻿using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp
{
    class DatabaseConnection
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static MySqlConnection MySqlConnection;
        private static DatabaseConnection Connection;

        private static string dbServer;
        private static string databaseName;
        private static string username;
        private static string password;
        
        static DatabaseConnection()
        {
            PropertiesReader propertiesReader = new PropertiesReader();
            propertiesReader.Load("database.properties");
            dbServer = propertiesReader.GetProperty("dbServer");
            databaseName = propertiesReader.GetProperty("database");
            username = propertiesReader.GetProperty("username");
            password = propertiesReader.GetProperty("password");
        }
        private DatabaseConnection()
        {
                MySqlConnection = new MySqlConnection("Server=" + dbServer + ";Database=" + databaseName +";Port=3306"+ ";UserId=" + username + ";Password=" + password);
        }
        public static DatabaseConnection GetConnection()
        {
            if (Connection == null)
            {
                Connection = new DatabaseConnection();
                try
                {
                    MySqlConnection.Open();
                }
                catch (Exception e)
                {
                    log.Warn(e);
                    Environment.Exit(0);
                }
            }
            return Connection;
        }
        public MySqlCommand CreateCommand()
        {
            return MySqlConnection.CreateCommand();
        }

        public void CloseConnection()
        {
            MySqlConnection.Close();
            MySqlConnection = null;
        }
    }
}
