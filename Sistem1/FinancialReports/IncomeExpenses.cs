﻿using FootApp.DAO;
using FootApp.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootApp.FinancialReports
{
    public class IncomeExpenses
    {
        private decimal playersSalaries;
        private decimal employeeSalaries;
        private decimal equipmentCosts;
        private decimal sponsorsIncome;
        private decimal ticketsIncome;
        private decimal membershipFees;

        public decimal PlayersSalaries { get => playersSalaries; set => playersSalaries = value; }
        public decimal EmployeeSalaries { get => employeeSalaries; set => employeeSalaries = value; }
        public decimal EquipmentCosts { get => equipmentCosts; set => equipmentCosts = value; }
        public decimal SponsorsIncome { get => sponsorsIncome; set => sponsorsIncome = value; }
        public decimal TicketsIncome { get => ticketsIncome; set => ticketsIncome = value; }
        public decimal MembershipFees { get => membershipFees; set => membershipFees = value; }

        public IncomeExpenses GetIncomeExpensseForCurrentMonth()
        {
            List<FinancialEntry> financialEntries = new FinancialEntryDAO().GetFinancialEntriesForCurrentMonth();
            return GetIncomeExpensse(financialEntries);

        }
        private IncomeExpenses GetIncomeExpensse(List<FinancialEntry> financialEntries)
        {
            IncomeExpenses incomeExpensse = new IncomeExpenses();
            foreach (var entry in financialEntries)
            {
                if (entry.Description.StartsWith("Employee Salary"))
                {
                    incomeExpensse.employeeSalaries += entry.Amount;
                }
                else if (entry.Description.StartsWith("Player Salary"))
                {
                    incomeExpensse.playersSalaries += entry.Amount;
                }
                else if (entry.Description.StartsWith("Membership payment"))
                {
                    incomeExpensse.membershipFees += entry.Amount;
                }
                else if (entry.Description.StartsWith("Sponsor Investment"))
                {
                    incomeExpensse.sponsorsIncome += entry.Amount;
                }
                else if (entry.Description.StartsWith("Equipment purchase"))
                {
                    incomeExpensse.equipmentCosts += entry.Amount;
                }
                else
                {
                    incomeExpensse.ticketsIncome += entry.Amount;
                }
            }
            return incomeExpensse;
        }
        public IncomeExpenses GetIncomeExpensseForPeriod(DateTime start, DateTime end)
        {
            List<FinancialEntry> financialEntries = new FinancialEntryDAO().GetFinancialEntriesForPeriod(start,end);
            return GetIncomeExpensse(financialEntries);
        }
    }
}
